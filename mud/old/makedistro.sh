#/bin/sh
# Make distro
# ������ �����������
# usage: makedistro
#
cd ..
rm vmud-distro.tgz
rm vmud-tmp.tar
rm -rf NEWROOT
mkdir NEWROOT
echo tar cf start
tar cf vmud-tmp.tar mud src
echo tar cf finish
mv vmud-tmp.tar NEWROOT
cd NEWROOT
echo tar xf start
tar xf vmud-tmp.tar
echo tar xf finish
echo delete files start
rm vmud-tmp.tar
rm mud/bin/circle
rm mud/bin/*.txt
rm mud/lib/etc/board/*
rm mud/lib/etc/mudmap.txt
rm mud/lib/etc/plrmail
rm mud/lib/system.txt
rm mud/lib/script.txt
rm mud/lib/script.sh
rm -rf mud/lib/misc/1
rm mud/lib/misc/maxusers.lst
echo>mud/lib/misc/apr_name
echo>mud/lib/misc/dis_name
echo>mud/lib/misc/new_name
echo>mud/lib/misc/bugs
echo>mud/lib/misc/globaluid
echo>mud/lib/misc/ideas
echo>mud/lib/misc/typos
cp mud/lib/misc/privilege.lst.sample mud/lib/misc/privilege.lst
#
rm -rf mud/lib/plralias/A-E/*
rm -rf mud/lib/plralias/F-J/*
rm -rf mud/lib/plralias/K-O/*
rm -rf mud/lib/plralias/P-T/*
rm -rf mud/lib/plralias/U-Z/*
rm -rf mud/lib/plralias/ZZZ/*
#
rm -rf mud/lib/plrobjs/A-E/*
rm -rf mud/lib/plrobjs/F-J/*
rm -rf mud/lib/plrobjs/K-O/*
rm -rf mud/lib/plrobjs/P-T/*
rm -rf mud/lib/plrobjs/U-Z/*
rm -rf mud/lib/plrobjs/ZZZ/*
#
rm -rf mud/lib/plrstuff/depot/*
rm -rf mud/lib/plrstuff/house/*
rm mud/lib/plrstuff/*
#
rm -rf mud/lib/plrs/A-E/*
rm -rf mud/lib/plrs/F-J/*
rm -rf mud/lib/plrs/K-O/*
rm -rf mud/lib/plrs/P-T/*
rm -rf mud/lib/plrs/U-Z/*
rm -rf mud/lib/plrs/ZZZ/*
rm mud/lib/plrs/*
#
rm -rf mud/log/*
rm mud/.crash
rm mud/.fastboot
rm mud/autolog.txt
rm mud/pause
rm mud/syslog
rm mud/syslog*
rm src/*.o
echo delete files finish
echo tar distro start
tar czf vmud-distro.tgz *
echo tar distro finish
mv vmud-distro.tgz ..
cd ..
mv vmud-distro.tgz ../vmud-distro-`date "+%d-%m-%Y"`.tgz
rm -rf NEWROOT
