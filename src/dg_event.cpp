/* ************************************************************************
*  File: dg_event.cpp                                    
*                                                                       
*  Usage: This file contains a simplified event system to allow wait    
*                                                                       
*  Death's Gate MUD is based on CircleMUD, Copyright (C) 1993, 94.    
*  CircleMUD is based on DikuMUD, Copyright (C) 1990, 1991.           
*                                                                    
*  $Date: 2008/08/15 12:18:15 $                                     
*  $Revision: 1.2 $                                           
************************************************************************ */

#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "utils.h"
#include "dg_event.h"
#include "db.h"
#include "dg_scripts.h"
#include "comm.h"

/*
** define statics
*/
static struct event_info *event_list = NULL;


/*
** Add an event to the current list
*/
struct event_info *add_event(int time, EVENT(*func), void *info)
{
	struct event_info *this_data, *prev, *curr;

	CREATE(this_data, struct event_info, 1);
	this_data->time_remaining = time;
	this_data->func = func;
	this_data->info = info;

	/* sort the event into the list in next-to-fire order */
	if (event_list == NULL)
		event_list = this_data;
	else if (this_data->time_remaining <= event_list->time_remaining) {
		this_data->next = event_list;
		event_list = this_data;
	} else {
		prev = event_list;
		curr = prev->next;

		while (curr && (curr->time_remaining > this_data->time_remaining)) {
			prev = curr;
			curr = curr->next;
		}

		this_data->next = curr;
		prev->next = this_data;
	}

	return this_data;
}

void remove_event(struct event_info *event)
{
	struct event_info *curr;

	if (event_list == event) {
		event_list = event->next;
	} else {
		curr = event_list;
		while (curr && (curr->next != event))
			curr = curr->next;
		if (!curr)
			return;	/* failed to find it */
		curr->next = curr->next->next;
	}
	free(event);
}

#define MAX_EVENT_TIME 80
void process_events(void)
{
	struct event_info *e = event_list;
	struct event_info *del;
	struct timeval start, stop;
	int trig_vnum;

	gettimeofday(&start, NULL);

	while (e) {
		if (--(e->time_remaining) == 0) {
			trig_vnum = GET_TRIG_VNUM(((struct wait_event_data *) (e->info))->trigger);
			e->func(e->info);

			del = e;
			e = e->next;

			remove_event(del);
			// �� ��������� ���������� ������ �������� ����� 50 �������
			// �� ���������� ������ ����������� ��������� �� ��������� ���.
			// ������ ��� ����� ������������ ������������� ������� ����������.
			gettimeofday(&stop, NULL);
			if (((stop.tv_sec - start.tv_sec) > 0) ||
			    (((stop.tv_usec - start.tv_usec) / 1000) > MAX_EVENT_TIME)) {
				// ������� ����� �������� ������� ���������� ����� ������.
				sprintf(buf,
					"SCRIPT TIMER (TrigVNum: %d) : �������� ����� ������� ��������� ���������� ���������.",
					trig_vnum);
				mudlog(buf, BRF, -1, ERRLOG, TRUE);

				break;
			}
		} else
			e = e->next;
	}
}
