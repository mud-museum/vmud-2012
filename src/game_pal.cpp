/* ************************************************************************
*  File: game_pal.cpp
*  Usage: ���� � �������
*  Author: Prool
*
************************************************************************ */

#if 1
#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "utils.h"
#include "comm.h"
#include "interpreter.h"
#include "handler.h"
#include "db.h"
#include "spells.h"
#include "skills.h"
#include "screen.h"
#include "dg_scripts.h"
#include "constants.h"
#include "features.hpp"
#include "house.h"
#include "prool.h"

/*   external vars  */

extern CHAR_DATA *character_list;
extern DESCRIPTOR_DATA *descriptor_list;
extern INDEX_DATA *mob_index;
extern INDEX_DATA *obj_index;
extern TIME_INFO_DATA time_info;
extern struct spell_create_type spell_create[];
extern int guild_info[][3];

/* extern functions */
void add_follower(CHAR_DATA * ch, CHAR_DATA * leader);
ACMD(do_drop);
ACMD(do_gen_door);
ACMD(do_say);
int go_track(CHAR_DATA * ch, CHAR_DATA * victim, int skill_no);
int has_key(CHAR_DATA * ch, obj_vnum key);
int find_first_step(room_rnum src, room_rnum target, CHAR_DATA * ch);
void do_doorcmd(CHAR_DATA * ch, OBJ_DATA * obj, int door, int scmd);
int check_recipe_items(CHAR_DATA * ch, int spellnum, int spelltype, int extract);
SPECIAL(shop_keeper);
void ASSIGNMASTER(mob_vnum mob, SPECIAL(fname), int learn_info);
int mag_manacost(CHAR_DATA * ch, int spellnum);
int has_key(CHAR_DATA * ch, obj_vnum key);
int ok_pick(CHAR_DATA * ch, obj_vnum keynum, int pickproof, int scmd);
int find_door(CHAR_DATA * ch, const char *type, char *dir, const char *cmdname);
#endif

/* functions */
SPECIAL(igra_v_palochki);

SPECIAL(igra_v_palochki)
{
OBJ_DATA *i;
printf("igra v palochki. cmd `%s' `%s'\n",cmd_info[cmd].command,argument);

if (CMD_IS("�������"))
    {
    printf("������� ���� �������\n");
    // ���� �������� ����� �� ����

	for (i = world[ch->in_room]->contents ; i; i = i->next_content)
    		{
		printf("looking %s\n", i->name);
    		}

    // ���� ��� � ��������
    // ���� ������ ����� � �������
    // ���� ������ ����� � �������
    // �� ���������� ������� �������
    }
return 0;
}
