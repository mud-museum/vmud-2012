/* ************************************************************************
*   File: geometry.cpp                           Part of Virtustan MUD    *
*  Usage: geometry package for room editor                                *
*                                                                         *
*  Copyleft 2009, Prool                                                   *
*                                                                         *
*  Author: Prool, proolix@gmail.com, http://prool.kharkov.org             *
************************************************************************ */

#include "geometry.h"

// static variables

int g_x=0; // geometry's x
int g_y=0;
int g_z=0;
int g_zone=-1; // ����� ����, ��� ������� �������� �����

int idmap=0;

struct geo_record karta[100];

void geo_init (void) // ������������� �����
{int i;
for (i=0;i<100;i++) karta[i].flag=0;
g_x=0;
g_y=0;
g_z=0;
}

// geometry 2
// ������ ���������������� ������
// (�������� ��� ���� �������, ��� �������� � ���������� � ����������)

int g2_x=0;
int g2_y=0;
int g2_z=0;
int g2_room=0; // ������� vnum �������

int automap_mode=0;
int minimap_mode=0;

int geo2_mode=0;

struct geo2_record karta2[GEO2_SIZE];

char kartograf[50];

void geo2_init (void) // ������������� �����
{int i;
for (i=0;i<GEO2_SIZE;i++)
	{
	karta2[i].room=0;
	karta2[i].x=0;
	karta2[i].y=0;
	karta2[i].z=0;
	}
g_x=0;
g_y=0;
g_z=0;
}
