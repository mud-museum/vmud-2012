/* ************************************************************************
*   File: geometry.h                             Part of Virtustan MUD    *
*  Usage: geometry package for room editor                                *
*                                                                         *
*  Copyleft 2009, Prool                                                   *
*                                                                         *
*  Author: Prool, proolix@gmail.com, http://prool.kharkov.org             *
************************************************************************ */

#define GEO2_SIZE 300 // ������ ������� ��� �����������-2

// static variables

extern int g_x; // geometry's x
extern int g_y;
extern int g_z;
extern int g_zone;

extern int idmap;

struct	geo_record
	{
	int flag; // 0 - ������ �� ���������, 1 - ������ ���������
	int x;
	int y;
	int z;
	};

extern struct geo_record karta[100];

void geo_init (void); // ������������� �����

// geometry 2
// ������ ���������������� ������
// (�������� ��� ���� �������, ��� �������� � ���������� � ����������)

extern int g2_x;
extern int g2_y;
extern int g2_z;
extern int g2_room; // ������� vnum �������

extern int automap_mode;
extern int minimap_mode;

extern int geo2_mode;

struct geo2_record
	{
	int room; // 0 - ������ �� ���������, �� 0 - vnum �������
	int x;
	int y;
	int z;
	};

extern struct geo2_record karta2[GEO2_SIZE];

extern char kartograf[];

void geo2_init();
void make_map2 (char *);
