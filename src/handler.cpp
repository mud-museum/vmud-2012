#define VIRTUSTAN
// #define PROOL

/* ************************************************************************
*   File: handler.cpp                                   
*  Usage: internal funcs: moving and finding chars/objs                   
*                                                                         
*  $Date: 2009/05/25 11:11:50 $                                           
*  $Revision: 1.8 $                                                       
************************************************************************ */

#include "conf.h"
#include <math.h>
// #include <sstream>
#include "sysdep.h"
#include "structs.h"
#include "constants.h"
#include "utils.h"
#include "comm.h"
#include "db.h"
#include "handler.h"
#include "interpreter.h"
#include "spells.h"
#include "skills.h"
#include "screen.h"
#include "dg_scripts.h"
#include "auction.h"
#include "features.hpp"
#include "house.h"
#include "exchange.h"
// #include "char.hpp"
// #include "char_player.hpp"
// #include "liquid.hpp"

// ��� ������, �� ����� ������ ������. ����� ���� �� ������ ������ ��� ��...

int max_stats2[][6] =
/* Str Dex Int Wis Con Cha */
{ {14, 13, 24, 25, 15, 10},	/* ������ */
{14, 12, 25, 23, 13, 16},	/* ������ */
{19, 25, 12, 12, 17, 16},	/* ��� */
{25, 11, 15, 15, 25, 10},	/* ������ */
{22, 24, 14, 14, 20, 12},	/* ������� */
{23, 17, 14, 14, 22, 12},	/* ��������� */
{14, 12, 25, 23, 13, 16},	/* �������� */
{14, 12, 25, 23, 13, 16},	/* ��������� */
{15, 13, 25, 23, 14, 13},	/* ������������ */
{22, 13, 16, 19, 18, 17},	/* ������ */
{25, 21, 16, 16, 20, 16},	/* ������� */
{25, 17, 13, 15, 17, 16},	/* ������ */
{21, 17, 13, 13, 21, 16},	/* ����� */
{18, 12, 24, 18, 17, 12},	/* ����� */
{25, 25, 24, 23, 22, 17}	/* ������� */
};

int min_stats2[][6] =
/*  Str Dex Int Wis Con Cha */
{ {11, 10, 19, 20, 12, 10},	/* ������ */
{10, 9, 20, 18, 10, 13},	/* ������ */
{16, 22, 9, 9, 14, 13},		/* ��� */
{21, 11, 11, 12, 22, 10},	/* ������ */
{17, 19, 11, 11, 15, 12},	/* ������� */
{20, 14, 10, 10, 17, 12},	/* ��������� */
{10, 9, 20, 18, 10, 13},	/* �������� */
{10, 9, 20, 18, 10, 13},	/* ��������� */
{10, 9, 20, 20, 11, 10},	/* ������������ */
{19, 10, 12, 15, 14, 13},	/* ������ */
{19, 15, 11, 11, 17, 11},	/* ������� */
{20, 14, 11, 12, 14, 12},	/* ������ */
{18, 14, 10, 10, 18, 13},	/* ����� */
{15, 10, 19, 15, 13, 12},	/* ����� */
{15, 10, 19, 15, 13, 12}	/* ������� */
};

/* local functions */
int apply_ac(CHAR_DATA * ch, int eq_pos);
int apply_armour(CHAR_DATA * ch, int eq_pos);
void update_object(OBJ_DATA * obj, int use);
void update_char_objects(CHAR_DATA * ch);

/* external functions */
void perform_drop_gold(CHAR_DATA * ch, int amount, byte mode, room_rnum RDR);
int mag_manacost(CHAR_DATA * ch, int spellnum);
int slot_for_char(CHAR_DATA * ch, int i);
int invalid_anti_class(CHAR_DATA * ch, OBJ_DATA * obj);
int invalid_unique(CHAR_DATA * ch, OBJ_DATA * obj);
int invalid_no_class(CHAR_DATA * ch, OBJ_DATA * obj);
int invalid_clan(CHAR_DATA * ch, OBJ_DATA * obj);
void remove_follower(CHAR_DATA * ch);
void clearMemory(CHAR_DATA * ch);
void die_follower(CHAR_DATA * ch);
int extra_damroll(int class_num, int level);
int Crash_delete_file(char *name, int mask);
void do_entergame(DESCRIPTOR_DATA * d);
ACMD(do_return);

extern void check_auction(CHAR_DATA * ch, OBJ_DATA * obj);
extern void check_exchange(OBJ_DATA * obj);
void free_script(SCRIPT_DATA * sc);
int get_player_charms(CHAR_DATA * ch, int spellnum);
int calculate_resistance_coeff (CHAR_DATA *ch, int resist_type, int effect);

extern struct zone_data *zone_table;

extern int global_uid;

char *fname(const char *namelist)
{
	static char holder[30];
	register char *point;

	for (point = holder; a_isalpha(*namelist); namelist++, point++)
		*point = *namelist;

	*point = '\0';

	return (holder);
}

/*
int isname(const char *str, const char *namelist)
{
  const char *curname, *curstr;

  curname = namelist;
  for (;;)
    {for (curstr = str;; curstr++, curname++)
         {if (!*curstr && !a_isalpha(*curname))
	         return (1);

          if (!*curname)
	         return (0);

          if (!*curstr || *curname == ' ')
	      break;

          if (LOWER(*curstr) != LOWER(*curname))
	      break;
         }

     for (; a_isalpha(*curname); curname++);
     if (!*curname)
        return (0);
     curname++;
     }
}
*/

int isname(const char *str, const char *namelist)
{
	int once_ok = FALSE;
	const char *curname, *curstr, *laststr;

	if (!namelist || !*namelist || !str)
		return (FALSE);

	curname = namelist;
	curstr = laststr = str;
	for (;;) {
		once_ok = FALSE;
		for (;; curstr++, curname++) {
			if (!*curstr)
				return (once_ok);
			if (curstr != laststr && *curstr == '!')
				if (a_isalnum(*curname)) {
					curstr = laststr;
					break;
				}
			if (!a_isalnum(*curstr)) {
				for (; !a_isalnum(*curstr); curstr++) {
					if (!*curstr)
						return (once_ok);
				}
				laststr = curstr;
				break;
			}
			if (!*curname)
				return (FALSE);
			if (!a_isalnum(*curname)) {
				curstr = laststr;
				break;
			}
			if (LOWER(*curstr) != LOWER(*curname)) {
				curstr = laststr;
				break;
			} else
				once_ok = TRUE;
		}
		/* skip to next name */
		for (; a_isalnum(*curname); curname++);
		for (; !a_isalnum(*curname); curname++) {
			if (!*curname)
				return (FALSE);
		}
	}
}

void set_quested(CHAR_DATA * ch, int quest)
{
	int i;
	if (IS_NPC(ch) || IS_IMMORTAL(ch))
		return;
	if (ch->Questing.quests) {
		for (i = 0; i < ch->Questing.count; i++)
			if (*(ch->Questing.quests + i) == quest)
				return;
		if (!(ch->Questing.count % 10L))
			RECREATE(ch->Questing.quests, int, (ch->Questing.count / 10L + 1) * 10L);
	} else {
		ch->Questing.count = 0;
		CREATE(ch->Questing.quests, int, 10);
	}
	*(ch->Questing.quests + ch->Questing.count++) = quest;
}

void set_unquest(CHAR_DATA * ch, int quest)
{
	int i, j;

	for (i = j = 0; j < ch->Questing.count; i++, j++) {
		if (ch->Questing.quests[i] == quest)
			j++;
		ch->Questing.quests[i] = ch->Questing.quests[j];
	}
	if (j > i)
		ch->Questing.count--;

}


int get_quested(CHAR_DATA * ch, int quest)
{
	int i;
	if (IS_NPC(ch) || IS_IMMORTAL(ch))
		return (FALSE);
	if (ch->Questing.quests) {
		for (i = 0; i < ch->Questing.count; i++)
			if (*(ch->Questing.quests + i) == quest)
				return (TRUE);
	}
	return (FALSE);
}


void check_light(CHAR_DATA * ch, int was_equip, int was_single, int was_holylight, int was_holydark, int koef)
{
	int light_equip = FALSE;
	if (!IS_NPC(ch) && !ch->desc)
		return;
	if (IN_ROOM(ch) == NOWHERE)
		return;
	//if (IS_IMMORTAL(ch))
	//   {sprintf(buf,"%d %d %d (%d)\r\n",world[IN_ROOM(ch)]->light,world[IN_ROOM(ch)]->glight,world[IN_ROOM(ch)]->gdark,koef);
	//    send_to_char(buf,ch);
	//   }
	if (GET_EQ(ch, WEAR_LIGHT)) {
		if (GET_OBJ_TYPE(GET_EQ(ch, WEAR_LIGHT)) == ITEM_LIGHT) {
			if (GET_OBJ_VAL(GET_EQ(ch, WEAR_LIGHT), 2)) {	//send_to_char("Light OK!\r\n",ch);
				light_equip = TRUE;
			}
		}
	}
	// In equipment
	if (light_equip) {
		if (was_equip == LIGHT_NO)
			world[ch->in_room]->light = MAX(0, world[ch->in_room]->light + koef);
	} else {
		if (was_equip == LIGHT_YES)
			world[ch->in_room]->light = MAX(0, world[ch->in_room]->light - koef);
	}
	// Singleligt affect
	if (AFF_FLAGGED(ch, AFF_SINGLELIGHT)) {
		if (was_single == LIGHT_NO)
			world[ch->in_room]->light = MAX(0, world[ch->in_room]->light + koef);
	} else {
		if (was_single == LIGHT_YES)
			world[ch->in_room]->light = MAX(0, world[ch->in_room]->light - koef);
	}
	// Holyligh affect
	if (AFF_FLAGGED(ch, AFF_HOLYLIGHT)) {
		if (was_holylight == LIGHT_NO)
			world[ch->in_room]->glight = MAX(0, world[ch->in_room]->glight + koef);
	} else {
		if (was_holylight == LIGHT_YES)
			world[ch->in_room]->glight = MAX(0, world[ch->in_room]->glight - koef);
	}
	// Holydark affect
	// if (IS_IMMORTAL(ch))
	//    {sprintf(buf,"holydark was %d\r\n",was_holydark);
	//     send_to_char(buf,ch);
	//    }
	if (AFF_FLAGGED(ch, AFF_HOLYDARK)) {	// if (IS_IMMORTAL(ch))
		//    send_to_char("holydark on\r\n",ch);
		if (was_holydark == LIGHT_NO)
			world[ch->in_room]->gdark = MAX(0, world[ch->in_room]->gdark + koef);
	} else {		// if (IS_IMMORTAL(ch))
		//   send_to_char("HOLYDARK OFF\r\n",ch);
		if (was_holydark == LIGHT_YES)
			world[ch->in_room]->gdark = MAX(0, world[ch->in_room]->gdark - koef);
	}
	//if (IS_IMMORTAL(ch))
	//   {sprintf(buf,"%d %d %d (%d)\r\n",world[IN_ROOM(ch)]->light,world[IN_ROOM(ch)]->glight,world[IN_ROOM(ch)]->gdark,koef);
	//    send_to_char(buf,ch);
	//   }
}

void affect_modify(CHAR_DATA * ch, byte loc, sbyte mod, bitvector_t bitv, bool add)
{
	if (add) {
#if 1 // prool: ��� � ����� ��������, �� �� ������ (����. ��� "������ �����")
		SET_BIT(AFF_FLAGS(ch, bitv), bitv);
#endif
	} else {
#if 1 // prool
		REMOVE_BIT(AFF_FLAGS(ch, bitv), bitv);
#endif
		mod = -mod;
	}

	switch (loc) {
	case APPLY_NONE:
		break;
	case APPLY_STR:
		GET_STR_ADD(ch) += mod;
		break;
	case APPLY_DEX:
		GET_DEX_ADD(ch) += mod;
		break;
	case APPLY_INT:
		GET_INT_ADD(ch) += mod;
		break;
	case APPLY_WIS:
		GET_WIS_ADD(ch) += mod;
		break;
	case APPLY_CON:
		GET_CON_ADD(ch) += mod;
		break;
	case APPLY_CHA:
		GET_CHA_ADD(ch) += mod;
		break;
	case APPLY_CLASS:
		break;

		/*
		 * My personal thoughts on these two would be to set the person to the
		 * value of the apply.  That way you won't have to worry about people
		 * making +1 level things to be imp (you restrict anything that gives
		 * immortal level of course).  It also makes more sense to set someone
		 * to a class rather than adding to the class number. -gg
		 */

	case APPLY_LEVEL:
		break;
	case APPLY_AGE:
		GET_AGE_ADD(ch) += mod;
		break;
	case APPLY_CHAR_WEIGHT:
		GET_WEIGHT_ADD(ch) += mod;
		break;
	case APPLY_CHAR_HEIGHT:
		GET_HEIGHT_ADD(ch) += mod;
		break;
	case APPLY_MANAREG:
		GET_MANAREG(ch) += mod;
		break;
	case APPLY_HIT:
		GET_HIT_ADD(ch) += mod;
		break;
	case APPLY_MOVE:
		GET_MOVE_ADD(ch) += mod;
		break;
	case APPLY_GOLD:
		break;
	case APPLY_EXP:
		break;
	case APPLY_AC:
		GET_AC_ADD(ch) += mod;
		break;
	case APPLY_HITROLL:
		GET_HR_ADD(ch) += mod;
		break;
	case APPLY_DAMROLL:
		GET_DR_ADD(ch) += mod;
		break;
	case APPLY_SAVING_WILL:
		GET_SAVE(ch, SAVING_WILL) += mod;
		break;
	case APPLY_RESIST_FIRE:
		GET_RESIST(ch, FIRE_RESISTANCE) += mod;
		break;
	case APPLY_RESIST_AIR:
		GET_RESIST(ch, AIR_RESISTANCE) += mod;
		break;
	case APPLY_SAVING_CRITICAL:
		GET_SAVE(ch, SAVING_CRITICAL) += mod;
		break;
	case APPLY_SAVING_STABILITY:
		GET_SAVE(ch, SAVING_STABILITY) += mod;
		break;
	case APPLY_SAVING_REFLEX:
		GET_SAVE(ch, SAVING_REFLEX) += mod;
		break;
	case APPLY_HITREG:
		GET_HITREG(ch) += mod;
		break;
	case APPLY_MOVEREG:
		GET_MOVEREG(ch) += mod;
		break;
	case APPLY_C1:
	case APPLY_C2:
	case APPLY_C3:
	case APPLY_C4:
	case APPLY_C5:
	case APPLY_C6:
	case APPLY_C7:
	case APPLY_C8:
	case APPLY_C9:
		GET_SLOT(ch, loc - APPLY_C1) += mod;
		break;
	case APPLY_SIZE:
		GET_SIZE_ADD(ch) += mod;
		break;
	case APPLY_ARMOUR:
		GET_ARMOUR(ch) += mod;
		break;
	case APPLY_POISON:
		GET_POISON(ch) += mod;
		break;
	case APPLY_CAST_SUCCESS:
		GET_CAST_SUCCESS(ch) += mod;
		break;
	case APPLY_MORALE:
		GET_MORALE(ch) += mod;
		break;
	case APPLY_INITIATIVE:
		GET_INITIATIVE(ch) += mod;
		break;
	case APPLY_RELIGION:
		if (add)
			GET_PRAY(ch) |= mod;
		else
			GET_PRAY(ch) &= mod;
		break;
	case APPLY_ABSORBE:
		GET_ABSORBE(ch) += mod;
		break;
	case APPLY_LIKES:
		GET_LIKES(ch) += mod;
		break;
	case APPLY_RESIST_WATER:
		GET_RESIST(ch, WATER_RESISTANCE) += mod;
		break;
	case APPLY_RESIST_EARTH:
		GET_RESIST(ch, EARTH_RESISTANCE) += mod;
		break;
	case APPLY_RESIST_VITALITY:
		GET_RESIST(ch, VITALITY_RESISTANCE) += mod;
		break;
	case APPLY_RESIST_MIND:
		GET_RESIST(ch, MIND_RESISTANCE) += mod;
		break;
	case APPLY_RESIST_IMMUNITY:
		GET_RESIST(ch, IMMUNITY_RESISTANCE) += mod;
		break;
	case APPLY_AR:
		GET_AR(ch) += mod;
		break;
	case APPLY_MR:
		GET_MR(ch) += mod;
		break;
	default:
		log("SYSERR: Unknown apply adjust %d attempt (%s, affect_modify).", loc, __FILE__);
		break;

	}			/* switch */
}

void affect_room_modify(ROOM_DATA * room, byte loc, sbyte mod, bitvector_t bitv, bool add)
{
	if (add) {
		SET_BIT(ROOM_AFF_FLAGS(room, bitv), bitv);
	} else {
		REMOVE_BIT(ROOM_AFF_FLAGS(room, bitv), bitv);
		mod = -mod;
	}

	switch (loc) {
	case APPLY_ROOM_NONE:
		break;
	case APPLY_ROOM_POISON:
		/* ����������� ������������ �� ������� ����������� SPELL_POISONED_FOG */
		/* ���� ��� �������� ������ ��� ������� ���� �� �������������� ������ */
			GET_ROOM_ADD_POISON(room) += mod;
		break;
	default:
		log("SYSERR: Unknown room apply adjust %d attempt (%s, affect_modify).", loc, __FILE__);
		break;

	}			/* switch */
}

void total_gods_affect(CHAR_DATA * ch)
{
	struct gods_celebrate_apply_type *cur = NULL;

	if (IS_NPC(ch) || supress_godsapply)
		return;
	// Set new affects
	if (GET_RELIGION(ch) == RELIGION_POLY)
		cur = Poly_apply;
	else
		cur = Mono_apply;
	// log("[GODAFFECT] Start function...");
	for (; cur; cur = cur->next)
		if (cur->gapply_type == GAPPLY_AFFECT) {
			affect_modify(ch, 0, 0, cur->modi, TRUE);
		} else if (cur->gapply_type == GAPPLY_MODIFIER) {
			affect_modify(ch, cur->what, cur->modi, 0, TRUE);
		};
	// log("[GODAFFECT] Stop function...");
}


int char_saved_aff[] = { AFF_GROUP,
	AFF_HORSE,
	0
};

int char_stealth_aff[] = { AFF_HIDE,
	AFF_SNEAK,
	AFF_CAMOUFLAGE,
	0
};
/* ��� �������������� ������ �������� �������� �� ������� */
void affect_room_total(ROOM_DATA * room)
{
	AFFECT_DATA *af;
	// � �� ��� ���� ������ ���������������� �������� �� ������.
	/* ������ ��� ������� ����� (����� ��� ������� �
	   ���������� ��������������) ���� ������ ������
	   ����������� ������������� ������� ������������ �������
	   � OLC , �� �������������� ����������� ������ ����� �������
	   + �� �� �������� ��� ��������. */

	/* �������� ��� ���������� �������������� */
	memset(&room->add_property, 0 , sizeof(room_property_data));

	/* ��������������� �������  */
	for (af = room->affected; af; af = af->next)
		affect_room_modify(room, af->location, af->modifier, af->bitvector, TRUE);

}

/* This updates a character by subtracting everything he is affected by */
/* restoring original abilities, and then affecting all again           */
void affect_total(CHAR_DATA * ch)
{
	AFFECT_DATA *af;
	OBJ_DATA *obj;
	struct extra_affects_type *extra_affect = NULL;
	struct obj_affected_type *extra_modifier = NULL;
	int i, j;
	FLAG_DATA saved;

	// Init struct
	saved.flags[0] = 0;
	saved.flags[1] = 0;
	saved.flags[2] = 0;
	saved.flags[3] = 0;

	// Clear all affect, because recalc one
	memset((char *) &ch->add_abils, 0, sizeof(struct char_played_ability_data));

	// PC's clear all affects, because recalc one
	if (!IS_NPC(ch)) {
		saved = ch->char_specials.saved.affected_by;
		ch->char_specials.saved.affected_by = clear_flags;
		for (i = 0; (j = char_saved_aff[i]); i++)
			if (IS_SET(GET_FLAG(saved, j), j))
				SET_BIT(AFF_FLAGS(ch, j), j);
	}

	/* Restore values for NPC - added by Adept */
	if (IS_NPC(ch)) {
	(ch)->add_abils = (&mob_proto[GET_MOB_RNUM(ch)])->add_abils;
	}

	// Apply some GODS affects and modifiers
	total_gods_affect(ch);

	/* move object modifiers */
	for (i = 0; i < NUM_WEARS; i++) {
		if ((obj = GET_EQ(ch, i))) {
			if (GET_OBJ_TYPE(obj) == ITEM_ARMOR) {
				GET_AC_ADD(ch) -= apply_ac(ch, i);
				GET_ARMOUR(ch) += apply_armour(ch, i);
			}
			/* Update weapon applies */
			for (j = 0; j < MAX_OBJ_AFFECT; j++)
				affect_modify(ch, GET_EQ(ch, i)->affected[j].location,
					      GET_EQ(ch, i)->affected[j].modifier, 0, TRUE);
			/* Update weapon bitvectors */
			for (j = 0; weapon_affect[j].aff_bitvector >= 0; j++) {
				// �� �� �����, �� ����������������
				if (weapon_affect[j].aff_bitvector == 0 || !IS_OBJ_AFF(obj, weapon_affect[j].aff_pos))
					continue;
				affect_modify(ch, APPLY_NONE, 0, weapon_affect[j].aff_bitvector, TRUE);
			}
		}
	}

	/* move features modifiers - added by Gorrah */
	for (i = 1; i < MAX_FEATS; i++) {
		if (can_use_feat(ch, i) && (feat_info[i].type == AFFECT_FTYPE))
			for (j = 0; j < MAX_FEAT_AFFECT; j++)
				affect_modify(ch, feat_info[i].affected[j].location,
								feat_info[i].affected[j].modifier, 0, TRUE);
	}
	/* ��������� "������������" � "������������ �������� */
	/* ����, ��� ���������, ����������, ��� ����� - ������� */
	if (!IS_NPC(ch)) {
		if (can_use_feat(ch, ENDURANCE_FEAT))
			affect_modify(ch, APPLY_MOVE, GET_LEVEL(ch) * 2, 0, TRUE);
		if (can_use_feat(ch, SPLENDID_HEALTH_FEAT))
			affect_modify(ch, APPLY_HIT, GET_LEVEL(ch) * 2, 0, TRUE);
	}

	/* move affect modifiers */
	for (af = ch->affected; af; af = af->next)
		affect_modify(ch, af->location, af->modifier, af->bitvector, TRUE);

	/* move race and class modifiers */
	if (!IS_NPC(ch)) {
		if ((int) GET_CLASS(ch) >= 0 && (int) GET_CLASS(ch) < NUM_CLASSES) {
			extra_affect = class_app[(int) GET_CLASS(ch)].extra_affects;
			//extra_modifier = class_app[(int) GET_CLASS(ch)].extra_modifiers;

			 for (i = 0; extra_affect && (extra_affect + i)->affect != -1; i++)
				affect_modify(ch, APPLY_NONE, 0, (extra_affect + i)->affect,
					      (extra_affect + i)->set_or_clear);
			/* for (i = 0; extra_modifier && (extra_modifier + i)->location != -1; i++)
				affect_modify(ch, (extra_modifier + i)->location,
					      (extra_modifier + i)->modifier, 0, TRUE);*/
		}

		if (GET_RACE(ch) < NUM_RACES) {
			extra_affect = race_app[(int) GET_RACE(ch)].extra_affects;
			extra_modifier = race_app[(int) GET_RACE(ch)].extra_modifiers;
			for (i = 0; extra_affect && (extra_affect + i)->affect != -1; i++)
				affect_modify(ch, APPLY_NONE, 0, (extra_affect + i)->affect,
					      (extra_affect + i)->set_or_clear);
			for (i = 0; extra_modifier && (extra_modifier + i)->location != (byte)-1; i++)
				affect_modify(ch, (extra_modifier + i)->location,
					      (extra_modifier + i)->modifier, 0, TRUE);
		}
		// Apply other PC modifiers
		switch (IS_CARRYING_W(ch) * 10 / MAX(1, CAN_CARRY_W(ch))) {
		case 10:
		case 9:
		case 8:
			GET_DEX_ADD(ch) -= 2;
			break;
		case 7:
		case 6:
		case 5:
			GET_DEX_ADD(ch) -= 1;
			break;
		}
		GET_DR_ADD(ch) += extra_damroll((int) GET_CLASS(ch), (int) GET_LEVEL(ch));
		GET_HITREG(ch) += ((int) GET_LEVEL(ch) + 4) / 5 * 10;
		if (GET_CON_ADD(ch)) {
			i = class_app[(int) GET_CLASS(ch)].koef_con * GET_CON_ADD(ch) * GET_LEVEL(ch) / 100;
			GET_HIT_ADD(ch) += i;
			if ((i = GET_MAX_HIT(ch) + GET_HIT_ADD(ch)) < 1)
				GET_HIT_ADD(ch) -= (i - 1);
		}
		if (!WAITLESS(ch) && on_horse(ch)) {
			REMOVE_BIT(AFF_FLAGS(ch, AFF_HIDE), AFF_HIDE);
			REMOVE_BIT(AFF_FLAGS(ch, AFF_SNEAK), AFF_SNEAK);
			REMOVE_BIT(AFF_FLAGS(ch, AFF_CAMOUFLAGE), AFF_CAMOUFLAGE);
			REMOVE_BIT(AFF_FLAGS(ch, AFF_INVISIBLE), AFF_INVISIBLE);
		}
	}

	/* correctize all weapon */
	if (!IS_NPC(ch) && (obj = GET_EQ(ch, WEAR_BOTHS)) && !IS_IMMORTAL(ch)
	    && !OK_BOTH(ch, obj)) {
		act("��� ������� ������ ������� $o3 � ����� ����� !", FALSE, ch, obj, 0, TO_CHAR);
		act("$n ���������$g ������������ $o3.", FALSE, ch, obj, 0, TO_ROOM);
		obj_to_char(unequip_char(ch, WEAR_BOTHS), ch);
		return;
	}
	if (!IS_NPC(ch) && (obj = GET_EQ(ch, WEAR_WIELD)) && !IS_IMMORTAL(ch)
	    && !OK_WIELD(ch, obj)) {
		act("��� ������� ������ ������� $o3 � ������ ���� !", FALSE, ch, obj, 0, TO_CHAR);
		act("$n ���������$g ������������ $o3.", FALSE, ch, obj, 0, TO_ROOM);
		obj_to_char(unequip_char(ch, WEAR_WIELD), ch);
		return;
	}
	if (!IS_NPC(ch) && (obj = GET_EQ(ch, WEAR_HOLD)) && !IS_IMMORTAL(ch)
	    && !OK_HELD(ch, obj)) {
		act("��� ������� ������ ������� $o3 � ����� ���� !", FALSE, ch, obj, 0, TO_CHAR);
		act("$n ���������$g ������������ $o3.", FALSE, ch, obj, 0, TO_ROOM);
		obj_to_char(unequip_char(ch, WEAR_HOLD), ch);
		return;
	}

	/* calculate DAMAGE value */
	GET_DAMAGE(ch) = (str_app[STRENGTH_APPLY_INDEX(ch)].todam + GET_REAL_DR(ch)) * 2;
	if ((obj = GET_EQ(ch, WEAR_BOTHS)) && GET_OBJ_TYPE(obj) == ITEM_WEAPON)
		GET_DAMAGE(ch) += (GET_OBJ_VAL(obj, 1) * (GET_OBJ_VAL(obj, 2) + 1)) >> 1;
	else {
		if ((obj = GET_EQ(ch, WEAR_WIELD))
		    && GET_OBJ_TYPE(obj) == ITEM_WEAPON)
			GET_DAMAGE(ch) += (GET_OBJ_VAL(obj, 1) * (GET_OBJ_VAL(obj, 2) + 1)) >> 1;
		if ((obj = GET_EQ(ch, WEAR_HOLD)) && GET_OBJ_TYPE(obj) == ITEM_WEAPON)
			GET_DAMAGE(ch) += (GET_OBJ_VAL(obj, 1) * (GET_OBJ_VAL(obj, 2) + 1)) >> 1;
	}

	/* Calculate CASTER value */
	for (GET_CASTER(ch) = 0, i = 1; !IS_NPC(ch) && i <= MAX_SPELLS; i++)
		if (IS_SET(GET_SPELL_TYPE(ch, i), SPELL_KNOW | SPELL_TEMP))
			GET_CASTER(ch) += (spell_info[i].danger * GET_SPELL_MEM(ch, i));

	/* Check steal affects */
	for (i = 0; (j = char_stealth_aff[i]); i++) {
		if (IS_SET(GET_FLAG(saved, j), j) && !IS_SET(AFF_FLAGS(ch, j), j))
			CHECK_AGRO(ch) = TRUE;
	}

	check_berserk(ch);
	if (FIGHTING(ch) || affected_by_spell(ch, SPELL_GLITTERDUST)) {
		REMOVE_BIT(AFF_FLAGS(ch, AFF_HIDE), AFF_HIDE);
		REMOVE_BIT(AFF_FLAGS(ch, AFF_SNEAK), AFF_SNEAK);
		REMOVE_BIT(AFF_FLAGS(ch, AFF_CAMOUFLAGE), AFF_CAMOUFLAGE);
		REMOVE_BIT(AFF_FLAGS(ch, AFF_INVISIBLE), AFF_INVISIBLE);
	}
}

/* ���������� ������ �� �������.
  ������������� ������ ������ ����� */
void affect_to_room(ROOM_DATA * room, AFFECT_DATA * af)
{
	AFFECT_DATA *affected_alloc;

	CREATE(affected_alloc, AFFECT_DATA, 1);

	*affected_alloc = *af;
	affected_alloc->next = room->affected;
	room->affected = affected_alloc;

	affect_room_modify(room, af->location, af->modifier, af->bitvector, TRUE);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Start");
	affect_room_total(room);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Stop");
//	check_light(ch, LIGHT_UNDEF, was_lgt, was_hlgt, was_hdrk, 1);
}



/* Insert an affect_type in a char_data structure
   Automatically sets apropriate bits and apply's */
void affect_to_char(CHAR_DATA * ch, AFFECT_DATA * af)
{
	long was_lgt = AFF_FLAGGED(ch, AFF_SINGLELIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hlgt = AFF_FLAGGED(ch, AFF_HOLYLIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hdrk = AFF_FLAGGED(ch, AFF_HOLYDARK) ? LIGHT_YES : LIGHT_NO;
	AFFECT_DATA *affected_alloc;

	CREATE(affected_alloc, AFFECT_DATA, 1);

	*affected_alloc = *af;
	affected_alloc->next = ch->affected;
	ch->affected = affected_alloc;

	affect_modify(ch, af->location, af->modifier, af->bitvector, TRUE);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Start");
	affect_total(ch);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Stop");
	check_light(ch, LIGHT_UNDEF, was_lgt, was_hlgt, was_hdrk, 1);
}



/*
 * Remove an affected_type structure from a char (called when duration
 * reaches zero). Pointer *af must never be NIL!  Frees mem and calls
 * affect_location_apply
 */

void affect_remove(CHAR_DATA * ch, AFFECT_DATA * af)
{
	int was_lgt = AFF_FLAGGED(ch, AFF_SINGLELIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hlgt = AFF_FLAGGED(ch, AFF_HOLYLIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hdrk = AFF_FLAGGED(ch, AFF_HOLYDARK) ? LIGHT_YES : LIGHT_NO, duration;

	AFFECT_DATA *temp;
	int change = 0;

	// if (IS_IMMORTAL(ch))
	//   {sprintf(buf,"<%d>\r\n",was_hdrk);
	//    send_to_char(buf,ch);
	//   }

	if (ch->affected == NULL) {
		log("SYSERR: affect_remove(%s) when no affects...", GET_NAME(ch));
		// core_dump();
		return;
	}

	affect_modify(ch, af->location, af->modifier, af->bitvector, FALSE);
	if (af->type == SPELL_ABSTINENT) {
		GET_DRUNK_STATE(ch) = GET_COND(ch, DRUNK) = MIN(GET_COND(ch, DRUNK), CHAR_DRUNKED - 1);
	} else if (af->type == SPELL_DRUNKED) {
		duration = pc_duration(ch, 3, MAX(0, GET_DRUNK_STATE(ch) - CHAR_DRUNKED), 0, 0, 0);
		if (can_use_feat(ch, DRUNKARD_FEAT))
			duration /= 2;
		if (af->location == APPLY_AC) {
			af->type = SPELL_ABSTINENT;
			af->duration = duration;
			af->modifier = 20;
			af->bitvector = AFF_ABSTINENT;
			change = 1;
		} else if (af->location == APPLY_HITROLL) {
			af->type = SPELL_ABSTINENT;
			af->duration = duration;
			af->modifier = -2;
			af->bitvector = AFF_ABSTINENT;
			change = 1;
		} else if (af->location == APPLY_DAMROLL) {
			af->type = SPELL_ABSTINENT;
			af->duration = duration;
			af->modifier = -2;
			af->bitvector = AFF_ABSTINENT;
			change = 1;
		}
	}

	if (change)
		affect_modify(ch, af->location, af->modifier, af->bitvector, TRUE);
	else {
		REMOVE_FROM_LIST(af, ch->affected, next);
		free(af);
	}
	//log("[AFFECT_REMOVE->AFFECT_TOTAL] Start");
	affect_total(ch);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Stop");
	check_light(ch, LIGHT_UNDEF, was_lgt, was_hlgt, was_hdrk, 1);
}



void affect_room_remove(ROOM_DATA * room, AFFECT_DATA * af)
{

	AFFECT_DATA *temp;
	int change = 0;

	// if (IS_IMMORTAL(ch))
	//   {sprintf(buf,"<%d>\r\n",was_hdrk);
	//    send_to_char(buf,ch);
	//   }

	if (room->affected == NULL) {
		log("SYSERR: affect_room_remove when no affects...");
		// core_dump();
		return;
	}

	affect_room_modify(room, af->location, af->modifier, af->bitvector, FALSE);
	if (change)
		affect_room_modify(room, af->location, af->modifier, af->bitvector, TRUE);
	else {
		REMOVE_FROM_LIST(af, room->affected, next);
		free(af);
	}
	//log("[AFFECT_REMOVE->AFFECT_TOTAL] Start");
	affect_room_total(room);
	//log("[AFFECT_TO_CHAR->AFFECT_TOTAL] Stop");
//	check_light(ch, LIGHT_UNDEF, was_lgt, was_hlgt, was_hdrk, 1);
}


/* Call affect_remove with every spell of spelltype "skill" */
void affect_from_char(CHAR_DATA * ch, int type)
{
	AFFECT_DATA *hjp, *next;

	for (hjp = ch->affected; hjp; hjp = next) {
		next = hjp->next;
		if (hjp->type == type) {
			affect_remove(ch, hjp);
		}
	}

	if (IS_NPC(ch) && type == SPELL_CHARM)
		EXTRACT_TIMER(ch) = 5;
}

/*
 * Return TRUE if a char is affected by a spell (SPELL_XXX),
 * FALSE indicates not affected.
 */
bool affected_by_spell(CHAR_DATA * ch, int type)
{
	AFFECT_DATA *hjp;

	if (type == SPELL_POWER_HOLD)
		type = SPELL_HOLD;
	else if (type == SPELL_POWER_SIELENCE)
		type = SPELL_SIELENCE;
	else if (type == SPELL_POWER_BLINDNESS)
		type = SPELL_BLINDNESS;


	for (hjp = ch->affected; hjp; hjp = hjp->next)
		if (hjp->type == type)
			return (TRUE);

	return (FALSE);
}
/* ��������� � �� ����� �� �� ������� ����� ��� */
bool room_affected_by_spell(ROOM_DATA * room, int type)
{
	AFFECT_DATA *hjp;

	for (hjp = room->affected; hjp; hjp = hjp->next)
		if (hjp->type == type)
			return (TRUE);

	return (FALSE);
}

void affect_join_fspell(CHAR_DATA * ch, AFFECT_DATA * af)
{
	AFFECT_DATA *hjp;
	bool found = FALSE;

	for (hjp = ch->affected; !found && hjp; hjp = hjp->next) {
		if ((hjp->type == af->type) && (hjp->location == af->location)) {

			if (hjp->modifier < af->modifier)
				hjp->modifier = af->modifier;
			if (hjp->duration < af->duration)
				hjp->duration = af->duration;
			affect_total(ch);
			found = TRUE;
		}
	}
	if (!found) {
		affect_to_char(ch, af);
	}
}
void affect_room_join_fspell(ROOM_DATA * room, AFFECT_DATA * af)
{
	AFFECT_DATA *hjp;
	bool found = FALSE;

	for (hjp = room->affected; !found && hjp; hjp = hjp->next) {
		if ((hjp->type == af->type) && (hjp->location == af->location)) {

			if (hjp->modifier < af->modifier)
				hjp->modifier = af->modifier;
			if (hjp->duration < af->duration)
				hjp->duration = af->duration;
			affect_room_total(room);
			found = TRUE;
		}
	}
	if (!found) {
		affect_to_room(room, af);
	}
}

void affect_room_join(ROOM_DATA * room, AFFECT_DATA * af, bool add_dur, bool avg_dur, bool add_mod, bool avg_mod)
{
	AFFECT_DATA *hjp;
	bool found = FALSE;

	for (hjp = room->affected; !found && hjp && af->location; hjp = hjp->next) {
		if ((hjp->type == af->type) && (hjp->location == af->location)) {
			if (add_dur)
				af->duration += hjp->duration;
			if (avg_dur)
				af->duration /= 2;
			if (add_mod)
				af->modifier += hjp->modifier;
			if (avg_mod)
				af->modifier /= 2;
			affect_room_remove(room, hjp);
			affect_to_room(room, af);
			found = TRUE;
		}
	}
	if (!found) {
		affect_to_room(room, af);
	}
}


void affect_join(CHAR_DATA * ch, AFFECT_DATA * af, bool add_dur, bool avg_dur, bool add_mod, bool avg_mod)
{
	AFFECT_DATA *hjp;
	bool found = FALSE;

	for (hjp = ch->affected; !found && hjp && af->location; hjp = hjp->next) {
		if ((hjp->type == af->type) && (hjp->location == af->location)) {
			if (add_dur)
				af->duration += hjp->duration;
			if (avg_dur)
				af->duration /= 2;
			if (add_mod)
				af->modifier += hjp->modifier;
			if (avg_mod)
				af->modifier /= 2;
			affect_remove(ch, hjp);
			affect_to_char(ch, af);
			found = TRUE;
		}
	}
	if (!found) {
		affect_to_char(ch, af);
	}
}

/* ��������� �������� ������������ - added by Gorrah */
void timed_feat_to_char(CHAR_DATA * ch, struct timed_type *timed)
{
	struct timed_type *timed_alloc, *skj;

	// �������. ������ ����. ���� ����� ��� ��� ���� � ������, ������ ������ ������.
	for (skj = ch->timed; skj; skj = skj->next) {
		if (skj->skill==timed->skill) {
			skj->time=timed->time;
			return;
		}
	}

	CREATE(timed_alloc, struct timed_type, 1);

	*timed_alloc = *timed;
	timed_alloc->next = ch->timed_feat;
	ch->timed_feat = timed_alloc;
}

void timed_feat_from_char(CHAR_DATA * ch, struct timed_type *timed)
{
	struct timed_type *temp;

	if (ch->timed_feat == NULL) {
		log("SYSERR: timed_feat_from_char(%s) when no timed...", GET_NAME(ch));
		return;
	}

	REMOVE_FROM_LIST(timed, ch->timed_feat, next);
	free(timed);
}

int timed_by_feat(CHAR_DATA * ch, int feat)
{
	struct timed_type *hjp;

	for (hjp = ch->timed_feat; hjp; hjp = hjp->next)
		if (hjp->skill == feat)
			return (hjp->time);

	return (0);
}
/* End of changes */

/* Insert an timed_type in a char_data structure */
void timed_to_char(CHAR_DATA * ch, struct timed_type *timed)
{
	struct timed_type *timed_alloc, *skj;

	// �������. ������ ����. ���� ����� ����� ��� ���� � ������, ������ ������ ������.
	for (skj = ch->timed; skj; skj = skj->next) {
		if (skj->skill==timed->skill) {
			skj->time=timed->time;
			return;
		}
	}

	CREATE(timed_alloc, struct timed_type, 1);

	*timed_alloc = *timed;
	timed_alloc->next = ch->timed;
	ch->timed = timed_alloc;
}

void timed_from_char(CHAR_DATA * ch, struct timed_type *timed)
{
	struct timed_type *temp;

	if (ch->timed == NULL) {
		log("SYSERR: timed_from_char(%s) when no timed...", GET_NAME(ch));
		// core_dump();
		return;
	}

	REMOVE_FROM_LIST(timed, ch->timed, next);
	free(timed);
}

int timed_by_skill(CHAR_DATA * ch, int skill)
{
	struct timed_type *hjp;

	for (hjp = ch->timed; hjp; hjp = hjp->next)
		if (hjp->skill == skill)
			return (hjp->time);

	return (0);
}


/* move a player out of a room */
void char_from_room(CHAR_DATA * ch)
{
	CHAR_DATA *temp;

	if (ch == NULL || ch->in_room == NOWHERE) {
		log("SYSERR: NULL character or NOWHERE in %s, char_from_room", __FILE__);
		return;
	}

	if (FIGHTING(ch) != NULL)
		stop_fighting(ch, TRUE);

	check_light(ch, LIGHT_NO, LIGHT_NO, LIGHT_NO, LIGHT_NO, -1);
	REMOVE_FROM_LIST(ch, world[ch->in_room]->people, next_in_room);
	ch->in_room = NOWHERE;
	ch->next_in_room = NULL;
}


/* place a character in a room */
void char_to_room(CHAR_DATA * ch, room_rnum room)
{
	if (ch == NULL || room < NOWHERE + 1 || room > top_of_world)
		log("SYSERR: Illegal value(s) passed to char_to_room. (Room: %d/%d Ch: %p", room, top_of_world, ch);
	else {
		ch->next_in_room = world[room]->people;
		world[room]->people = ch;
		ch->in_room = room;
		check_light(ch, LIGHT_NO, LIGHT_NO, LIGHT_NO, LIGHT_NO, 1);
		REMOVE_BIT(EXTRA_FLAGS(ch, EXTRA_FAILHIDE), EXTRA_FAILHIDE);
		REMOVE_BIT(EXTRA_FLAGS(ch, EXTRA_FAILSNEAK), EXTRA_FAILSNEAK);
		REMOVE_BIT(EXTRA_FLAGS(ch, EXTRA_FAILCAMOUFLAGE), EXTRA_FAILCAMOUFLAGE);
		if (IS_GRGOD(ch) && PRF_FLAGGED(ch, PRF_CODERINFO)) {
			sprintf(buf,
				"%s�������=%s%d %s����=%s%d %s�����=%s%d %s������=%s%d \r\n"
				"%s����=%s%d %s������=%s%d %s������=%s%d %s����=%s%d %s����=%s%d.\r\n",
				CCNRM(ch, C_NRM), CCINRM(ch, C_NRM), room, CCRED(ch,
										 C_NRM),
				CCIRED(ch, C_NRM), world[room]->light, CCGRN(ch, C_NRM),
				CCIGRN(ch, C_NRM), world[room]->glight, CCYEL(ch, C_NRM),
				CCIYEL(ch, C_NRM), world[room]->fires, CCBLU(ch, C_NRM),
				CCIBLU(ch, C_NRM), world[room]->gdark, CCMAG(ch, C_NRM),
				CCIMAG(ch, C_NRM), world[room]->forbidden, CCCYN(ch,
										 C_NRM),
				CCICYN(ch, C_NRM), weather_info.sky, CCWHT(ch, C_NRM),
				CCIWHT(ch, C_NRM), weather_info.sunlight, CCYEL(ch,
										C_NRM),
				CCIYEL(ch, C_NRM), weather_info.moon_day);
			send_to_char(buf, ch);
		}

		/* Stop fighting now, if we left. */
		if (FIGHTING(ch) && IN_ROOM(ch) != IN_ROOM(FIGHTING(ch))) {
			stop_fighting(FIGHTING(ch), FALSE);
			stop_fighting(ch, TRUE);
		}

		if (!IS_NPC(ch)) {
			zone_table[world[room]->zone].used = 1;
			zone_table[world[room]->zone].activity++;
		}
	}
}

void restore_object(OBJ_DATA * obj, CHAR_DATA * ch)
{
	int i, j;
	if ((i = GET_OBJ_RNUM(obj)) < 0)
		return;
	if (GET_OBJ_OWNER(obj) && OBJ_FLAGGED(obj, ITEM_NODONATE) && (!ch || GET_UNIQUE(ch) != GET_OBJ_OWNER(obj))) {
		GET_OBJ_VAL(obj, 0) = GET_OBJ_VAL(obj_proto[i], 0);
		GET_OBJ_VAL(obj, 1) = GET_OBJ_VAL(obj_proto[i], 1);
		GET_OBJ_VAL(obj, 2) = GET_OBJ_VAL(obj_proto[i], 2);
		GET_OBJ_VAL(obj, 3) = GET_OBJ_VAL(obj_proto[i], 3);
		GET_OBJ_MATER(obj) = GET_OBJ_MATER(obj_proto[i]);
		GET_OBJ_MAX(obj) = GET_OBJ_MAX(obj_proto[i]);
		GET_OBJ_CUR(obj) = 1;
		GET_OBJ_WEIGHT(obj) = GET_OBJ_WEIGHT(obj_proto[i]);
		GET_OBJ_TIMER(obj) = 24 * 60;
		obj->obj_flags.extra_flags = obj_proto[i]->obj_flags.extra_flags;
		obj->obj_flags.affects = obj_proto[i]->obj_flags.affects;
		GET_OBJ_WEAR(obj) = GET_OBJ_WEAR(obj_proto[i]);
		GET_OBJ_OWNER(obj) = 0;
		for (j = 0; j < MAX_OBJ_AFFECT; j++)
			obj->affected[j] = obj_proto[i]->affected[j];
	}
}

// ��������� ��������� �� ��������
bool equal_obj(OBJ_DATA *obj_one, OBJ_DATA *obj_two)
{
	if (GET_OBJ_VNUM(obj_one) != GET_OBJ_VNUM(obj_two)
		|| (GET_OBJ_TYPE(obj_one) == ITEM_DRINKCON && GET_OBJ_VAL(obj_one, 2) != GET_OBJ_VAL(obj_two, 2))
		|| (GET_OBJ_TYPE(obj_one) == ITEM_CONTAINER && (obj_one->contains || obj_two->contains))
		|| GET_OBJ_VNUM(obj_two) == -1
		|| (GET_OBJ_TYPE(obj_one) == ITEM_MING && strcmp(obj_one->name, obj_two->name)))
	{
		return 0;
	}
	return 1;
}

namespace {

// ���������� ����������� �������� ����� ���������� � ������ ������ obj
void insert_obj_and_group(OBJ_DATA *obj, OBJ_DATA **list_start)
{
	// AL: �������� �)
	// Krodo: ��������� ������ ���, �� ��������� � ����� � ���� �)

	// begin - ������ ������� � �������� ������
	// end - ��������� ������� � ������������ ���������
	// before - ��������� ������� ����� ������� ���������
	OBJ_DATA *p, *begin, *end, *before;

	obj->next_content = begin = *list_start;
	*list_start = obj;

	// ������� ������� ��� ������ � ������ ��� ������ ������
	if (!begin || equal_obj(begin, obj)) return;

	before = p = begin;

	while (p && !equal_obj(p, obj))
		before = p, p = p->next_content;

	// ��� ������� ���������
	if (!p) return;

	end = p;

	while (p && equal_obj(p, obj))
		end = p, p = p->next_content;

	end->next_content = begin;
	obj->next_content = before->next_content;
	before->next_content = p; // ����� 0 ���� ����� ������������ ������ �� ������
}

} // no-name namespace

/* give an object to a char   */
void obj_to_char(OBJ_DATA * object, CHAR_DATA * ch)
{
	OBJ_DATA *i;
	unsigned int tuid;
	int inworld;

	int may_carry = TRUE;
#ifdef PROOL
	printf("obj_to_char() #1\n");
#endif		
	if (object && ch) {
#ifdef PROOL
		printf("obj_to_char() #2\n");
#endif		
		restore_object(object, ch);
#ifdef PROOL
		printf("obj_to_char() #3\n");
#endif	
#ifndef VIRTUSTAN	
		if (invalid_anti_class(ch, object) || invalid_unique(ch, object))
			may_carry = FALSE;
#endif

#ifdef PROOL
		printf("obj_to_char() #4\n");
#endif		
		if (strstr(object->name,"clan")) {
			if (!CLAN(ch))
				may_carry = FALSE;
			else {
				char buf[128];
				sprintf (buf,"clan%d!",CLAN(ch)->GetRent());
				if (!strstr(object->name,buf))
					may_carry = FALSE;
			}
		}

#ifdef PROOL
		printf("obj_to_char() #5\n");
#endif		
		if (!may_carry) {
			act("��� ������� ��� ������� ����� $o3.", FALSE, ch, object, 0, TO_CHAR);
			act("$n �������$u ����� $o3 - � ����� �� ������$g.", FALSE, ch, object, 0, TO_ROOM);
			obj_to_room(object, IN_ROOM(ch));
			return;
		}

#ifdef PROOL
		printf("obj_to_char() #6\n");
#endif		
		if (!IS_NPC(ch)) {
			// �������� ������������ ���������
			if (object && // ������ ����������
				GET_OBJ_UID(object) != 0 && // ���� UID
				GET_OBJ_TIMER(object)>0) { // ���������
				tuid = GET_OBJ_UID(object);
				inworld = 1;
				// ������ ����� ��� ��������. ���� � ���� ����� ��.
   				for (i = object_list; i; i = i->next) {
      				if (GET_OBJ_UID(i) == tuid && // UID ���������
						GET_OBJ_TIMER(i)>0 && // ���������
						object!=i && // �� ��� ��
						GET_OBJ_VNUM(i) == GET_OBJ_VNUM(object)) { // ��� ��������
						inworld++;
					}
				}
				if (inworld>1) { // � ������� ���� ��� ������� ���� �����
					sprintf(buf, "Copy detected and prepared to extract! Object %s (UID=%d, VNUM=%d), holder %s. In world %d.",
							object->PNames[0], GET_OBJ_UID(object), GET_OBJ_VNUM(object), GET_NAME(ch), inworld);
					mudlog(buf, BRF, LVL_IMMORT, SYSLOG, TRUE);
					// �������� ��������
					act("$o0 �������$Q � �� ������� �������� ������������ ���� 'DUPE'.", FALSE, ch, object, 0, TO_CHAR);
					GET_OBJ_TIMER(object) = 0; // ���� ��������, ���������� �� ����
   					SET_BIT(GET_OBJ_EXTRA(object, ITEM_NOSELL), ITEM_NOSELL); // ��� �����
				}
			} // ��������� UID
			else if (GET_OBJ_VNUM(object) > 0 && // ������ �� �����������
					 GET_OBJ_UID(object) == 0) { // � ������� ����� ��� ����
				global_uid++; // ����������� ���������� ������� �����
				global_uid = global_uid==0 ? 1 : global_uid; // ���� ��������� ������������ ����
				GET_OBJ_UID(object) = global_uid; // ��������� ���
			}
		}

#ifdef PROOL
		printf("obj_to_char() #7\n");
#endif		
		if (!IS_NPC(ch) || (ch->master && !IS_NPC(ch->master))) {
			SET_BIT(GET_OBJ_EXTRA(object, ITEM_TICKTIMER), ITEM_TICKTIMER);
			insert_obj_and_group(object, &ch->carrying);
		} else {
			// ��� ��� ����, ����� �������� ������ ������� ��������� �� ������� ��������� � ���� ���� // Krodo
			object->next_content = ch->carrying;
			ch->carrying = object;
		}

#ifdef PROOL
		printf("obj_to_char() #8\n");
#endif		
		object->carried_by = ch;
		object->in_room = NOWHERE;
#ifdef PROOL
		printf("obj_to_char() #9\n");
#endif		
		IS_CARRYING_W(ch) += GET_OBJ_WEIGHT(object);
#ifdef PROOL
		printf("obj_to_char() #10\n");
#endif		
		IS_CARRYING_N(ch)++;

		/* set flag for crash-save system, but not on mobs! */
		if (!IS_NPC(ch))
			SET_BIT(PLR_FLAGS(ch, PLR_CRASH), PLR_CRASH);

	} else
		log("SYSERR: NULL obj (%p) or char (%p) passed to obj_to_char.", object, ch);
#ifdef PROOL
		printf("obj_to_char() end\n");
#endif		
}


/* take an object from a char */
void obj_from_char(OBJ_DATA * object)
{
	OBJ_DATA *temp;

	if (object == NULL) {
		log("SYSERR: NULL object passed to obj_from_char.");
		return;
	}
	REMOVE_FROM_LIST(object, object->carried_by->carrying, next_content);

	/* set flag for crash-save system, but not on mobs! */
	if (!IS_NPC(object->carried_by))
		SET_BIT(PLR_FLAGS(object->carried_by, PLR_CRASH), PLR_CRASH);

	IS_CARRYING_W(object->carried_by) -= GET_OBJ_WEIGHT(object);
	IS_CARRYING_N(object->carried_by)--;
	object->carried_by = NULL;
	object->next_content = NULL;
}



/* Return the effect of a piece of armor in position eq_pos */
int apply_ac(CHAR_DATA * ch, int eq_pos)
{
	int factor = 1;

	if (GET_EQ(ch, eq_pos) == NULL) {
		log("SYSERR: apply_ac(%s,%d) when no equip...", GET_NAME(ch), eq_pos);
		// core_dump();
		return (0);
	}

	if (!(GET_OBJ_TYPE(GET_EQ(ch, eq_pos)) == ITEM_ARMOR))
		return (0);

	switch (eq_pos) {
	case WEAR_BODY:
		factor = 3;
		break;		/* 30% */
	case WEAR_HEAD:
		factor = 2;
		break;		/* 20% */
	case WEAR_LEGS:
		factor = 2;
		break;		/* 20% */
	default:
		factor = 1;
		break;		/* all others 10% */
	}

	if (IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM))
		factor *= MOB_AC_MULT;

	return (factor * GET_OBJ_VAL(GET_EQ(ch, eq_pos), 0));
}

int apply_armour(CHAR_DATA * ch, int eq_pos)
{
	int factor = 1;
	OBJ_DATA *obj = GET_EQ(ch, eq_pos);

	if (!obj) {
		log("SYSERR: apply_armor(%s,%d) when no equip...", GET_NAME(ch), eq_pos);
		// core_dump();
		return (0);
	}

	if (!(GET_OBJ_TYPE(obj) == ITEM_ARMOR))
		return (0);

	switch (eq_pos) {
	case WEAR_BODY:
		factor = 3;
		break;		/* 30% */
	case WEAR_HEAD:
		factor = 2;
		break;		/* 20% */
	case WEAR_LEGS:
		factor = 2;
		break;		/* 20% */
	default:
		factor = 1;
		break;		/* all others 10% */
	}

	if (IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM))
		factor *= MOB_ARMOUR_MULT;

	return (factor * GET_OBJ_VAL(obj, 1) * GET_OBJ_CUR(obj) / MAX(1, GET_OBJ_MAX(obj)));
}

int invalid_align(CHAR_DATA * ch, OBJ_DATA * obj)
{
	if (IS_NPC(ch) || IS_IMMORTAL(ch))
		return (FALSE);
	if (IS_OBJ_ANTI(obj, ITEM_AN_MONO) && GET_RELIGION(ch) == RELIGION_MONO)
		return TRUE;
	if (IS_OBJ_ANTI(obj, ITEM_AN_POLY) && GET_RELIGION(ch) == RELIGION_POLY)
		return TRUE;
	return FALSE;
}

void wear_message(CHAR_DATA * ch, OBJ_DATA * obj, int where)
{
	const char *wear_messages[][2] = {
		{"$n ��������$g $o3 � ����$g �� ������ ����.",
		 "�� ������ $o3 � ����� �� ������ ����."},

		{"$n0 �����$g $o3 �� ������ ������������ �����.",
		 "�� ������ $o3 �� ������ ������������ �����."},

		{"$n0 �����$g $o3 �� ����� ������������ �����.",
		 "�� ������ $o3 �� ����� ������������ �����."},

		{"$n0 �����$g $o3 ������ ���.",
		 "�� ������ $o3 ������ ���."},

		{"$n0 �����$g $o3 �� �����.",
		 "�� ������ $o3 �� �����."},

		{"$n0 �����$g $o3 �� ��������.",
		 "�� ������ $o3 �� ��������.",},

		{"$n0 ��������$g $o3 �� ������.",
		 "�� ��������� $o3 ���� �� ������."},

		{"$n0 �����$g $o3 �� ����.",
		 "�� ������ $o3 �� ����."},

		{"$n0 ����$g $o3.",
		 "�� ����� $o3."},

		{"$n0 �����$g $o3 �� �����.",
		 "�� ������ $o3 �� �����."},

		{"$n0 �����$g $o3 �� ����.",
		 "�� ������ $o3 �� ����."},

		{"$n0 �����$g ������������ $o3 ��� ���.",
		 "�� ������ ������������ $o3 ��� ���."},

		{"$n0 �������$u � $o3.",
		 "�� ���������� � $o3."},

		{"$n0 �����$g $o3 ������ �����.",
		 "�� ������ $o3 ������ �����."},

		{"$n0 �����$g $o3 ������ ������� ��������.",
		 "�� ������ $o3 ������ ������� ��������."},

		{"$n0 �����$g $o3 ������ ������ ��������.",
		 "�� ������ $o3 ������ ������ ��������."},

		{"$n0 ����$g � ������ ���� $o3.",
		 "�� ����������� $o4."},

		{"$n0 ����$g $o3 � ����� ����.",
		 "�� ����� $o3 � ����� ����."},

		{"$n0 ����$g $o3 � ��� ����.",
		 "�� ����� $o3 � ��� ����."}
	};

	act(wear_messages[where][1], FALSE, ch, obj, 0, TO_CHAR);
	act(wear_messages[where][0], IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM) ? FALSE : TRUE, ch, obj, 0, TO_ROOM);
}

int flag_data_by_char_class(const CHAR_DATA * ch)
{
	if (ch == NULL)
		return 0;

	return flag_data_by_num(IS_NPC(ch) ? NUM_CLASSES * NUM_KIN : GET_CLASS(ch) + NUM_CLASSES * GET_KIN(ch));
}

unsigned int activate_stuff(CHAR_DATA * ch, OBJ_DATA * obj,
			    id_to_set_info_map::const_iterator it, int pos, unsigned int set_obj_qty)
{
	int show_msg = IS_SET(pos, 0x80), no_cast = IS_SET(pos, 0x40);
	std::string::size_type delim;

	REMOVE_BIT(pos, (0x80 | 0x40));

	if (pos < NUM_WEARS) {
		set_info::const_iterator set_obj_info;

		if (GET_EQ(ch, pos) && OBJ_FLAGGED(GET_EQ(ch, pos), ITEM_SETSTUFF) &&
		    (set_obj_info = it->second.find(GET_OBJ_VNUM(GET_EQ(ch, pos)))) != it->second.end()) {
			unsigned int oqty = activate_stuff(ch, obj, it, pos + 1 | (show_msg ? 0x80 : 0) | (no_cast ? 0x40 : 0),
							   set_obj_qty + 1);
			qty_to_camap_map::const_iterator qty_info = set_obj_info->second.upper_bound(oqty);
			qty_to_camap_map::const_iterator old_qty_info = GET_EQ(ch, pos) == obj ?
								 set_obj_info->second.begin() :
								 set_obj_info->second.upper_bound(oqty - 1);

			while (qty_info != old_qty_info) {
				class_to_act_map::const_iterator class_info;

				qty_info--;
				if ((class_info =
				     qty_info->second.find(unique_bit_flag_data().set_plane(flag_data_by_char_class(ch)))) !=
				    qty_info->second.end()) {
					if (GET_EQ(ch, pos) != obj) {
						for (int i = 0; i < MAX_OBJ_AFFECT; i++)
							affect_modify(ch, GET_EQ(ch, pos)->affected[i].location, GET_EQ(ch, pos)->affected[i].modifier,
								      0, FALSE);

						if (IN_ROOM(ch) != NOWHERE)
							for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
								if (weapon_affect[i].aff_bitvector == 0 ||
								    !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
									continue;
								affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, FALSE);
							}
					}

					std::string act_msg = GET_EQ(ch, pos)->activate_obj(class_info->second);
					delim = act_msg.find('\n');

					if (show_msg) {
						act(act_msg.substr(0, delim).c_str(), FALSE, ch, GET_EQ(ch, pos), 0, TO_CHAR);
						act(act_msg.erase(0, delim + 1).c_str(),
						    IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM) ? FALSE : TRUE,
						    ch, GET_EQ(ch, pos), 0, TO_ROOM);
					}

					for (int i = 0; i < MAX_OBJ_AFFECT; i++)
						affect_modify(ch, GET_EQ(ch, pos)->affected[i].location, GET_EQ(ch, pos)->affected[i].modifier, 0, TRUE);

					if (IN_ROOM(ch) != NOWHERE)
						for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
							if (weapon_affect[i].aff_spell == 0 || !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
								continue;
							if (!no_cast)
								if (ROOM_FLAGGED(IN_ROOM(ch), ROOM_NOMAGIC)) {
									act("����� $o1 ��������� ������� � ���������� �� �������",
									    FALSE, ch, GET_EQ(ch, pos), 0, TO_ROOM);
									act("����� $o1 ��������� ������� � ���������� �� �������",
									    FALSE, ch, GET_EQ(ch, pos), 0, TO_CHAR);
								} else
									mag_affects(GET_LEVEL(ch), ch, ch, weapon_affect[i].aff_spell,
										    SAVING_WILL);
						}

					return oqty;
				}
			}

			if (GET_EQ(ch, pos) == obj) {
				for (int i = 0; i < MAX_OBJ_AFFECT; i++)
					affect_modify(ch, obj->affected[i].location, obj->affected[i].modifier, 0, TRUE);

				if (IN_ROOM(ch) != NOWHERE)
					for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
						if (weapon_affect[i].aff_spell == 0 || !IS_OBJ_AFF(obj, weapon_affect[i].aff_pos))
							continue;
						if (!no_cast)
							if (ROOM_FLAGGED(IN_ROOM(ch), ROOM_NOMAGIC)) {
								act("����� $o1 ��������� ������� � ���������� �� �������",
								    FALSE, ch, obj, 0, TO_ROOM);
								act("����� $o1 ��������� ������� � ���������� �� �������",
								    FALSE, ch, obj, 0, TO_CHAR);
							} else
								mag_affects(GET_LEVEL(ch), ch, ch, weapon_affect[i].aff_spell,
									    SAVING_WILL);
					}
			}

			return oqty;
		} else
			return activate_stuff(ch, obj, it, pos + 1 | (show_msg ? 0x80 : 0) | (no_cast ? 0x40 : 0), set_obj_qty);
	} else
		return set_obj_qty;
}

//  0x40 - no spell casting
//  0x80 - no total affect update
// 0x100 - show wear and activation messages
void equip_char(CHAR_DATA * ch, OBJ_DATA * obj, int pos)
{
	int was_lgt = AFF_FLAGGED(ch, AFF_SINGLELIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hlgt = AFF_FLAGGED(ch, AFF_HOLYLIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hdrk = AFF_FLAGGED(ch, AFF_HOLYDARK) ? LIGHT_YES : LIGHT_NO, was_lamp = FALSE;
	int j, show_msg = IS_SET(pos, 0x100), skip_total = IS_SET(pos, 0x80),
	    no_cast = IS_SET(pos, 0x40);

	REMOVE_BIT(pos, (0x100 | 0x80 | 0x40));

	if (pos < 0 || pos >= NUM_WEARS) {
		log("SYSERR: equip_char(%s,%d) in unknown pos...", GET_NAME(ch), pos);
		return;
	}

	if (GET_EQ(ch, pos)) {
		log("SYSERR: Char is already equipped: %s, %s", GET_NAME(ch), obj->short_description);
		return;
	}
	//if (obj->carried_by) {
	//	log("SYSERR: EQUIP: %s - Obj is carried_by when equip.", OBJN(obj, ch, 0));
	//	return;
	//}
	if (obj->in_room != NOWHERE) {
		log("SYSERR: EQUIP: %s - Obj is in_room when equip.", OBJN(obj, ch, 0));
		return;
	}

	if (invalid_anti_class(ch, obj)) {
		act("��� ������� ��� ������� ������ $o3.", FALSE, ch, obj, 0, TO_CHAR);
		act("$n �������$u ������ $o3 - � ����� �� �������$g.", FALSE, ch, obj, 0, TO_ROOM);
		obj_from_char(obj);
		obj_to_room(obj, IN_ROOM(ch));
		obj_decay(obj);
		return;
	}

	if (!IS_NPC(ch) && invalid_align(ch, obj) ||
	    invalid_no_class(ch, obj) ||
	    AFF_FLAGGED(ch, AFF_CHARM) && (OBJ_FLAGGED(obj, ITEM_SHARPEN) || OBJ_FLAGGED(obj, ITEM_ARMORED))) {
		act("$o3 ���� �� ������������$A ��� ���.", FALSE, ch, obj, 0, TO_CHAR);
		act("$n �������$u ������ $o3, �� � �$s ������ �� ����������.", FALSE, ch, obj, 0, TO_ROOM);
		//obj_to_char(obj, ch);
		return;
	}

	if (obj->carried_by)
		obj_from_char(obj);

	if (GET_EQ(ch, WEAR_LIGHT) &&
	    GET_OBJ_TYPE(GET_EQ(ch, WEAR_LIGHT)) == ITEM_LIGHT && GET_OBJ_VAL(GET_EQ(ch, WEAR_LIGHT), 2))
		was_lamp = TRUE;

	GET_EQ(ch, pos) = obj;
	obj->worn_by = ch;
	obj->worn_on = pos;
	obj->next_content = NULL;
	CHECK_AGRO(ch) = TRUE;

	if (show_msg)
		wear_message(ch, obj, pos);

	if (ch->in_room == NOWHERE)
		log("SYSERR: ch->in_room = NOWHERE when equipping char %s.", GET_NAME(ch));

	id_to_set_info_map::iterator it = obj_data::set_table.begin();

	if (OBJ_FLAGGED(obj, ITEM_SETSTUFF))
		for (; it != obj_data::set_table.end(); it++)
			if (it->second.find(GET_OBJ_VNUM(obj)) != it->second.end()) {
				activate_stuff(ch, obj, it, 0 | (show_msg ? 0x80 : 0) | (no_cast ? 0x40 : 0), 0);
				break;
			}

	if (!OBJ_FLAGGED(obj, ITEM_SETSTUFF) || it == obj_data::set_table.end()) {
		for (j = 0; j < MAX_OBJ_AFFECT; j++)
			affect_modify(ch, obj->affected[j].location, obj->affected[j].modifier, 0, TRUE);

		if (IN_ROOM(ch) != NOWHERE)
			for (j = 0; weapon_affect[j].aff_bitvector >= 0; j++) {
				if (weapon_affect[j].aff_spell == 0 || !IS_OBJ_AFF(obj, weapon_affect[j].aff_pos))
					continue;
				if (!no_cast)
					if (ROOM_FLAGGED(IN_ROOM(ch), ROOM_NOMAGIC)) {
						act("����� $o1 ��������� ������� � ���������� �� �������",
						    FALSE, ch, obj, 0, TO_ROOM);
						act("����� $o1 ��������� ������� � ���������� �� �������",
						    FALSE, ch, obj, 0, TO_CHAR);
					} else
						mag_affects(GET_LEVEL(ch), ch, ch, weapon_affect[j].aff_spell, SAVING_WILL);
			}
	}

	if (!skip_total) {
		affect_total(ch);
		check_light(ch, was_lamp, was_lgt, was_hlgt, was_hdrk, 1);
	}
}

unsigned int deactivate_stuff(CHAR_DATA * ch, OBJ_DATA * obj,
			    id_to_set_info_map::const_iterator it, int pos, unsigned int set_obj_qty)
{
	int show_msg = IS_SET(pos, 0x40);
	std::string::size_type delim;

	REMOVE_BIT(pos, 0x40);

	if (pos < NUM_WEARS) {
		set_info::const_iterator set_obj_info;

		if (GET_EQ(ch, pos) && OBJ_FLAGGED(GET_EQ(ch, pos), ITEM_SETSTUFF) &&
		    (set_obj_info = it->second.find(GET_OBJ_VNUM(GET_EQ(ch, pos)))) != it->second.end()) {
			unsigned int oqty = deactivate_stuff(ch, obj, it, pos + 1 | (show_msg ? 0x40 : 0),
							   set_obj_qty + 1);
			qty_to_camap_map::const_iterator old_qty_info = set_obj_info->second.upper_bound(oqty);
			qty_to_camap_map::const_iterator qty_info = GET_EQ(ch, pos) == obj ?
								 set_obj_info->second.begin() :
								 set_obj_info->second.upper_bound(oqty - 1);

			while (old_qty_info != qty_info) {
				class_to_act_map::const_iterator class_info;

				old_qty_info--;
				if ((class_info =
				     old_qty_info->second.find(unique_bit_flag_data().set_plane(flag_data_by_char_class(ch)))) !=
				    old_qty_info->second.end()) {
					while (qty_info != set_obj_info->second.begin()) {
						class_to_act_map::const_iterator class_info2;

						qty_info--;
						if ((class_info2 =
						     qty_info->second.find(unique_bit_flag_data().set_plane(flag_data_by_char_class(ch)))) !=
						    qty_info->second.end()) {
							for (int i = 0; i < MAX_OBJ_AFFECT; i++)
								affect_modify(ch, GET_EQ(ch, pos)->affected[i].location,
									      GET_EQ(ch, pos)->affected[i].modifier, 0, FALSE);

							if (IN_ROOM(ch) != NOWHERE)
								for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
									if (weapon_affect[i].aff_bitvector == 0 ||
									    !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
										continue;
									affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, FALSE);
								}

							std::string act_msg = GET_EQ(ch, pos)->activate_obj(class_info2->second);
							delim = act_msg.find('\n');

							if (show_msg) {
								act(act_msg.substr(0, delim).c_str(), FALSE, ch, GET_EQ(ch, pos), 0, TO_CHAR);
								act(act_msg.erase(0, delim + 1).c_str(),
								    IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM) ? FALSE : TRUE,
								    ch, GET_EQ(ch, pos), 0, TO_ROOM);
							}

							for (int i = 0; i < MAX_OBJ_AFFECT; i++)
								affect_modify(ch, GET_EQ(ch, pos)->affected[i].location,
									      GET_EQ(ch, pos)->affected[i].modifier, 0, TRUE);

							if (IN_ROOM(ch) != NOWHERE)
								for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
									if (weapon_affect[i].aff_bitvector == 0 ||
									    !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
										continue;
									affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, TRUE);
								}

							return oqty;
						}
					}

					for (int i = 0; i < MAX_OBJ_AFFECT; i++)
						affect_modify(ch, GET_EQ(ch, pos)->affected[i].location,
							      GET_EQ(ch, pos)->affected[i].modifier, 0, FALSE);

					if (IN_ROOM(ch) != NOWHERE)
						for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
							if (weapon_affect[i].aff_bitvector == 0 ||
							    !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
								continue;
							affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, FALSE);
						}

					std::string deact_msg = GET_EQ(ch, pos)->deactivate_obj(class_info->second);
					delim = deact_msg.find('\n');

					if (show_msg) {
						act(deact_msg.substr(0, delim).c_str(), FALSE, ch, GET_EQ(ch, pos), 0, TO_CHAR);
						act(deact_msg.erase(0, delim + 1).c_str(),
						    IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM) ? FALSE : TRUE,
						    ch, GET_EQ(ch, pos), 0, TO_ROOM);
					}

					if (GET_EQ(ch, pos) != obj) {
						for (int i = 0; i < MAX_OBJ_AFFECT; i++)
							affect_modify(ch, GET_EQ(ch, pos)->affected[i].location, GET_EQ(ch, pos)->affected[i].modifier,
								      0, TRUE);

						if (IN_ROOM(ch) != NOWHERE)
							for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
								if (weapon_affect[i].aff_bitvector == 0 ||
								    !IS_OBJ_AFF(GET_EQ(ch, pos), weapon_affect[i].aff_pos))
									continue;
								affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, TRUE);
							}
					}

					return oqty;
				}
			}

			if (GET_EQ(ch, pos) == obj) {
				for (int i = 0; i < MAX_OBJ_AFFECT; i++)
					affect_modify(ch, obj->affected[i].location, obj->affected[i].modifier, 0, FALSE);

				if (IN_ROOM(ch) != NOWHERE)
					for (int i = 0; weapon_affect[i].aff_bitvector >= 0; i++) {
						if (weapon_affect[i].aff_bitvector == 0 || !IS_OBJ_AFF(obj, weapon_affect[i].aff_pos))
							continue;
						affect_modify(ch, APPLY_NONE, 0, weapon_affect[i].aff_bitvector, FALSE);
					}

				obj->deactivate_obj(activation());
			}

			return oqty;
		} else
			return deactivate_stuff(ch, obj, it, pos + 1 | (show_msg ? 0x40 : 0), set_obj_qty);
	} else
		return set_obj_qty;
}

//  0x40 - show setstuff related messages
//  0x80 - no total affect update
OBJ_DATA *unequip_char(CHAR_DATA * ch, int pos)
{
	int was_lgt = AFF_FLAGGED(ch, AFF_SINGLELIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hlgt = AFF_FLAGGED(ch, AFF_HOLYLIGHT) ? LIGHT_YES : LIGHT_NO,
	    was_hdrk = AFF_FLAGGED(ch, AFF_HOLYDARK) ? LIGHT_YES : LIGHT_NO, was_lamp = FALSE;

	int j, skip_total = IS_SET(pos, 0x80), show_msg = IS_SET(pos, 0x40);

	OBJ_DATA *obj;

	REMOVE_BIT(pos, (0x80 | 0x40));

	if ((pos < 0 || pos >= NUM_WEARS) || (obj = GET_EQ(ch, pos)) == NULL) {
		log("SYSERR: unequip_char(%s,%d) - unused pos or no equip...", GET_NAME(ch), pos);
		return (NULL);
	}

	if (GET_EQ(ch, WEAR_LIGHT) &&
	    GET_OBJ_TYPE(GET_EQ(ch, WEAR_LIGHT)) == ITEM_LIGHT && GET_OBJ_VAL(GET_EQ(ch, WEAR_LIGHT), 2))
		was_lamp = TRUE;

	if (ch->in_room == NOWHERE)
		log("SYSERR: ch->in_room = NOWHERE when unequipping char %s.", GET_NAME(ch));

	id_to_set_info_map::iterator it = obj_data::set_table.begin();

	if (OBJ_FLAGGED(obj, ITEM_SETSTUFF))
		for (; it != obj_data::set_table.end(); it++)
			if (it->second.find(GET_OBJ_VNUM(obj)) != it->second.end()) {
				deactivate_stuff(ch, obj, it, 0 | (show_msg ? 0x40 : 0), 0);
				break;
			}

	if (!OBJ_FLAGGED(obj, ITEM_SETSTUFF) || it == obj_data::set_table.end()) {
		for (j = 0; j < MAX_OBJ_AFFECT; j++)
			affect_modify(ch, obj->affected[j].location, obj->affected[j].modifier, 0, FALSE);

		if (IN_ROOM(ch) != NOWHERE)
			for (j = 0; weapon_affect[j].aff_bitvector >= 0; j++) {
				if (weapon_affect[j].aff_bitvector == 0 || !IS_OBJ_AFF(obj, weapon_affect[j].aff_pos))
					continue;
				affect_modify(ch, APPLY_NONE, 0, weapon_affect[j].aff_bitvector, FALSE);
			}

		if (OBJ_FLAGGED(obj, ITEM_SETSTUFF))
			obj->deactivate_obj(activation());
	}

	GET_EQ(ch, pos) = NULL;
	obj->worn_by = NULL;
	obj->worn_on = NOWHERE;
	obj->next_content = NULL;

	if (!skip_total) {
		affect_total(ch);
		check_light(ch, was_lamp, was_lgt, was_hlgt, was_hdrk, 1);
	}

	return (obj);
}


int get_number(char **name)
{
	int i;
	char *ppos;
	char number[MAX_INPUT_LENGTH];

	*number = '\0';

	if ((ppos = strchr(*name, '.')) != NULL) {
		*ppos = '\0';
		strcpy(number, *name);
		for (i = 0; *(number + i); i++) {
			if (!isdigit(*(number + i))) {
				*ppos = '.';
				return (1);
			}
		}
		strcpy(*name, ppos + 1);
		return (atoi(number));
	}
	return (1);
}



/* Search a given list for an object number, and return a ptr to that obj */
OBJ_DATA *get_obj_in_list_num(int num, OBJ_DATA * list)
{
	OBJ_DATA *i;

	for (i = list; i; i = i->next_content)
		if (GET_OBJ_RNUM(i) == num)
			return (i);

	return (NULL);
}

/* Search a given list for an object virtul_number, and return a ptr to that obj */
OBJ_DATA *get_obj_in_list_vnum(int num, OBJ_DATA * list)
{
	OBJ_DATA *i;

	for (i = list; i; i = i->next_content)
		if (GET_OBJ_VNUM(i) == num)
			return (i);

	return (NULL);
}


/* search the entire world for an object number, and return a pointer  */
OBJ_DATA *get_obj_num(obj_rnum nr)
{
	OBJ_DATA *i;

	for (i = object_list; i; i = i->next)
		if (GET_OBJ_RNUM(i) == nr)
			return (i);

	return (NULL);
}



/* search a room for a char, and return a pointer if found..  */
CHAR_DATA *get_char_room(char *name, room_rnum room)
{
	CHAR_DATA *i;
	int j = 0, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	strcpy(tmp, name);
	if (!(number = get_number(&tmp)))
		return (NULL);

	for (i = world[room]->people; i && (j <= number); i = i->next_in_room)
		if (isname(tmp, i->player.name))
			if (++j == number)
				return (i);

	return (NULL);
}



/* search all over the world for a char num, and return a pointer if found */
CHAR_DATA *get_char_num(mob_rnum nr)
{
	CHAR_DATA *i;

	for (i = character_list; i; i = i->next)
		if (GET_MOB_RNUM(i) == nr)
			return (i);

	return (NULL);
}


const int money_destroy_timer = 60;
const int death_destroy_timer = 5;
const int room_destroy_timer = 10;
const int room_nodestroy_timer = -1;
const int script_destroy_timer = 1; /** !!! Never set less than ONE **/

/* put an object in a room */
void obj_to_room(OBJ_DATA * object, room_rnum room)
{
	int sect = 0;
#ifdef PROOL
	printf("obj_to_room() 1\n");
#endif
	if (!object || room < FIRST_ROOM || room > top_of_world) {
#ifdef PROOL
		printf("obj_to_room() 1.1\n");
#endif
		log("SYSERR: Illegal value(s) passed to obj_to_room. (Room #%d/%d, obj %p)",
		    room, top_of_world, object);
		if (object) {
#ifdef PROOL
			printf("obj_to_room() 1.2\n");
#endif
			extract_obj(object);
			}
	} else {
#ifdef PROOL
		printf("obj_to_room() 1.3\n");
#endif
		restore_object(object, 0);
#ifdef PROOL
		printf("obj_to_room() 1.4\n");
#endif
		insert_obj_and_group(object, &world[room]->contents);
#ifdef PROOL
		printf("obj_to_room() 1.5\n");
#endif
		object->in_room = room;
		object->carried_by = NULL;
		object->worn_by = NULL;
#ifdef PROOL
		printf("obj_to_room() 1.6\n");
#endif
#ifndef VIRTUSTAN
		if (ROOM_FLAGGED(room, ROOM_NOITEM)) {
#ifdef PROOL
			printf("obj_to_room() 1.6a\n");
#endif
			SET_BIT(GET_OBJ_EXTRA(object, ITEM_DECAY), ITEM_DECAY);
			}
#endif // VIRTUSTAN
#ifdef PROOL
		printf("obj_to_room() 1.7\n");
#endif
		sect = real_sector(room);
//      if (ROOM_FLAGGED(room, ROOM_HOUSE))
//         SET_BIT(ROOM_FLAGS(room, ROOM_HOUSE_CRASH), ROOM_HOUSE_CRASH);
//      if (object->proto_script || object->script)
#ifdef PROOL
	printf("obj_to_room() 2\n");
#endif
		if (object->script)
			GET_OBJ_DESTROY(object) = script_destroy_timer;
		else if (OBJ_FLAGGED(object, ITEM_NODECAY))
			GET_OBJ_DESTROY(object) = room_nodestroy_timer;
		else
			/*  ������ ������ � �������, ��������� ����������� ��������
			   ��� ����� - �������
			   if ((
			   (sect == SECT_WATER_SWIM || sect == SECT_WATER_NOSWIM) &&
			   !IS_CORPSE(object) &&
			   !OBJ_FLAGGED(object, ITEM_SWIMMING)
			   ) ||
			   ((sect == SECT_FLYING ) &&
			   !IS_CORPSE(object) &&
			   !OBJ_FLAGGED(object, ITEM_FLYING)
			   )
			   )
			   {extract_obj(object);
			   }
			   else
			   if (OBJ_FLAGGED(object, ITEM_DECAY) ||
			   (OBJ_FLAGGED(object, ITEM_ZONEDECAY) &&
			   GET_OBJ_ZONE(object) != NOWHERE &&
			   GET_OBJ_ZONE(object) != world[room]->zone
			   )
			   )
			   {act("$o0 ��������$U � ������ ����, ������� ������� �����", FALSE,
			   world[room]->people, object, 0, TO_ROOM);
			   act("$o0 ��������$U � ������ ����, ������� ������� �����", FALSE,
			   world[room]->people, object, 0, TO_CHAR);
			   extract_obj(object);
			   }
			   else */
		if (GET_OBJ_TYPE(object) == ITEM_MONEY)
			GET_OBJ_DESTROY(object) = money_destroy_timer;
		else if (ROOM_FLAGGED(room, ROOM_DEATH))
			GET_OBJ_DESTROY(object) = death_destroy_timer;
		else
			GET_OBJ_DESTROY(object) = room_destroy_timer;
	}
#ifdef PROOL
	printf("obj_to_room() 3\n");
#endif
}

/* ������� ��� �������� �������� ����� ����� � �������
   ��������� ������ - 1 ���� ���������, 0 - ���� ������� */
int obj_decay(OBJ_DATA * object)
{
	int room, sect;
	room = object->in_room;

	if (room == NOWHERE)
		return (0);

	sect = real_sector(room);

	if (((sect == SECT_WATER_SWIM || sect == SECT_WATER_NOSWIM) &&
	     !IS_CORPSE(object) && !OBJ_FLAGGED(object, ITEM_SWIMMING))) {

		act("$o0 �������� ������$G.", FALSE, world[room]->people, object, 0, TO_ROOM);
		act("$o0 �������� ������$G.", FALSE, world[room]->people, object, 0, TO_CHAR);
		extract_obj(object);
		return (1);
	}

	if (((sect == SECT_FLYING) && !IS_CORPSE(object) && !OBJ_FLAGGED(object, ITEM_FLYING))) {

		act("$o0 ����$G ����.", FALSE, world[room]->people, object, 0, TO_ROOM);
		act("$o0 ����$G ����.", FALSE, world[room]->people, object, 0, TO_CHAR);
		extract_obj(object);
		return (1);
	}


	if (OBJ_FLAGGED(object, ITEM_DECAY) ||
	    (OBJ_FLAGGED(object, ITEM_ZONEDECAY) &&
	     GET_OBJ_ZONE(object) != NOWHERE && GET_OBJ_ZONE(object) != world[room]->zone)) {

		act("$o0 ��������$U � ������ ����, ������� ������� �����", FALSE,
		    world[room]->people, object, 0, TO_ROOM);
		act("$o0 ��������$U � ������ ����, ������� ������� �����", FALSE,
		    world[room]->people, object, 0, TO_CHAR);
		extract_obj(object);
		return (1);
	}

	return (0);
}

/* Take an object from a room */
void obj_from_room(OBJ_DATA * object)
{
	OBJ_DATA *temp;

	if (!object || object->in_room == NOWHERE) {
		log("SYSERR: NULL object (%p) or obj not in a room (%d) passed to obj_from_room",
		    object, object->in_room);
		return;
	}

	REMOVE_FROM_LIST(object, world[object->in_room]->contents, next_content);

//  if (ROOM_FLAGGED(object->in_room, ROOM_HOUSE))
//     SET_BIT(ROOM_FLAGS(object->in_room, ROOM_HOUSE_CRASH), ROOM_HOUSE_CRASH);
	object->in_room = NOWHERE;
	object->next_content = NULL;
}


/* put an object in an object (quaint)  */
void obj_to_obj(OBJ_DATA * obj, OBJ_DATA * obj_to)
{
	OBJ_DATA *tmp_obj;

	if (!obj || !obj_to || obj == obj_to) {
		log("SYSERR: NULL object (%p) or same source (%p) and target (%p) obj passed to obj_to_obj.",
		    obj, obj, obj_to);
		return;
	}

	insert_obj_and_group(obj, &obj_to->contains);
	obj->in_obj = obj_to;

	for (tmp_obj = obj->in_obj; tmp_obj->in_obj; tmp_obj = tmp_obj->in_obj)
		GET_OBJ_WEIGHT(tmp_obj) += GET_OBJ_WEIGHT(obj);

	/* top level object.  Subtract weight from inventory if necessary. */
	GET_OBJ_WEIGHT(tmp_obj) += GET_OBJ_WEIGHT(obj);
	if (tmp_obj->carried_by)
		IS_CARRYING_W(tmp_obj->carried_by) += GET_OBJ_WEIGHT(obj);
}


/* remove an object from an object */
void obj_from_obj(OBJ_DATA * obj)
{
	OBJ_DATA *temp, *obj_from;

	if (obj->in_obj == NULL) {
		log("SYSERR: (%s): trying to illegally extract obj from obj.", __FILE__);
		return;
	}
	obj_from = obj->in_obj;
	REMOVE_FROM_LIST(obj, obj_from->contains, next_content);

	/* Subtract weight from containers container */
	for (temp = obj->in_obj; temp->in_obj; temp = temp->in_obj)
		GET_OBJ_WEIGHT(temp) = MAX(1,GET_OBJ_WEIGHT(temp) - GET_OBJ_WEIGHT(obj));

	/* Subtract weight from char that carries the object */
	GET_OBJ_WEIGHT(temp) = MAX(1,GET_OBJ_WEIGHT(temp) - GET_OBJ_WEIGHT(obj));
	if (temp->carried_by)
		IS_CARRYING_W(temp->carried_by) = MAX(1, IS_CARRYING_W(temp->carried_by) - GET_OBJ_WEIGHT(obj));

	obj->in_obj = NULL;
	obj->next_content = NULL;
}


/* Set all carried_by to point to new owner */
void object_list_new_owner(OBJ_DATA * list, CHAR_DATA * ch)
{
	if (list) {
		object_list_new_owner(list->contains, ch);
		object_list_new_owner(list->next_content, ch);
		list->carried_by = ch;
	}
}

/* Extract an object from the world */
void extract_obj(OBJ_DATA * obj)
{
	char name[MAX_STRING_LENGTH];
	OBJ_DATA *temp;

	strcpy(name, obj->PNames[0]);
	log("Extracting obj %s", name);
// TODO: � ����� log("Start extract obj %s", name);

	/* Get rid of the contents of the object, as well. */
	/* ��������� ����������� ���������� ��� ��� ����������� */
	while (obj->contains) {
		temp = obj->contains;
		obj_from_obj(temp);

		if (obj->carried_by) {
			if (IS_NPC(obj->carried_by)
			    || (IS_CARRYING_N(obj->carried_by) >= CAN_CARRY_N(obj->carried_by))) {
				obj_to_room(temp, IN_ROOM(obj->carried_by));
				obj_decay(temp);
			} else {
				obj_to_char(temp, obj->carried_by);
			}
		} else if (obj->worn_by != NULL) {
			if (IS_NPC(obj->worn_by)
			    || (IS_CARRYING_N(obj->worn_by) >= CAN_CARRY_N(obj->worn_by))) {
				obj_to_room(temp, IN_ROOM(obj->worn_by));
				obj_decay(temp);
			} else {
				obj_to_char(temp, obj->worn_by);
			}
		} else if (obj->in_room != NOWHERE) {
			obj_to_room(temp, obj->in_room);
			obj_decay(temp);
		} else if (obj->in_obj) {
			extract_obj(temp);
		} else
			extract_obj(temp);
	}
	/* ���������� ���������� ������� */

	if (obj->worn_by != NULL)
		if (unequip_char(obj->worn_by, obj->worn_on) != obj)
			log("SYSERR: Inconsistent worn_by and worn_on pointers!!");
	if (obj->in_room != NOWHERE)
		obj_from_room(obj);
	else if (obj->carried_by)
		obj_from_char(obj);
	else if (obj->in_obj)
		obj_from_obj(obj);

	check_auction(NULL, obj);
	check_exchange(obj);
	REMOVE_FROM_LIST(obj, object_list, next);

	if (GET_OBJ_RNUM(obj) >= 0)
		(obj_index[GET_OBJ_RNUM(obj)].number)--;

	free_script(SCRIPT(obj));	// ��� ������������

	free_obj(obj);
// TODO: � ����� log("Stop extract obj %s", name);
}



void update_object(OBJ_DATA * obj, int use)
{
	/* dont update objects with a timer trigger */
	if (!SCRIPT_CHECK(obj, OTRIG_TIMER) && GET_OBJ_TIMER(obj) > 0 && OBJ_FLAGGED(obj, ITEM_TICKTIMER))
		GET_OBJ_TIMER(obj) -= use;
	if (obj->contains)
		update_object(obj->contains, use);
	if (obj->next_content)
		update_object(obj->next_content, use);
}


void update_char_objects(CHAR_DATA * ch)
{
	int i;

	if (GET_EQ(ch, WEAR_LIGHT) != NULL) {
		if (GET_OBJ_TYPE(GET_EQ(ch, WEAR_LIGHT)) == ITEM_LIGHT) {
			if (GET_OBJ_VAL(GET_EQ(ch, WEAR_LIGHT), 2) > 0) {
				i = --GET_OBJ_VAL(GET_EQ(ch, WEAR_LIGHT), 2);
				if (i == 1) {
					act("���$G $o ��������$G � �����$G �������.\r\n",
					    FALSE, ch, GET_EQ(ch, WEAR_LIGHT), 0, TO_CHAR);
					act("$o $n1 ��������$G � �����$G �������.",
					    FALSE, ch, GET_EQ(ch, WEAR_LIGHT), 0, TO_ROOM);
				} else if (i == 0) {
					act("���$G $o �����$Q.\r\n", FALSE, ch, GET_EQ(ch, WEAR_LIGHT), 0, TO_CHAR);
					act("$o $n1 �����$Q.", FALSE, ch, GET_EQ(ch, WEAR_LIGHT), 0, TO_ROOM);
					if (IN_ROOM(ch) != NOWHERE) {
						if (world[IN_ROOM(ch)]->light > 0)
							world[IN_ROOM(ch)]->light -= 1;
					}
					if (OBJ_FLAGGED(GET_EQ(ch, WEAR_LIGHT), ITEM_DECAY))
						extract_obj(GET_EQ(ch, WEAR_LIGHT));
				}
			}
		}
	}

	for (i = 0; i < NUM_WEARS; i++)
		if (GET_EQ(ch, i))
			update_object(GET_EQ(ch, i), 1);

	if (ch->carrying)
		update_object(ch->carrying, 1);
}

void change_fighting(CHAR_DATA * ch, int need_stop)
{
	CHAR_DATA *k, *j, *temp;

	for (k = character_list; k; k = temp) {
		temp = k->next;
		if (PROTECTING(k) == ch) {
			PROTECTING(k) = NULL;
			CLR_AF_BATTLE(k, EAF_PROTECT);
		}
		if (TOUCHING(k) == ch) {
			TOUCHING(k) = NULL;
			CLR_AF_BATTLE(k, EAF_PROTECT);
		}
		if (GET_EXTRA_VICTIM(k) == ch)
			SET_EXTRA(k, 0, NULL);
		if (GET_CAST_CHAR(k) == ch)
			SET_CAST(k, 0, 0, NULL, NULL, NULL);
		if (FIGHTING(k) == ch && IN_ROOM(k) != NOWHERE) {
			log("[Change fighting] Change victim");
			for (j = world[IN_ROOM(ch)]->people; j; j = j->next_in_room)
				if (FIGHTING(j) == k) {
					act("�� ����������� �������� �� $N3.", FALSE, k, 0, j, TO_CHAR);
					act("$n ����������$u �� ��� !", FALSE, k, 0, j, TO_VICT);
					FIGHTING(k) = j;
					break;
				}
			if (!j && need_stop)
				stop_fighting(k, FALSE);
		}
	}
}

/**
* ���� �� ���� ������, ������ �� ����� ������ ����, �� ��� ������ � ������ ����� ���� - ��� ������������ � ��� ��.
* ���� �� ���� ������, �������� � ������ � ������ (������ ��� �����), �� �� �� ��� ������ ������� �� �����, ��� ������.
* � �� ��� ������� �������� ��� �� ����� ���������� ������ ���� � �����, ������� � ��� �� ����, ��� ��� ���� ��������,
* ���� ���� ���� ������������ � ����� ���� ������� ��������� ������ ����� � ���������� �������. -- Krodo
* \param inv - 1 ��������� � ����������� �� ���������, 0 - � ������ � ����
* \param zone_reset - 1 - ������ ���� ��� ���������� ��������, 0 - �� ������ ������
*/
void drop_obj_on_zreset(CHAR_DATA *ch, OBJ_DATA *obj, bool inv, bool zone_reset)
{
#ifdef PROOL
	printf("drop_obj_on_zreset 1\n");
#endif	
	if (zone_reset && !OBJ_FLAGGED(obj, ITEM_TICKTIMER))
		extract_obj(obj);
	else {
		if (inv)
			act("�� ��������� $o3 �� �����.", FALSE, ch, obj, 0, TO_CHAR);
		else
			act("�� ����� $o3 � ��������� �� �����.", FALSE, ch, obj, 0, TO_CHAR);
		/* ���� ���� ��� ����� �� �������, �� �� �������� ���������
		����� ������ ������ ��������� � ��� � � ������ */
		if (!IS_NPC(ch) || !MOB_FLAGGED(ch, MOB_CORPSE)) {
			if (inv)
				act("$n ������$g $o3 �� �����.", FALSE, ch, obj, 0, TO_ROOM);
			else
				act("$n ����$g $o3 � ������$g �� �����.", FALSE, ch, obj, 0, TO_ROOM);
		}
#ifdef PROOL
		printf("drop_obj_on_zreset 2\n");
#endif	
		obj_to_room(obj, ch->in_room);
#ifdef PROOL
		printf("drop_obj_on_zreset 3\n");
#endif	
		obj_decay(obj);
	}
#ifdef PROOL
	printf("drop_obj_on_zreset 4\n");
#endif	
}

/**
* Extract a ch completely from the world, and leave his stuff behind
* \param zone_reset - 0 ������� ���� ����� ������ (�� ���������), 1 - ���� ��� ������ ����
*/
void extract_char(CHAR_DATA * ch, int clear_objs, bool zone_reset)
{
	char name[MAX_STRING_LENGTH];
	DESCRIPTOR_DATA *t_desc;
	int i, freed = 0;
	CHAR_DATA *ch_w, *temp;

#ifdef PROOL
	printf("extract_char 1\n");
#endif

	if (MOB_FLAGGED(ch, MOB_FREE) || MOB_FLAGGED(ch, MOB_DELETE))
		return;

#ifdef PROOL
	printf("extract_char 2\n");
#endif
	strcpy(name, GET_NAME(ch));
#ifdef PROOL
	printf("extract_char 3\n");
#endif

	log("[Extract char] Start function for char %s", name);
	if (!IS_NPC(ch) && !ch->desc) {
		log("[Extract char] Extract descriptors");
		for (t_desc = descriptor_list; t_desc; t_desc = t_desc->next)
			if (t_desc->original == ch)
				do_return(t_desc->character, NULL, 0, 0);
	}
#ifdef PROOL
	printf("extract_char 4\n");
#endif

	if (ch->in_room == NOWHERE) {
//log("SYSERR: NOWHERE extracting char %s. (%s, extract_char)",GET_NAME(ch), __FILE__);
		return;
		// exit(1);
	}
#ifdef PROOL
	printf("extract_char 5\n");
#endif

	/* Forget snooping, if applicable */
	log("[Extract char] Stop snooping");
	if (ch->desc) {
		if (ch->desc->snooping) {
			ch->desc->snooping->snoop_by = NULL;
			ch->desc->snooping = NULL;
		}
		if (ch->desc->snoop_by) {
			SEND_TO_Q("���� ������ ������ ����������.\r\n", ch->desc->snoop_by);
			ch->desc->snoop_by->snooping = NULL;
			ch->desc->snoop_by = NULL;
		}
	}
#ifdef PROOL
	printf("extract_char 6\n");
#endif

	/* transfer equipment to room, if any */
	log("[Extract char] Drop equipment");
	for (i = 0; i < NUM_WEARS; i++) {
		if (GET_EQ(ch, i)) {
			OBJ_DATA *obj_eq = unequip_char(ch, i);
			drop_obj_on_zreset(ch, obj_eq, 0, zone_reset);
		}
	}
#ifdef PROOL
	printf("extract_char 7\n");
#endif

	/* transfer objects to room, if any */
	log("[Extract char] Drop objects");
	while (ch->carrying) {
		OBJ_DATA *obj = ch->carrying;
		obj_from_char(obj);
#ifdef PROOL
	printf("extract_char 7.1\n");
#endif
		drop_obj_on_zreset(ch, obj, 1, zone_reset);
#ifdef PROOL
	printf("extract_char 7.2\n");
#endif
	}
#ifdef PROOL
	printf("extract_char 8\n");
#endif

	log("[Extract char] Die followers");
	if (ch->followers || ch->master)
		die_follower(ch);
#ifdef PROOL
	printf("extract_char 9\n");
#endif

	log("[Extract char] Stop fighting self");
	if (FIGHTING(ch))
		stop_fighting(ch, TRUE);
#ifdef PROOL
	printf("extract_char 10\n");
#endif

	log("[Extract char] Stop all fight for opponee");
	change_fighting(ch, TRUE);

#ifdef PROOL
	printf("extract_char 11\n");
#endif

	log("[Extract char] Remove char from room");
	char_from_room(ch);
#ifdef PROOL
	printf("extract_char 12\n");
#endif

	/* pull the char from the list */
	SET_BIT(MOB_FLAGS(ch, MOB_DELETE), MOB_DELETE);
	REMOVE_FROM_LIST(ch, character_list, next);

	if (ch->desc && ch->desc->original)
		do_return(ch, NULL, 0, 0);

	if (!IS_NPC(ch)) {
		log("[Extract char] All save for PC");
		check_auction(ch, NULL);
		save_char(ch, NOWHERE);
		//��������� ����-�����, ���� ������ �������� �� ���� � �����
		Crash_delete_crashfile(ch);
//      if (clear_objs)
//         Crash_delete_files(GET_NAME(ch), CRASH_DELETE_OLD | CRASH_DELETE_NEW);
	} else {
		log("[Extract char] All clear for NPC");
		if (GET_MOB_RNUM(ch) > -1)	/* if mobile */
			mob_index[GET_MOB_RNUM(ch)].number--;
		clearMemory(ch);	/* Only NPC's can have memory */

		free_script(SCRIPT(ch));	// ��� ������������
		SCRIPT(ch) = NULL;	// �.�. ������� char ����� ������ �����
		// ����� �������� ��� script_data, �����
		// ��� ������ ������ random_triggers
		if (SCRIPT_MEM(ch)) {
			extract_script_mem(SCRIPT_MEM(ch));
			SCRIPT_MEM(ch) = NULL;	// ���������� ����������� �����������
		}

		SET_BIT(MOB_FLAGS(ch, MOB_FREE), MOB_FREE);
		free_char(ch);
		freed = 1;
	}

	if (!freed && ch->desc != NULL) {
		STATE(ch->desc) = CON_MENU;
		SEND_TO_Q(MENU, ch->desc);
		if (!IS_NPC(ch) && RENTABLE(ch) && clear_objs) {
			ch_w = ch->next;
			do_entergame(ch->desc);
			ch->next = ch_w;
		}

	} else {		/* if a player gets purged from within the game */
		if (!freed) {
			SET_BIT(MOB_FLAGS(ch, MOB_FREE), MOB_FREE);
			free_char(ch);
		}
	}
	log("[Extract char] Stop function for char %s", name);
}


/* Extract a MOB completely from the world, and destroy his stuff */
void extract_mob(CHAR_DATA * ch)
{
	int i;
	CHAR_DATA *temp;

	if (MOB_FLAGGED(ch, MOB_FREE) || MOB_FLAGGED(ch, MOB_DELETE))
		return;

	if (ch->in_room == NOWHERE) {
		log("SYSERR: NOWHERE extracting char %s. (%s, extract_mob)", GET_NAME(ch), __FILE__);
		return;
		exit(1);
	}
	if (ch->followers || ch->master)
		die_follower(ch);

	/* Forget snooping, if applicable */
	if (ch->desc) {
		if (ch->desc->snooping) {
			ch->desc->snooping->snoop_by = NULL;
			ch->desc->snooping = NULL;
		}
		if (ch->desc->snoop_by) {
			SEND_TO_Q("���� ������ ������ ����������.\r\n", ch->desc->snoop_by);
			ch->desc->snoop_by->snooping = NULL;
			ch->desc->snoop_by = NULL;
		}
	}

	/* extract objects, if any */
	while (ch->carrying)
		extract_obj(ch->carrying);

	/* transfer equipment to room, if any */
	for (i = 0; i < NUM_WEARS; i++)
		if (GET_EQ(ch, i))
			extract_obj(GET_EQ(ch, i));

	if (FIGHTING(ch))
		stop_fighting(ch, TRUE);

	log("[Extract mob] Stop all fight for opponee");
	change_fighting(ch, TRUE);

	char_from_room(ch);

	/* pull the char from the list */
	SET_BIT(MOB_FLAGS(ch, MOB_DELETE), MOB_DELETE);
	REMOVE_FROM_LIST(ch, character_list, next);


	if (ch->desc && ch->desc->original)
		do_return(ch, NULL, 0, 0);

	if (GET_MOB_RNUM(ch) > -1)
		mob_index[GET_MOB_RNUM(ch)].number--;
	clearMemory(ch);

	free_script(SCRIPT(ch));	// ��. ����
	SCRIPT(ch) = NULL;

	if (SCRIPT_MEM(ch)) {
		extract_script_mem(SCRIPT_MEM(ch));
		SCRIPT_MEM(ch) = NULL;
	}

	SET_BIT(MOB_FLAGS(ch, MOB_FREE), MOB_FREE);
	free_char(ch);
}




/* ***********************************************************************
* Here follows high-level versions of some earlier routines, ie functions*
* which incorporate the actual player-data                               *.
*********************************************************************** */


CHAR_DATA *get_player_vis(CHAR_DATA * ch, const char *name, int inroom)
{
	CHAR_DATA *i;

	for (i = character_list; i; i = i->next) {
		//if (IS_NPC(i) || (!(i->desc) && !RENTABLE(i) && !(inroom & FIND_CHAR_DISCONNECTED)))
		//   continue;
		if (IS_NPC(i))
			continue;
		if (!HERE(i))
			continue;
		if ((inroom & FIND_CHAR_ROOM) && i->in_room != ch->in_room)
			continue;
		//if (str_cmp(i->player.name, name))
		//   continue;
		if (!CAN_SEE_CHAR(ch, i))
			continue;
		if (!isname(name, i->player.name))
			continue;
		return (i);
	}

	return (NULL);
}


CHAR_DATA *get_player_pun(CHAR_DATA * ch, const char *name, int inroom)
{
	CHAR_DATA *i;

	for (i = character_list; i; i = i->next) {
		if (IS_NPC(i))
			continue;
		if ((inroom & FIND_CHAR_ROOM) && i->in_room != ch->in_room)
			continue;
		if (!isname(name, i->player.name))
			continue;
		return (i);
	}

	return (NULL);
}


CHAR_DATA *get_char_room_vis(CHAR_DATA * ch, char *name)
{
	CHAR_DATA *i;
	int j = 0, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	/* JE 7/18/94 :-) :-) */
	if (!str_cmp(name, "self") || !str_cmp(name, "me") ||
	    !str_cmp(name, "�") || !str_cmp(name, "����") || !str_cmp(name, "����"))
		return (ch);

	/* 0.<name> means PC with name */
	strcpy(tmp, name);
	if (!(number = get_number(&tmp)))
		return (get_player_vis(ch, tmp, FIND_CHAR_ROOM));

	for (i = world[ch->in_room]->people; i && j <= number; i = i->next_in_room)
		if (HERE(i) && CAN_SEE(ch, i) && isname(tmp, i->player.name))
			if (++j == number)
				return (i);

	return (NULL);
}


CHAR_DATA *get_char_vis(CHAR_DATA * ch, char *name, int where)
{
	CHAR_DATA *i;
	int j = 0, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	/* check the room first */
	if (where == FIND_CHAR_ROOM)
		return get_char_room_vis(ch, name);
	else if (where == FIND_CHAR_WORLD) {
		if ((i = get_char_room_vis(ch, name)) != NULL)
			return (i);

		strcpy(tmp, name);
		if (!(number = get_number(&tmp)))
			return get_player_vis(ch, tmp, 0);

		for (i = character_list; i && (j <= number); i = i->next)
			if (HERE(i) && CAN_SEE(ch, i) && isname(tmp, i->player.name))
				if (++j == number)
					return (i);
	}

	return (NULL);
}


OBJ_DATA *get_obj_in_list_vis(CHAR_DATA * ch, char *name, OBJ_DATA * list)
{
	OBJ_DATA *i;
	int j = 0, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	strcpy(tmp, name);
	if (!(number = get_number(&tmp)))
		return (NULL);

	for (i = list; i && (j <= number); i = i->next_content)
		if (isname(tmp, i->name))
			if (CAN_SEE_OBJ(ch, i))
				if (++j == number) {	//* sprintf(buf,"Show obj %d %s %x ", number, i->name, i);
					//* send_to_char(buf,ch);
					return (i);
				}

	return (NULL);
}




/* search the entire world for an object, and return a pointer  */
OBJ_DATA *get_obj_vis(CHAR_DATA * ch, char *name)
{
	OBJ_DATA *i;
	int j = 0, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	/* scan items carried */
	if ((i = get_obj_in_list_vis(ch, name, ch->carrying)) != NULL)
		return (i);

	/* scan room */
	if ((i = get_obj_in_list_vis(ch, name, world[ch->in_room]->contents)) != NULL)
		return (i);

	strcpy(tmp, name);
	if ((number = get_number(&tmp)) == 0)
		return (NULL);

	/* ok.. no luck yet. scan the entire obj list   */
	for (i = object_list; i && (j <= number); i = i->next)
		if (isname(tmp, i->name))
			if (CAN_SEE_OBJ(ch, i))
				if (++j == number)
					return (i);

	return (NULL);
}



OBJ_DATA *get_object_in_equip_vis(CHAR_DATA * ch, char *arg, OBJ_DATA * equipment[], int *j)
{
	int l, number;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	strcpy(tmp, arg);
	if ((number = get_number(&tmp)) == 0)
		return (NULL);

	for ((*j) = 0, l = 0; (*j) < NUM_WEARS; (*j)++)
		if (equipment[(*j)])
			if (CAN_SEE_OBJ(ch, equipment[(*j)]))
				if (isname(arg, equipment[(*j)]->name))
					if (++l == number)
						return (equipment[(*j)]);

	return (NULL);
}


OBJ_DATA *get_obj_in_eq_vis(CHAR_DATA * ch, char *arg)
{
	int l, number, j;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	strcpy(tmp, arg);
	if ((number = get_number(&tmp)) == 0)
		return (NULL);

	for (j = 0, l = 0; j < NUM_WEARS; j++)
		if (GET_EQ(ch, j))
			if (CAN_SEE_OBJ(ch, GET_EQ(ch, j)))
				if (isname(tmp, GET_EQ(ch, j)->name))
					if (++l == number)
						return (GET_EQ(ch, j));

	return (NULL);
}


char *money_desc(int amount, int padis)
{
	static char buf[128];
	const char *single[6][2] = { {"�", "�"},
	{"��", "�"},
	{"��", "�"},
	{"�", "�"},
	{"��", "��"},
	{"��", "�"}
	}, *plural[6][3] = {
		{
		"��", "�", "�"}, {
		"��", "�", "�"}, {
		"��", "�", "�"}, {
		"��", "�", "�"}, {
		"��", "��", "��"}, {
	"��", "�", "�"}};

	if (amount <= 0) {
		log("SYSERR: Try to create negative or 0 money (%d).", amount);
		return (NULL);
	}
	if (amount == 1) {
		sprintf(buf, "���%s �����%s", single[padis][0], single[padis][1]);
	} else if (amount <= 10)
		sprintf(buf, "���������%s ������%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 20)
		sprintf(buf, "�������%s ������%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 75)
		sprintf(buf, "�������%s ������%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 200)
		sprintf(buf, "�������%s ����%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 1000)
		sprintf(buf, "�������%s ����%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 5000)
		sprintf(buf, "����%s ����", plural[padis][1]);
	else if (amount <= 10000)
		sprintf(buf, "�����%s ����%s ����", plural[padis][0], plural[padis][1]);
	else if (amount <= 20000)
		sprintf(buf, "����%s ����", plural[padis][2]);
	else if (amount <= 75000)
		sprintf(buf, "�����%s ����%s ����", plural[padis][0], plural[padis][2]);
	else if (amount <= 150000)
		sprintf(buf, "����%s ����", plural[padis][1]);
	else if (amount <= 250000)
		sprintf(buf, "���%s ����", plural[padis][2]);
	else
		sprintf(buf, "������%s ���%s ����", plural[padis][0], plural[padis][2]);

	return (buf);
}


OBJ_DATA *create_money(int amount)
{
	int i;
	OBJ_DATA *obj;
	EXTRA_DESCR_DATA *new_descr;
	char buf[200];

	if (amount <= 0) {
		log("SYSERR: Try to create negative or 0 money. (%d)", amount);
		return (NULL);
	}
	obj = create_obj();
	CREATE(new_descr, EXTRA_DESCR_DATA, 1);

	if (amount == 1) {
		sprintf(buf, "coin gold ���� ������ ����� ����� %s", money_desc(amount, 0));
		obj->name = str_dup(buf);
		obj->short_description = str_dup("����");
		obj->description = str_dup("���� ���� ����� �����.");
		new_descr->keyword = str_dup("coin gold ����� ���� �����");
		new_descr->description = str_dup("����� ���� ���� ����.");
		for (i = 0; i < NUM_PADS; i++)
			obj->PNames[i] = str_dup(money_desc(amount, i));
	} else {
		sprintf(buf, "coins gold ���� ����� %s", money_desc(amount, 0));
		obj->name = str_dup(buf);
		obj->short_description = str_dup(money_desc(amount, 0));
		for (i = 0; i < NUM_PADS; i++)
			obj->PNames[i] = str_dup(money_desc(amount, i));

		sprintf(buf, "����� ����� %s.", money_desc(amount, 0));
		obj->description = str_dup(CAP(buf));

		new_descr->keyword = str_dup("coins gold ���� �����");
	}

	new_descr->next = NULL;
	obj->ex_description = new_descr;

	GET_OBJ_TYPE(obj) = ITEM_MONEY;
	GET_OBJ_WEAR(obj) = ITEM_WEAR_TAKE;
	GET_OBJ_SEX(obj) = SEX_FEMALE;
	GET_OBJ_VAL(obj, 0) = amount;
	GET_OBJ_COST(obj) = amount;
	GET_OBJ_MAX(obj) = 100;
	GET_OBJ_CUR(obj) = 100;
	GET_OBJ_TIMER(obj) = 24 * 60 * 7;
	GET_OBJ_WEIGHT(obj) = 1;
	SET_BIT(GET_OBJ_EXTRA(obj, ITEM_NODONATE), ITEM_NODONATE);
	SET_BIT(GET_OBJ_EXTRA(obj, ITEM_NOSELL), ITEM_NOSELL);

	return (obj);
}


/* Generic Find, designed to find any object/character
 *
 * Calling:
 *  *arg     is the pointer containing the string to be searched for.
 *           This string doesn't have to be a single word, the routine
 *           extracts the next word itself.
 *  bitv..   All those bits that you want to "search through".
 *           Bit found will be result of the function
 *  *ch      This is the person that is trying to "find"
 *  **tar_ch Will be NULL if no character was found, otherwise points
 * **tar_obj Will be NULL if no object was found, otherwise points
 *
 * The routine used to return a pointer to the next word in *arg (just
 * like the one_argument routine), but now it returns an integer that
 * describes what it filled in.
 */
int generic_find(char *arg, bitvector_t bitvector, CHAR_DATA * ch, CHAR_DATA ** tar_ch, OBJ_DATA ** tar_obj)
{
	char name[256];

	*tar_ch = NULL;
	*tar_obj = NULL;

	OBJ_DATA *i;
	int l, number, j = 0;
	char tmpname[MAX_INPUT_LENGTH];
	char *tmp = tmpname;

	one_argument(arg, name);

	if (!*name)
		return (0);

	if (IS_SET(bitvector, FIND_CHAR_ROOM)) {	/* Find person in room */
		if ((*tar_ch = get_char_vis(ch, name, FIND_CHAR_ROOM)) != NULL)
			return (FIND_CHAR_ROOM);
	}
	if (IS_SET(bitvector, FIND_CHAR_WORLD)) {
		if ((*tar_ch = get_char_vis(ch, name, FIND_CHAR_WORLD)) != NULL)
			return (FIND_CHAR_WORLD);
	}
	if (IS_SET(bitvector, FIND_OBJ_WORLD)) {
		if ((*tar_obj = get_obj_vis(ch, name)))
			return (FIND_OBJ_WORLD);
	}

/* ������ ���������.
   (�) ������� ��� dzMUDiST ��� ������ */

// ��������� ���, �������������� ��������� FIND_OBJ_EQUIP | FIND_OBJ_INV | FIND_OBJ_ROOM
// � ����� ����� ������� ���������� � "���������� - �������� - �������" ��������
// ������ ���������� ��������� "���������" ���������.
// ������ ��� ��������������� � ����� � ����� ���������.

	strcpy(tmp, name);
	if (!(number = get_number(&tmp)))
		return 0;

	if (IS_SET(bitvector, FIND_OBJ_EQUIP))
		for (l = 0; l < NUM_WEARS; l++)
			if (GET_EQ(ch, l))
				if (CAN_SEE_OBJ(ch, GET_EQ(ch, l)))
					if (isname(tmp, GET_EQ(ch, l)->name))
						if (++j == number) {
							*tar_obj = GET_EQ(ch, l);
							return (FIND_OBJ_EQUIP);
						}

	if (IS_SET(bitvector, FIND_OBJ_INV))
		for (i = ch->carrying; i && (j <= number); i = i->next_content)
			if (isname(tmp, i->name))
				if (CAN_SEE_OBJ(ch, i))
					if (++j == number) {
						*tar_obj = i;
						return (FIND_OBJ_INV);
					}

	if (IS_SET(bitvector, FIND_OBJ_ROOM))
		for (i = world[ch->in_room]->contents; i && (j <= number); i = i->next_content)
			if (isname(tmp, i->name))
				if (CAN_SEE_OBJ(ch, i))
					if (++j == number) {
						*tar_obj = i;
						return (FIND_OBJ_ROOM);
					}
//  if (IS_SET (bitvector, FIND_OBJ_EQUIP))
//    {
//      if ((*tar_obj = get_obj_in_eq_vis (ch, name)) != NULL)
//      return (FIND_OBJ_EQUIP);
//    }
//  if (IS_SET (bitvector, FIND_OBJ_INV))
//    {
//      if ((*tar_obj = get_obj_in_list_vis (ch, name, ch->carrying)) != NULL)
//      return (FIND_OBJ_INV);
//    }
//  if (IS_SET (bitvector, FIND_OBJ_ROOM))
//    {
//      if ((*tar_obj =
//         get_obj_in_list_vis (ch, name,
//                              world[ch->in_room]->contents)) != NULL)
//      return (FIND_OBJ_ROOM);
//    }

/* ����� ���������.
   (�) ������� ��� dzMUDiST ��� ������ */

	return (0);
}


/* a function to scan for "all" or "all.x" */
int find_all_dots(char *arg)
{
	if (!str_cmp(arg, "all") || !str_cmp(arg, "���"))
		return (FIND_ALL);
	else if (!strn_cmp(arg, "all.", 4) || !strn_cmp(arg, "���.", 4)) {
		strcpy(arg, arg + 4);
		return (FIND_ALLDOT);
	} else
		return (FIND_INDIV);
}

/* ������� ��� ������ � ��������� ��� "townportal" */
/* ���������� ��������� �� ����� �� vnum ������� ��� NULL ���� �� ������� */
char *find_portal_by_vnum(int vnum)
{
	struct portals_list_type *i;
	for (i = portals_list; i; i = i->next_portal) {
		if (i->vnum == vnum)
			return (i->wrd);
	}
	return (NULL);
}

/* ���������� ����������� ������� ��� �������� ������� */
int level_portal_by_vnum(int vnum)
{
	struct portals_list_type *i;
	for (i = portals_list; i; i = i->next_portal) {
		if (i->vnum == vnum)
			return (i->level);
	}
	return (0);
}

/* ���������� vnum ������� �� ��������� ����� ��� NOWHERE ���� �� ������� */
int find_portal_by_word(char *wrd)
{
	struct portals_list_type *i;
	for (i = portals_list; i; i = i->next_portal) {
		if (!str_cmp(i->wrd, wrd))
			return (i->vnum);
	}
	return (NOWHERE);
}

struct portals_list_type *get_portal(int vnum, char *wrd)
{
	struct portals_list_type *i;
	for (i = portals_list; i; i = i->next_portal) {
		if ((!wrd || !str_cmp(i->wrd, wrd)) && ((vnum == -1) || (i->vnum == vnum)))
			break;
	}
	return i;
}

/* ��������� � ������ ���� ������ � ������� vnum - � ��������� �����������
   � ���� ���� ��� ����� - �� ��������� ��� 1� � ������ */
void add_portal_to_char(CHAR_DATA * ch, int vnum)
{
	struct char_portal_type *tmp, *dlt = NULL;

	/* �������� �� �� ��� ��� ���� ������ � ������, ���� ����, ������� */
	for (tmp = GET_PORTALS(ch); tmp; tmp = tmp->next) {
		if (tmp->vnum == vnum) {
			if (dlt) {
				dlt->next = tmp->next;
			} else {
				GET_PORTALS(ch) = tmp->next;
			}
			free(tmp);
			break;
		}
		dlt = tmp;
	}

	CREATE(tmp, struct char_portal_type, 1);
	tmp->vnum = vnum;
	tmp->next = GET_PORTALS(ch);
	GET_PORTALS(ch) = tmp;
}

/* �������� �� ��, ����� �� ��� ������ � ������� vnum */
int has_char_portal(CHAR_DATA * ch, int vnum)
{
	struct char_portal_type *tmp;

	for (tmp = GET_PORTALS(ch); tmp; tmp = tmp->next) {
		if (tmp->vnum == vnum)
			return (1);
	}
	return (0);
}

/* ������� ������ � �������������� ������� � ���� */
void check_portals(CHAR_DATA * ch)
{
	int max_p, portals;
	struct char_portal_type *tmp, *dlt = NULL;
	struct portals_list_type *port;

	/* ��������� ������������ ���������� ��������, ������� ����� ��������� ��� */
	max_p = MAX_PORTALS(ch);
	portals = 0;

	/* ��������� max_p ������� */
	for (tmp = GET_PORTALS(ch); tmp;) {
		port = get_portal(tmp->vnum, NULL);
		if (!port || (portals >= max_p) || (MAX(1, port->level - GET_REMORT(ch)/2) > GET_LEVEL(ch))) {
			if (dlt) {
				dlt->next = tmp->next;
			} else {
				GET_PORTALS(ch) = tmp->next;
			}
			free(tmp);
			if (dlt) {
				tmp = dlt->next;
			} else {
				tmp = GET_PORTALS(ch);
			}
		} else {
			dlt = tmp;
			portals++;
			tmp = tmp->next;
		}
	}
}

/*-----------------------------------------*/
/* ������� ��� ������ � ���������          */
/* ������� ������������ ����� ���������� ������� ���������� � ������ */
int charm_points(CHAR_DATA * ch)
{
	int lp;

	if (GET_CLASS(ch) != CLASS_CHARMMAGE)
		return (0);
	lp = GET_LEVEL(ch) + GET_REAL_CHA(ch) - 16;
	return (lp);
}

/* ������� ���������� ���������� ��������� ������� ���������� ��� ����,
   ����� ��������� ���� */
int on_charm_points(CHAR_DATA * ch)
{
	int lp;
	lp = GET_LEVEL(ch);
	return (lp);
}

/* ������� ������������ ��������������� ���������� ������� ���������� � ������ */
int used_charm_points(CHAR_DATA * ch)
{
	int lp;
	struct follow_type *f;

	lp = 0;
	for (f = ch->followers; f; f = f->next) {
		if (!AFF_FLAGGED(f->follower, AFF_CHARM))
			continue;
		lp = lp + GET_LEVEL(f->follower);
	}
	return (lp);
}

/* ������� ������ ������ ���� ������ ���������� ���� ��� ����������
   - ��� �����. ���������� ��������� �� ������������ ���� ���
   NULL ���� ��������� ��������� */
CHAR_DATA *charm_mob(CHAR_DATA * victim)
{
	int vnum = 0;
	CHAR_DATA *mob;

	vnum = CHARM_MOB_VNUM + GET_LEVEL(victim);

	if (vnum == GET_MOB_VNUM(victim))
		return (victim);

	/* ��������� ���� CHARM_MOB_VNUM+������� victim */
	if (!(mob = read_mobile(CHARM_MOB_VNUM + GET_LEVEL(victim), VIRTUAL)))
		return (NULL);
	char_to_room(mob, victim->in_room);
	/* ������ �������� ���� ����� �� ��� � ���������� */
	GET_PAD(mob, 0) = str_dup(GET_PAD(victim, 0));
	GET_PAD(mob, 1) = str_dup(GET_PAD(victim, 1));
	GET_PAD(mob, 2) = str_dup(GET_PAD(victim, 2));
	GET_PAD(mob, 3) = str_dup(GET_PAD(victim, 3));
	GET_PAD(mob, 4) = str_dup(GET_PAD(victim, 4));
	GET_PAD(mob, 5) = str_dup(GET_PAD(victim, 5));
	mob->player.name = str_dup(victim->player.name);
	mob->player.short_descr = str_dup(victim->player.short_descr);
	mob->player.long_descr = str_dup(victim->player.long_descr);
	mob->player.description = str_dup(victim->player.description);
	/* ������� ���� victim */
	extract_mob(victim);
	return (mob);
}

//������� ��� ����������������� �����
float get_damage_per_round(CHAR_DATA * victim)
{
	float dam_per_round = 0.0;
	dam_per_round = (GET_DR(victim) + str_app[GET_STR(victim)].todam +
			 victim->mob_specials.damnodice *
			 (victim->mob_specials.damsizedice + 1) / 2.0) *
	    (1 + victim->mob_specials.ExtraAttack +
	     2 * get_skill(victim, SKILL_ADDSHOT) / MAX(1, get_skill(victim, SKILL_ADDSHOT)));

//���� ������� - �� ����� ���������� �� 1.1
#ifndef VIRTUSTAN
	if (MOB_FLAGGED(victim, (MOB_FIREBREATH | MOB_GASBREATH | MOB_FROSTBREATH | MOB_ACIDBREATH | MOB_LIGHTBREATH)))
		dam_per_round *= 1.1;
#endif

	return dam_per_round;
}

float get_effective_cha(CHAR_DATA * ch, int spellnum)
{
	int key_value, key_value_add, i;

//��� �������/������� ���� ��������� ��������, � ����� ������ ������ - �������
	if (spellnum == SPELL_RESSURECTION || spellnum == SPELL_ANIMATE_DEAD) {
		key_value = GET_WIS(ch) - 6;
		key_value_add = MIN(56 - GET_WIS(ch), GET_WIS_ADD(ch));
		i = 3;
	} else {
		key_value = GET_CHA(ch);
		key_value_add = MIN(50 - GET_CHA(ch), GET_CHA_ADD(ch));
		i = 5;
	}
	float eff_cha = 0.0;
	if (GET_LEVEL(ch) <= 14)
		eff_cha = MIN(max_stats2[(int) GET_CLASS(ch)][i], key_value)
		    - 6 * (float) (14 - GET_LEVEL(ch)) / 13.0 + key_value_add
		    * (0.2 + 0.3 * (float) (GET_LEVEL(ch) - 1) / 13.0);
	else if (GET_LEVEL(ch) <= 26) {
		if (key_value <= 16)
			eff_cha = key_value + key_value_add * (0.5 + 0.5 * (float) (GET_LEVEL(ch) - 14) / 12.0);
		else
			eff_cha =
			    16 + (float) ((key_value - 16) * (GET_LEVEL(ch) - 14)) / 12.0 +
			    key_value_add * (0.5 + 0.5 * (float) (GET_LEVEL(ch) - 14) / 12.0);
	} else
		eff_cha = key_value + key_value_add;
	return eff_cha;
}

float get_effective_int(CHAR_DATA * ch)
{
	float eff_int = 0.0;
	if (GET_LEVEL(ch) <= 14)
		eff_int = MIN(max_stats2[(int) GET_CLASS(ch)][2], GET_INT(ch))
		    - 6 * (float) (14 - GET_LEVEL(ch)) / 13.0 + GET_INT_ADD(ch)
		    * (0.2 + 0.3 * (float) (GET_LEVEL(ch) - 1) / 13.0);
	else if (GET_LEVEL(ch) <= 26) {
		if (GET_INT(ch) <= 16)
			eff_int = GET_INT(ch) + GET_INT_ADD(ch) * (0.5 + 0.5 * (float) (GET_LEVEL(ch) - 14) / 12.0);
		else
			eff_int =
			    16 + (float) ((GET_INT(ch) - 16) * (GET_LEVEL(ch) - 14)) / 12.0 +
			    GET_INT_ADD(ch) * (0.5 + 0.5 * (float) (GET_LEVEL(ch) - 14) / 12.0);
	} else
		eff_int = GET_REAL_INT(ch);
	return eff_int;
}

float calc_cha_for_hire(CHAR_DATA * victim)
{
	int i;
	float reformed_hp = 0.0, needed_cha = 0.0;
	for (i = 0; i < 50; i++) {
		reformed_hp = GET_MAX_HIT(victim) + get_damage_per_round(victim) * cha_app[i].dam_to_hit_rate;
		if (cha_app[i].charms >= reformed_hp)
			break;
	}
	i = POSI(i);
	needed_cha = i - 1 + (reformed_hp - cha_app[i - 1].charms) / (cha_app[i].charms - cha_app[i - 1].charms);
//sprintf(buf,"check: charms = %d   rhp = %f\r\n",cha_app[i].charms,reformed_hp);
//act(buf,FALSE,victim,0,0,TO_ROOM);
	return VPOSI(needed_cha, 1.0, 50.0);
}


int calc_hire_price(CHAR_DATA * ch, CHAR_DATA * victim)
{
	float needed_cha = calc_cha_for_hire(victim), dpr = 0.0;
	float e_cha = get_effective_cha(ch, 0), e_int = get_effective_int(ch);
	//((e_cha<(1+min_stats2[(int)GET_CLASS(ch)][5]))?MIN(GET_CHA(ch),(1+min_stats2[(int)GET_CLASS(ch)][5])):e_cha)-
	//                     1 - min_stats2[(int)GET_CLASS(ch)][5] +
	//((e_int<(1+min_stats2[(int)GET_CLASS(ch)][2]))?MIN(GET_INT(ch),(1+min_stats2[(int)GET_CLASS(ch)][2])):e_int)-
	//                     1.0 - min_stats2[(int)GET_CLASS(ch)][2];
	float stat_overlimit = VPOSI(e_cha + e_int - 1.0 -
				     min_stats2[(int) GET_CLASS(ch)][5] - 1 -
				     min_stats2[(int) GET_CLASS(ch)][2], 0, 100);

	if (GET_LEVEL(ch) > 14 && GET_LEVEL(ch) <= 26)
		stat_overlimit =
		    VPOSI(stat_overlimit - GET_REMORT(ch) * (0.5 + 0.5 * (float) (GET_LEVEL(ch) - 14) / 12.0), 0, 100);
	else if (GET_LEVEL(ch) > 26)
		stat_overlimit = VPOSI(stat_overlimit - GET_REMORT(ch), 0, 100);

	float price = 0;
	float real_cha = 1.0 + GET_LEVEL(ch) / 2.0 + stat_overlimit / 2.0;
	float difference = needed_cha - real_cha;
	//sprintf(buf,"diff =%f statover=%f ncha=%f rcha=%f pow:%f\r\n",difference,stat_overlimit,needed_cha, real_cha, pow(2.0,difference));
	//send_to_char(buf,ch);
	dpr = get_damage_per_round(victim);

	if (difference <= 0)
		price = dpr * (1.0 - 0.01 * stat_overlimit);
	else
		price = MMIN((dpr * pow(2.0F, difference)), MAXPRICE);

	if (price <= 0.0 || (difference >= 25 && (int) dpr))
		price = MAXPRICE;

	return (int) ceil(price);
}


int get_player_charms(CHAR_DATA * ch, int spellnum)
{
	float r_hp = 0;
	float eff_cha = 0.0;
	eff_cha = get_effective_cha(ch, spellnum);
	if (spellnum != SPELL_CHARM)
		eff_cha = MMIN(48, eff_cha + 2);	// ��� ����� ����� �������� � ������� � 2

	r_hp = (1 - eff_cha + (int) eff_cha) * cha_app[(int) eff_cha].charms +
	    (eff_cha - (int) eff_cha) * cha_app[(int) eff_cha + 1].charms;

	return (int) r_hp;
}


int get_reformed_charmice_hp(CHAR_DATA * ch, CHAR_DATA * victim, int spellnum)
{
	float r_hp = 0;
	float eff_cha = 0.0;
	eff_cha = get_effective_cha(ch, spellnum);
	if (spellnum != SPELL_CHARM)
		eff_cha = MMIN(48, eff_cha + 2);	// ��� ����� ����� �������� � ������� � 2

	// ������������ ����� ���������� ��� ����� �������� �������
	r_hp = GET_MAX_HIT(victim) + get_damage_per_round(victim) *
	    ((1 - eff_cha + (int) eff_cha) * cha_app[(int) eff_cha].dam_to_hit_rate +
	     (eff_cha - (int) eff_cha) * cha_app[(int) eff_cha + 1].dam_to_hit_rate);

	return (int) r_hp;
}

//********************************************************************
// ������ � �������� ����

void MemQ_init(CHAR_DATA * ch)
{
	ch->MemQueue.stored = 0;
	ch->MemQueue.total = 0;
	ch->MemQueue.queue = NULL;
}

void MemQ_flush(CHAR_DATA * ch)
{
	struct spell_mem_queue_item *i;
	while (ch->MemQueue.queue) {
		i = ch->MemQueue.queue;
		ch->MemQueue.queue = i->link;
		free(i);
	}
	MemQ_init(ch);
}

int MemQ_learn(CHAR_DATA * ch)
{
	int num;
	struct spell_mem_queue_item *i;
	if (ch->MemQueue.queue == NULL)
		return 0;
	num = GET_MEM_CURRENT(ch);
	ch->MemQueue.stored -= num;
	ch->MemQueue.total -= num;
	num = ch->MemQueue.queue->spellnum;
	i = ch->MemQueue.queue;
	ch->MemQueue.queue = i->link;
	free(i);
	sprintf(buf, "�� ������� ���������� \"%s%s%s\".\r\n",
		CCICYN(ch, C_NRM), spell_info[num].name, CCNRM(ch, C_NRM));
	send_to_char(buf, ch);
	return num;
}

void MemQ_remember(CHAR_DATA * ch, int num)
{
	int *slots;
	int slotcnt, slotn;
	struct spell_mem_queue_item *i, **pi = &ch->MemQueue.queue;

	// ��������� ���������� ������
	slots = MemQ_slots(ch);
	slotn = spell_info[num].slot_forc[(int) GET_CLASS(ch)][(int) GET_KIN(ch)] - 1;	slotcnt = slot_for_char(ch, slotn + 1);
	slotcnt -= slots[slotn];	// ���-�� ��������� ������

	if (slotcnt <= 0) {
		send_to_char("� ��� ��� ��������� ����� ����� �����.", ch);
		return;
	}

	if (GET_RELIGION(ch) == RELIGION_MONO)
		sprintf(buf, "�� �������� ���������� \"%s%s%s\" � ���� ��������.\r\n",
			CCIMAG(ch, C_NRM), spell_info[num].name, CCNRM(ch, C_NRM));
	else
		sprintf(buf, "�� ������� ���������� \"%s%s%s\" � ���� ����.\r\n",
			CCIMAG(ch, C_NRM), spell_info[num].name, CCNRM(ch, C_NRM));
	send_to_char(buf, ch);

	ch->MemQueue.total += mag_manacost(ch, num);
	while (*pi)
		pi = &((*pi)->link);
	CREATE(i, struct spell_mem_queue_item, 1);
	*pi = i;
	i->spellnum = num;
	i->link = NULL;
}

void MemQ_forget(CHAR_DATA * ch, int num)
{
	struct spell_mem_queue_item **q = NULL, **i;

	for (i = &ch->MemQueue.queue; *i; i = &(i[0]->link)) {
		if (i[0]->spellnum == num)
			q = i;
	}

	if (q == NULL) {
		send_to_char("�� � �� ���������� ������� ��� ����������.\r\n", ch);
	} else {
		struct spell_mem_queue_item *ptr;
		if (q == &ch->MemQueue.queue)
			GET_MEM_COMPLETED(ch) = 0;
		GET_MEM_TOTAL(ch) = MAX(0, GET_MEM_TOTAL(ch) - mag_manacost(ch, num));
		ptr = q[0];
		q[0] = q[0]->link;
		free(ptr);
		sprintf(buf,
			"�� ���������� ���������� \"%s%s%s\" �� ������ ��� �����������.\r\n",
			CCIMAG(ch, C_NRM), spell_info[num].name, CCNRM(ch, C_NRM));
		send_to_char(buf, ch);
	}
}

int *MemQ_slots(CHAR_DATA * ch)
{
	struct spell_mem_queue_item **q, *qt;
	static int slots[MAX_SLOT];
	int i, n, sloti;

	// �������������
	for (i = 0; i < MAX_SLOT; ++i)
		slots[i] = slot_for_char(ch, i + 1);

	for (i = MAX_SPELLS; i >= 1; --i) {
		if (!IS_SET(GET_SPELL_TYPE(ch, i), SPELL_KNOW))
			continue;
		if ((n = GET_SPELL_MEM(ch, i)) == 0)
			continue;
		sloti = spell_info[i].slot_forc[(int) GET_CLASS(ch)][(int) GET_KIN(ch)] - 1;
		if (MIN_CAST_LEV(spell_info[i], ch) > GET_LEVEL (ch)
		   || MIN_CAST_REM(spell_info[i],ch) > GET_REMORT (ch)){
			GET_SPELL_MEM(ch, i) = 0;
			continue;
		}
		slots[sloti] -= n;
		if (slots[sloti] < 0) {
			GET_SPELL_MEM(ch, i) += slots[sloti];
			slots[sloti] = 0;
		}

	}

	for (q = &ch->MemQueue.queue; q[0];) {
		sloti = spell_info[q[0]->spellnum].slot_forc[(int) GET_CLASS(ch)][(int) GET_KIN(ch)] - 1;
		if (sloti >= 0 && sloti <= 10) {
			--slots[sloti];
			if (slots[sloti] >= 0 &&
			    MIN_CAST_LEV(spell_info[q[0]->spellnum],ch) <= GET_LEVEL (ch)
			    && MIN_CAST_REM(spell_info[q[0]->spellnum],ch) <= GET_REMORT (ch)){
				q = &(q[0]->link);
			} else {
				if (q == &ch->MemQueue.queue)
					GET_MEM_COMPLETED(ch) = 0;
				GET_MEM_TOTAL(ch) = MAX(0, GET_MEM_TOTAL(ch) - mag_manacost(ch, q[0]->spellnum));
				++slots[sloti];
				qt = q[0];
				q[0] = q[0]->link;
				free(qt);
			}
		}
	}

	for (i = 0; i < MAX_SLOT; ++i)
		slots[i] = slot_for_char(ch, i + 1) - slots[i];

	return slots;
}

int equip_in_metall(CHAR_DATA * ch)
{
	int i, wgt = 0;

	if (IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM))
		return (FALSE);
	if (IS_GOD(ch))
		return (FALSE);

	for (i = 0; i < NUM_WEARS; i++) {
		if (GET_EQ(ch, i) &&
		    GET_OBJ_TYPE(GET_EQ(ch, i)) == ITEM_ARMOR && GET_OBJ_MATER(GET_EQ(ch, i)) <= MAT_COLOR)
			wgt += GET_OBJ_WEIGHT(GET_EQ(ch, i));
	}

	if (wgt > GET_REAL_STR(ch))
		return (TRUE);

	return (FALSE);
}

int awake_others(CHAR_DATA * ch)
{
	int i;

	if (IS_NPC(ch) && !AFF_FLAGGED(ch, AFF_CHARM))
		return (FALSE);

	if (IS_GOD(ch))
		return (FALSE);

	if (AFF_FLAGGED(ch, AFF_STAIRS) ||
	    AFF_FLAGGED(ch, AFF_SANCTUARY) || AFF_FLAGGED(ch, AFF_SINGLELIGHT) || AFF_FLAGGED(ch, AFF_HOLYLIGHT))
		return (TRUE);

	for (i = 0; i < NUM_WEARS; i++) {
		if (GET_EQ(ch, i))
			if ((GET_OBJ_TYPE(GET_EQ(ch, i)) == ITEM_ARMOR &&
			     GET_EQ(ch, i)->obj_flags.Obj_mater <= MAT_COLOR) ||
			    OBJ_FLAGGED(GET_EQ(ch, i), ITEM_HUM) || OBJ_FLAGGED(GET_EQ(ch, i), ITEM_GLOW))
				return (TRUE);
	}
	return (FALSE);
}

/* ���� ������� - ������������ ������ �� ����� ��� ������ � ������ ������� */

int calculate_resistance_coeff (CHAR_DATA *ch, int resist_type, int effect)
{

	int result, resistance;

        resistance = GET_RESIST(ch, resist_type);

	if (resistance <= 0) {
		return (int)((1 - resistance/100) * effect);
	}
	if (IS_NPC(ch) && resistance >= 200) {
		return (0);
	}
	if (!IS_NPC(ch)) {
		resistance = MIN(75, resistance);
	}
	result = (int)(effect - (resistance + number(0, resistance)) * effect / 200);
	result = MAX(0, result);
	return result;
}
