// #define PROOL
#define VIRTUSTAN
// #define DEBUG

// #define HTML1 "/home/prool-k/data/www/prool.kharkov.org/mud/mud1.html"
// #define HTML2 "/home/prool-k/data/www/prool.kharkov.org/mud/mud2.html"

/* ************************************************************************
*   File: interpreter.cpp
*  Usage: parse user commands, search for specials, call ACMD functions   
*                                                                         
*  $Date: 2011/01/15 12:05:01 $                                           
*  $Revision: 1.54 $                                                       
************************************************************************ */

#define __INTERPRETER_C__

#include "sys/stat.h"
#include "sys/types.h"
#include "conf.h"
#include <boost/lexical_cast.hpp>
#include "sysdep.h"
#include "structs.h"
#include "comm.h"
#include "interpreter.h"
#include "constants.h"
#include "db.h"
#include "utils.h"
#include "spells.h"
#include "skills.h"
#include "handler.h"
#include "house.h"
#include "mail.h"
#include "screen.h"
#include "olc.h"
#include "dg_scripts.h"
#include "pk.h"
#include "genchar.h"
#include "ban.hpp"
#include "item.creation.hpp"
#include "arena.hpp"
#include "features.hpp"
#include "boards.h"
#include "top.h"
#include "title.hpp"
#include "password.hpp"
#include "privilege.hpp"

#include "prool.h"

extern room_rnum r_mortal_start_room;
extern room_rnum r_immort_start_room;
extern room_rnum r_frozen_start_room;
extern room_rnum r_helled_start_room;
extern room_rnum r_named_start_room;
extern room_rnum r_unreg_start_room;
extern const char *class_menu;
extern const char *class_menu_vik;
extern const char *class_menu_step;
extern const char *race_menu;
extern const char *race_menu_step;
extern const char *race_menu_vik;
extern const char *kin_menu;
extern const char *religion_menu;
extern const char *color_menu;
extern char *motd;
extern char *rules;
extern char *background;
extern const char *MENU;
extern const char *WELC_MESSG;
extern const char *START_MESSG;
extern CHAR_DATA *character_list;
extern DESCRIPTOR_DATA *descriptor_list;
extern struct player_index_element *player_table;
extern int top_of_p_table;
extern int circle_restrict;
extern int no_specials;
extern int max_bad_pws;
extern INDEX_DATA *mob_index;
extern INDEX_DATA *obj_index;

extern struct pclean_criteria_data pclean_criteria[];

extern char *GREETINGS;
extern const char *pc_class_types[];
extern const char *pc_class_types_vik[];
extern const char *pc_class_types_step[];
extern const char *race_types[];
extern const char *race_types_step[];
extern const char *race_types_vik[];
extern const char *kin_types[];
extern struct cheat_list_type *cheaters_list;
extern struct set_struct set_fields[];
extern struct show_struct show_fields[];
extern BanList *ban;

extern time_t boot_time;
extern int reboot_uptime;

extern time_t max_time; // ������������ ����� ����������� ������ ������. prool
extern char *server_compilation_date;
extern char *server_compilation_time;

extern int statistic_zones;
extern int statistic_rooms;
extern int statistic_mobs;
extern int statistic_objs;

int max_players_after_start=0; // prool
time_t max_players_time=0;
char total_max_players_date [40];
int idclip=0; // ����� "����������" ����� ������
int idlink=0; // ����� �������������� ��� ���������� ����� ������

char idmap_user[50]; // ��� ������������ ������ idmap

/* external functions */
void do_start(CHAR_DATA * ch, int newbie);
int parse_class(char arg);
int parse_class_vik (char arg);
int parse_class_step (char arg);
int parse_race(char arg);
int parse_race_step (char arg);
int parse_race_vik (char arg);
int parse_kin (char arg);
int special(CHAR_DATA * ch, int cmd, char *arg);
int Valid_Name(char *newname);
int Is_Valid_Name(char *newname);
int Is_Valid_Dc(char *newname);
void read_aliases(CHAR_DATA * ch);
void read_saved_vars(CHAR_DATA * ch);
void oedit_parse(DESCRIPTOR_DATA * d, char *arg);
void redit_parse(DESCRIPTOR_DATA * d, char *arg);
void zedit_parse(DESCRIPTOR_DATA * d, char *arg);
void medit_parse(DESCRIPTOR_DATA * d, char *arg);
void sedit_parse(DESCRIPTOR_DATA * d, char *arg);
void trigedit_parse(DESCRIPTOR_DATA * d, char *arg);
void Crash_timer_obj(int index, long timer_dec);
int find_social(char *name);
int calc_loadroom(CHAR_DATA * ch);
void delete_char(char *name);
void do_aggressive_mob(CHAR_DATA * ch, int check_sneak);
extern int process_auto_agreement(DESCRIPTOR_DATA * d);
extern int CheckProxy(DESCRIPTOR_DATA * ch);
extern void NewNameShow(CHAR_DATA * ch);
extern void NewNameAdd(CHAR_DATA * ch, bool save = 1);

/* local functions */
int perform_dupe_check(DESCRIPTOR_DATA * d);
struct alias_data *find_alias(struct alias_data *alias_list, char *str);
void free_alias(struct alias_data *a);
void perform_complex_alias(struct txt_q *input_q, char *orig, struct alias_data *a);
int perform_alias(DESCRIPTOR_DATA * d, char *orig);
int reserved_word(char *argument);
int find_name(const char *name);
int _parse_name(char *arg, char *name);
void add_logon_record(DESCRIPTOR_DATA * d);
/* prototypes for all do_x functions. */
int find_action(char *cmd);
int do_social(CHAR_DATA * ch, char *argument);
void skip_wizard(CHAR_DATA * ch, char *string);
void single_god_invoice(CHAR_DATA* ch);
void login_change_invoice(CHAR_DATA* ch);
void print_statistics (DESCRIPTOR_DATA *d, int mode); // prool
int players_num();

ACMD(do_advance);
ACMD(do_alias);
ACMD(do_antigods);
ACMD(do_assist);
ACMD(do_at);
ACMD(do_affects);
ACMD(do_backstab);
ACMD(do_ban);
ACMD(do_bash);
ACMD(do_beep);
ACMD(do_cast);
ACMD(do_cheat);
ACMD(do_clanstuff);
ACMD(do_create);
ACMD(do_mixture);
ACMD(do_color);
ACMD(do_courage);
ACMD(do_commands);
ACMD(do_consider);
ACMD(do_credits);
ACMD(do_date);
ACMD(do_dc);
ACMD(do_diagnose);
ACMD(do_display);
ACMD(do_drink);
ACMD(do_drunkoff);
ACMD(do_features);
ACMD(do_featset);
ACMD(do_findhelpee);
ACMD(do_firstaid);
ACMD(do_fire);
ACMD(do_drop);
ACMD(do_eat);
ACMD(do_destroy);
ACMD(do_echo);
ACMD(do_email);
ACMD(do_enter);
ACMD(do_manadrain);
ACMD(do_equipment);
ACMD(do_examine);
ACMD(do_revenge);
ACMD(do_remort);
ACMD(do_remember_char);
ACMD(do_exit);
ACMD(do_exits);
ACMD(do_flee);
ACMD(do_follow);
ACMD(do_horseon);
ACMD(do_horseoff);
ACMD(do_horseput);
ACMD(do_horseget);
ACMD(do_horsetake);
ACMD(do_hidetrack);
ACMD(do_hidemove);
ACMD(do_force);
ACMD(do_forcetime);
ACMD(do_glory);
ACMD(do_gecho);
ACMD(do_gen_comm);
ACMD(do_mobshout);
ACMD(do_gen_door);
ACMD(do_gen_ps);
ACMD(do_gen_write);
ACMD(do_get);
ACMD(do_give);
ACMD(do_givehorse);
ACMD(do_gold);
ACMD(do_goto);
ACMD(do_grab);
ACMD(do_group);
ACMD(do_gsay);
ACMD(do_help);
ACMD(do_hide);
ACMD(do_hit);
ACMD(do_info);
ACMD(do_index);
ACMD(do_insult);
ACMD(do_inventory);
ACMD(do_invis);
ACMD(do_kick);
ACMD(do_kill);
ACMD(do_last);
ACMD(do_mode);
ACMD(do_mark);
ACMD(do_makefood);
ACMD(do_name);
ACMD(do_disarm);
ACMD(do_chopoff);
ACMD(do_deviate);
ACMD(do_leave);
ACMD(do_levels);
ACMD(do_liblist);
ACMD(do_lightwalk);
ACMD(do_load);
ACMD(do_look);
ACMD(do_sides);
ACMD(do_not_here);
ACMD(do_offer);
ACMD(do_olc);
ACMD(do_order);
ACMD(do_page);
ACMD(do_pray);
ACMD(do_polomat);
ACMD(do_pochinit);
ACMD(do_poofset);
ACMD(do_pour);
ACMD(do_skills);
ACMD(do_statistic);
ACMD(do_spells);
ACMD(do_remember);
ACMD(do_learn);
ACMD(do_forget);
ACMD(do_purge);
ACMD(do_put);
ACMD(do_quit);
ACMD(do_reboot);
ACMD(do_remove);
ACMD(do_rent);
ACMD(do_reply);
ACMD(do_report);
ACMD(do_rescue);
ACMD(do_stopfight);
ACMD(do_stophorse);
ACMD(do_rest);
ACMD(do_restore);
ACMD(do_return);
ACMD(do_save);
ACMD(do_say);
ACMD(do_score);
ACMD(do_send);
ACMD(do_set);
ACMD(do_show);
ACMD(do_shutdown);
ACMD(do_sit);
ACMD(do_skillset);
ACMD(do_sleep);
ACMD(do_sneak);
ACMD(do_snoop);
ACMD(do_spec_comm);
ACMD(do_split);
ACMD(do_stand);
ACMD(do_stat);
ACMD(do_steal);
ACMD(do_switch);
ACMD(do_syslog);
ACMD(do_throw);
ACMD(do_teleport);
ACMD(do_tell);
ACMD(do_time);
ACMD(do_toggle);
ACMD(do_track);
ACMD(do_sense);
ACMD(do_trans);
ACMD(do_unban);
ACMD(do_ungroup);
ACMD(do_use);
ACMD(do_users);
ACMD(do_visible);
ACMD(do_vnum);
ACMD(do_vstat);
ACMD(do_wake);
ACMD(do_wear);
ACMD(do_weather);
ACMD(do_where);
ACMD(do_who);
ACMD(do_who_new);
ACMD(do_wield);
ACMD(do_wimpy);
ACMD(do_wizlock);
ACMD(do_wiznet);
ACMD(do_wizutil);
ACMD(do_write);
ACMD(do_zreset);
ACMD(do_parry);
ACMD(do_multyparry);
ACMD(do_style);
ACMD(do_poisoned);
ACMD(do_repair);
ACMD(do_camouflage);
ACMD(do_stupor);
ACMD(do_mighthit);
ACMD(do_block);
ACMD(do_touch);
ACMD(do_transform_weapon);
ACMD(do_protect);
ACMD(do_dig);
ACMD(do_insertgem);
ACMD(do_ignore);
ACMD(do_proxy);
ACMD(do_turn_undead);

/* by prool */
//ACMD(do_system);
//ACMD(do_pwd);
//ACMD(do_wwwlog);
ACMD(do_eee);
ACMD(do_zsave);
ACMD(do_zrestore);
ACMD(do_userlist);
ACMD(do_fflush);
ACMD(do_shell);
ACMD(do_all_actors);
ACMD(do_kogda);
ACMD(do_sotvorit);
ACMD(do_pererab);
ACMD(do_reviziya);
ACMD(do_idclip);
ACMD(do_idmap);
ACMD(do_coord);
ACMD(do_coord2);
ACMD(do_map);
ACMD(do_idlink);
ACMD(do_automap);
ACMD(do_minimap);
ACMD(do_map2);
ACMD(do_purge_map);
ACMD(do_load_map);
ACMD(do_save_map);
ACMD(do_buy2);
ACMD(do_assorti);
ACMD(do_ruletka);
ACMD(do_sysinfo);
ACMD(do_ping);
ACMD(do_counter);
ACMD(do_fishing);
ACMD(do_gluk);
ACMD(do_color16);
ACMD(do_color256);
ACMD(do_esc);
ACMD(do_zadanie);

/* DG Script ACMD's */
ACMD(do_attach);
ACMD(do_detach);
ACMD(do_tlist);
ACMD(do_tstat);
ACMD(do_masound);
ACMD(do_mkill);
ACMD(do_mjunk);
ACMD(do_mdamage);
ACMD(do_mdoor);
ACMD(do_mechoaround);
ACMD(do_msend);
ACMD(do_mecho);
ACMD(do_mload);
ACMD(do_mpurge);
ACMD(do_mgoto);
ACMD(do_mat);
ACMD(do_mteleport);
ACMD(do_mforce);
ACMD(do_mexp);
ACMD(do_mgold);
ACMD(do_mhunt);
ACMD(do_mremember);
ACMD(do_mforget);
ACMD(do_mfeatturn);
ACMD(do_mtransform);
ACMD(do_mskillturn);
ACMD(do_mskilladd);
ACMD(do_mspellturn);
ACMD(do_mspelladd);
ACMD(do_mspellitem);
ACMD(do_vdelete);
ACMD(do_hearing);
ACMD(do_looking);
ACMD(do_ident);
ACMD(do_upgrade);
ACMD(do_armored);
ACMD(do_recall);
ACMD(do_pray_gods);
ACMD(do_rset);
ACMD(do_recipes);
ACMD(do_cook);
ACMD(do_forgive);
ACMD(do_imlist);
ACMD(do_townportal);
ACMD(DoBoard);
ACMD(DoBoardList);
ACMD(DoHouse);
ACMD(DoClanChannel);
ACMD(DoClanList);
ACMD(DoShowPolitics);
ACMD(DoHcontrol);
ACMD(DoWhoClan);
ACMD(DoClanPkList);
ACMD(DoStoreHouse);
ACMD(do_clanstuff);
ACMD(DoBest);
ACMD(do_zone);

/* This is the Master Command List(tm).

 * You can put new commands in, take commands out, change the order
 * they appear in, etc.  You can adjust the "priority" of commands
 * simply by changing the order they appear in the command list.
 * (For example, if you want "as" to mean "assist" instead of "ask",
 * just put "assist" above "ask" in the Master Command List(tm).
 *
 * In general, utility commands such as "at" should have high priority;
 * infrequently used and dangerously destructive commands should have low
 * priority.
 */

#define MAGIC_NUM 419
#define MAGIC_LEN 8

const char *create_name_rules =
"  ������� ���������� ������� ����������\r\n"
"\r\n"
"����� ���������� ����� �������� ������ ��� �� ������� (�������������), ���\r\n"
"������ �� ��������� ����.\r\n��� ���,\r\n"
"��� �� ����� ������� ������� �����, ���� ������ ����, ���������� ����������.\r\n"
"\r\n\r\n";

cpp_extern const struct command_info cmd_info[] = {
	/* command; minimum_position; command_pointer; minimum_level; subcmd; unhide_percent; */

	{"RESERVED", 0, 0, 0, 0},	/* this must be first -- for specprocs */

	/* directions must come before other commands but after RESERVED */
	{"�����", POS_STANDING, do_move, 0, SCMD_NORTH, -2},
	{"������", POS_STANDING, do_move, 0, SCMD_EAST, -2},
	{"��", POS_STANDING, do_move, 0, SCMD_SOUTH, -2},
	{"�����", POS_STANDING, do_move, 0, SCMD_WEST, -2},
	{"�����", POS_STANDING, do_move, 0, SCMD_UP, -2},
	{"����", POS_STANDING, do_move, 0, SCMD_DOWN, -2},
	{"north", POS_STANDING, do_move, 0, SCMD_NORTH, -2},
	{"east", POS_STANDING, do_move, 0, SCMD_EAST, -2},
	{"south", POS_STANDING, do_move, 0, SCMD_SOUTH, -2},
	{"west", POS_STANDING, do_move, 0, SCMD_WEST, -2},
	{"up", POS_STANDING, do_move, 0, SCMD_UP, -2},
	{"down", POS_STANDING, do_move, 0, SCMD_DOWN, -2},

	{"������", POS_DEAD, do_gen_ps, 0, SCMD_CREDITS, 0},
	{"������", POS_DEAD, DoBoard, 1, NOTICE_BOARD, -1},
	{"������", POS_DEAD, do_date, 0, SCMD_UPTIME, 0}, // prool
	{"�����������", POS_STANDING, do_assorti, 1, 0, -1},
	{"���������", POS_FIGHTING, do_hit, 0, SCMD_MURDER, -1},
	{"�������", POS_RESTING, do_gen_comm, 0, SCMD_AUCTION, 100},
	{"�������", POS_DEAD, do_affects, 0, SCMD_AUCTION, 0},

	{"�����", POS_STANDING, do_not_here, 1, 0, -1},
	{"������", POS_STANDING, do_not_here, 1, 0, 0},
	{"������", POS_FIGHTING, do_flee, 1, 0, -1},
	{"������", POS_DEAD, DoBoard, 1, GODBUILD_BOARD, -1},
	{"����", POS_FIGHTING, do_block, 0, 0, -1},
	{"�������", POS_DEAD, DoBoard, 1, PERS_BOARD, -1},
	{"����", POS_DEAD, do_gen_ps, 0, SCMD_IMMLIST, 0},
	{"��������", POS_DEAD, DoBoard, 1, GODGENERAL_BOARD, -1},
	{"�������", POS_RESTING, do_gen_comm, 0, SCMD_GOSSIP, -1},
	{"�������", POS_RESTING, do_drop, 0, SCMD_DROP, -1},

	{"������", POS_RESTING, do_cook, 0, 0, 200},
	{"������", POS_DEAD, do_gen_ps, 0, SCMD_VERSION, 0},
	{"����", POS_DEAD, DoBoard, 1, GENERAL_BOARD, -1},
	{"�����", POS_RESTING, do_get, 0, 0, 200},
	{"���������", POS_RESTING, do_diagnose, 0, 0, 100},
	{"��������", POS_STANDING, do_gen_door, 1, SCMD_PICK, -1},
	{"�������", POS_STANDING, do_not_here, 1, 0, -1},
	{"���������", POS_DEAD, do_return, 0, 0, -1},
	{"�����", POS_STANDING, do_enter, 0, 0, -2},
	{"�����������", POS_RESTING, do_wield, 0, 0, 200},
	{"�������", POS_RESTING, do_recall, 0, 0, -1},
	{"��������", POS_DEAD, do_pray_gods, 0, 0, -1},
	{"��������", POS_STANDING, do_insertgem, 0, SKILL_INSERTGEM, -1},
	{"�����", POS_DEAD, do_time, 0, 0, 0},
	{"�����", POS_SITTING, do_townportal, 1, 0, -1},
	{"��������", POS_FIGHTING, do_horseon, 0, 0, 500},
	{"������", POS_RESTING, do_stand, 0, 0, 500},
	{"���������", POS_DEAD, do_remember_char, 0, 0, 0},
	{"���������", POS_RESTING, do_drop, 0, 0 /*SCMD_DONATE */ , 300},
	{"���������", POS_STANDING, do_track, 0, 0, 500},
	{"������", POS_STANDING, do_pour, 0, SCMD_POUR, 500},
	{"������", POS_RESTING, do_exits, 0, 0, 0},

	{"��������", POS_RESTING, do_say, 0, 0, -1},
	{"�������", POS_SLEEPING, do_gsay, 0, 0, 500},
	{"���������", POS_SLEEPING, do_gsay, 0, 0, 500},
	{"�������", POS_SLEEPING, DoClanChannel, 0, SCMD_CHANNEL, 0},
	{"���", POS_RESTING, do_where, 1, 0, 0},
	{"����", POS_RESTING, do_zone, 0, 0, 0},
	{"������", POS_RESTING, do_drink, 0, SCMD_SIP, 200},
	{"����", POS_DEAD, do_gluk, 0, 0, 0}, // prool
	{"������", POS_SLEEPING, do_group, 1, 0, -1},
	{"����������", POS_SLEEPING, DoClanChannel, 0, SCMD_ACHANNEL, 0},
	{"����", POS_DEAD, do_gecho, LVL_GOD, 0, 0},
	{"������", POS_DEAD, do_wiznet, LVL_IMMORT, 0, 0},

	{"����", POS_RESTING, do_give, 0, 0, 500},
	{"����", POS_DEAD, do_date, 0, SCMD_DATE, 0},
	{"������", POS_RESTING, do_split, 1, 0, 200},
	{"�������", POS_RESTING, do_grab, 0, 0, 300},
	{"��������", POS_RESTING, do_report, 0, 0, 500},

	{"�����", POS_DEAD, DoBoardList, 0, 0, 0},
	{"�������", POS_DEAD, DoClanList, 0, 0, 0},
	{"���������", POS_DEAD, DoBoard, 1, CLANNEWS_BOARD, -1},
	{"������", POS_DEAD, DoBoard, 1, CLAN_BOARD, -1},
	{"������", POS_DEAD, DoClanPkList, 0, 1, 0},

	{"����", POS_RESTING, do_eat, 0, SCMD_EAT, 500},

	{"����������", POS_STANDING, do_pray, 1, SCMD_DONATE, -1},

	{"��������", POS_STANDING, do_backstab, 1, 0, 1},
	{"������", POS_RESTING, do_forget, 0, 0, 0},
	{"��������������", POS_DEAD, do_load_map, 0, 0, 0},
	{"�������", POS_STANDING, do_zadanie, 0, 0, 0},
	{"���������", POS_STANDING, do_not_here, 1, 0, -1},
	{"����������", POS_SLEEPING, do_spells, 0, 0, 0},
	{"�������", POS_SITTING, do_gen_door, 0, SCMD_CLOSE, 500},
	{"��������", POS_DEAD, do_cheat, LVL_IMPL, 0, 0},
	{"�������", POS_STANDING, do_hidetrack, 1, 0, -1},
	{"���������", POS_DEAD, do_wizutil, LVL_GOD, SCMD_MUTE, 0},
	{"����������", POS_DEAD, do_wizutil, LVL_FREEZE, SCMD_FREEZE, 0},
	{"���������", POS_RESTING, do_remember, 0, 0, 0},
	{"��������", POS_SITTING, do_gen_door, 0, SCMD_LOCK, 500},
	{"������", POS_DEAD, do_ban, LVL_GRGOD, 0, 0},
	{"�������", POS_SLEEPING, do_sleep, 0, 0, -1},
	{"��������", POS_DEAD, do_gen_ps, 0, SCMD_MOTD, 0},
	{"���������", POS_SLEEPING, do_force, LVL_GRGOD, 0, 0},
	{"��������", POS_RESTING, do_upgrade, 0, 0, 500},
	{"�������", POS_RESTING, do_remember, 0, 0, 0},
	{"��������", POS_RESTING, do_use, 0, SCMD_RECITE, 500},
	{"������", POS_RESTING, do_gold, 0, 0, 0},

	{"���������", POS_DEAD, do_inventory, 0, 0, 0},
	{"������������", POS_DEAD, do_ignore, 0, 0, 0},
	{"������", POS_DEAD, do_userlist, 0, 0, 0}, // prool
	{"����", POS_DEAD, DoBoard, 1, IDEA_BOARD, 0},
	{"����", POS_DEAD, do_gen_write, 0, SCMD_IDEA, 0},
	{"������� ������", POS_RESTING, do_turn_undead, 0, 0, -1},
	{"�������", POS_SITTING, do_learn, 0, 0, 0},
	{"����������", POS_SLEEPING, do_gen_ps, 0, SCMD_INFO, 0},
	{"������", POS_RESTING, do_use, 0, SCMD_QUAFF, 500},
	{"���", POS_SLEEPING, do_name, LVL_IMMORT, 0, 0},

	{"���������", POS_SITTING, do_cast, 1, 0, -1},
	{"�����", POS_RESTING, do_not_here, 1, 0, 0},
	{"�����", POS_DEAD, do_map2, 1, 0, 0},
	{"����", POS_RESTING, DoHouse, 0, 0, 0},
	{"�����", POS_DEAD, do_kogda, 0, 0, 0},
	{"�����", POS_DEAD, DoBoard, 1, GODCODE_BOARD, -1},
	{"�������", POS_DEAD, do_commands, 0, SCMD_COMMANDS, 0},
	{"����", POS_SLEEPING, do_quit, 0, 0, 0},
	{"�����", POS_SLEEPING, do_quit, 0, SCMD_QUIT, 0},
	{"����������", POS_DEAD, do_coord, 0/*LVL_IMMORT*/, 0, 0},
	{"����������2", POS_DEAD, do_coord2, 0/*LVL_IMMORT*/, 0, 0},
	{"������", POS_STANDING, do_dig, 0, SKILL_DIG, -1},
	{"��������", POS_STANDING, do_hidemove, 1, 0, -2},
	{"�������", POS_RESTING, do_gen_comm, 0, SCMD_SHOUT, -1},
	{"���", POS_RESTING, do_who, 0, 0, 0},
	{"����������", POS_RESTING, DoWhoClan, 0, 0, 0},
	{"����", POS_RESTING, do_who_new, LVL_IMMORT, 0, 0},
	{"����", POS_DEAD, do_gen_ps, 0, SCMD_WHOAMI, 0},
	{"������", POS_STANDING, do_not_here, 0, 0, -1},

	{"���������", POS_RESTING, do_grab, 1, 0, 300},
	{"������", POS_STANDING, do_firstaid, 0, 0, -1},
	{"����", POS_STANDING, do_pour, 0, SCMD_POUR, 500},
	{"������", POS_STANDING, do_not_here, 1, 0, -1},
	{"������", POS_DEAD, DoBest, 0, 0, 0},

	{"����������", POS_RESTING, do_camouflage, 0, 0, 500},
	{"�������", POS_FIGHTING, do_throw, 0, 0, -1},
	{"������", POS_STANDING, do_not_here, 0, 0, -1},
	{"�����", POS_RESTING, do_revenge, 0, 0, 0},
	{"���������", POS_DEAD, do_minimap, 0, 0, 0},
	{"�����", POS_FIGHTING, do_mighthit, 0, 0, -1},
	{"��������", POS_STANDING, do_pray, 1, SCMD_PRAY, -1},

	{"������", POS_RESTING, do_wear, 0, 0, 500}, // prool
	{"���������", POS_DEAD, DoBoard, 1, GODPUNISH_BOARD, -1},
	{"������", POS_STANDING, do_pour, 0, SCMD_FILL, 500},
	{"���������", POS_STANDING, do_pour, 0, SCMD_FILL, 500},
	{"�����", POS_STANDING, do_sense, 0, 0, 500},
	{"������", POS_STANDING, do_findhelpee, 0, SCMD_BUYHELPEE, -1},
	{"�������", POS_SLEEPING, do_gen_ps, 0, SCMD_INFO, 0},
	{"�������", POS_DEAD, DoBoard, 1, NEWS_BOARD, -1}, // prool

	{"�����������", POS_FIGHTING, do_disarm, 0, 0, -1},
	// {"��������", POS_RESTING, do_wear, 0, 0, 500}, // prool
	{"�����", POS_STANDING, do_not_here, 0, 0, 0},
	{"����������", POS_RESTING, do_sides, 0, 0, 0},
	{"��������", POS_FIGHTING, do_stupor, 0, 0, -1},
	{"�����", POS_RESTING, do_wear, 0, 0, 500},
	{"��������", POS_RESTING, do_ident, 0, 0, 500},
	{"������������", POS_RESTING, do_drunkoff, 0, 0, -1},
	{"��������", POS_DEAD, do_gen_write, 0, SCMD_TYPO, 0},
	{"��������", POS_RESTING, do_put, 0, 0, 500},
	{"�����", POS_RESTING, do_gen_comm, 1, SCMD_HOLLER, -1},
	{"���������", POS_RESTING, do_examine, 0, 0, 0},
	{"��������", POS_STANDING, do_horsetake, 1, 0, -1},
	{"���������", POS_RESTING, do_insult, 0, 0, -1},
	{"�������", POS_RESTING, do_use, 0, SCMD_QUAFF, 300},
	{"����������", POS_STANDING, do_makefood, 0, 0, -1},
	{"��������", POS_RESTING, do_reply, 0, 0, -1},
	{"��������", POS_FIGHTING, do_multyparry, 0, 0, -1},
	{"��������", POS_DEAD, do_horseget, 0, 0, -1},
	{"���������", POS_RESTING, do_rest, 0, 0, -1},
	{"�������", POS_SITTING, do_gen_door, 0, SCMD_OPEN, 500},
	{"��������", POS_SITTING, do_gen_door, 0, SCMD_UNLOCK, 500},
	{"���������", POS_SITTING, do_stophorse, 0, 0, -1},
	{"��������", POS_FIGHTING, do_poisoned, 0, 0, -1},
	{"��������", POS_RESTING, do_antigods, 1, 0, -1},
	{"���������", POS_FIGHTING, do_stopfight, 1, 0, -1},
	{"���������", POS_STANDING, do_not_here, 1, 0, -1},
	{"�������", POS_STANDING, do_not_here, 0, 0, 500},
	{"����", POS_DEAD, do_score, 0, 0, 0},
	{"��������", POS_DEAD, do_not_here, 0, SCMD_CLEAR, -1},
	{"������", POS_DEAD, DoBoard, 1, ERROR_BOARD, 0},
	{"������", POS_DEAD, do_gen_write, 0, SCMD_BUG, 0},

	{"����������", POS_FIGHTING, do_parry, 0, 0, -1},
	{"�����������", POS_FIGHTING, do_touch, 0, 0, -1},
	{"����������", POS_STANDING, do_transform_weapon, 0, SKILL_TRANSFORMWEAPON,
	 -1},
	{"��������", POS_STANDING, do_givehorse, 0, 0, -1},
	{"���������", POS_STANDING, do_not_here, 1, 0, -1},
	{"������������", POS_STANDING, do_pererab, 1, 0, -1},
	{"���������", POS_DEAD, do_all_actors, 1, 0, -1}, // prool
//	{"�������", POS_DEAD, do_email, LVL_IMPL, 0, 0},
	{"��������", POS_DEAD, do_pochinit, 1, 0, -1},
	{"��������������", POS_STANDING, do_remort, 0, 0, -1},
	{"���������������", POS_STANDING, do_remort, 0, 1, -1},
	{"��������", POS_STANDING, do_pour, 0, SCMD_POUR, 500},
	{"����", POS_RESTING, do_drink, 0, SCMD_DRINK, 400},
	{"������", POS_STANDING, do_write, 1, 0, -1},
	{"������", POS_SLEEPING, DoClanPkList, 0, 0, 0},
	{"�����", POS_FIGHTING, do_kick, 1, 0, -1},
	{"������", POS_RESTING, do_weather, 0, 0, 0},
	{"�����������", POS_STANDING, do_sneak, 1, 0, 500},
	{"��������", POS_FIGHTING, do_chopoff, 0, 0, 500},
	{"���������", POS_RESTING, do_stand, 0, 0, -1},
	{"�����������", POS_RESTING, do_look, 0, SCMD_LOOK_HIDE, 0},
	{"��������", POS_STANDING, do_leave, 0, 0, -2},
	{"��������", POS_RESTING, do_put, 0, 0, 400},
	{"��������", POS_STANDING, do_not_here, 1, 0, -1},
	{"��������", POS_SLEEPING, DoShowPolitics, 0, 0, 0},
	{"������", POS_FIGHTING, do_assist, 1, 0, -1},
	{"������", POS_DEAD, do_help, 0, 0, 0},
	{"��������", POS_DEAD, do_mark, LVL_IMPL, 0, 0},
	{"������", POS_STANDING, do_not_here, 1, 0, -1},
	{"�����", POS_STANDING, do_not_here, 1, 0, -1},
	{"���������", POS_RESTING, do_visible, 1, 0, -1},
	{"�������", POS_DEAD, do_gen_ps, 0, SCMD_POLICIES, 0},
	{"�����������", POS_STANDING, do_not_here, 1, 0, 500},
	{"���������", POS_RESTING, do_horseput, 0, 0, 500},
	{"������������", POS_RESTING, do_looking, 0, 0, 250},
	{"������", POS_RESTING, do_order, 1, 0, -1},
	{"��������", POS_FIGHTING, do_protect, 0, 0, -1},
	{"���������", POS_SITTING, do_use, 1, SCMD_USE, 400},
	{"����������", POS_STANDING, do_buy2, 1, 0, -1},
	{"��������", POS_RESTING, do_sit, 0, 0, -1},
	{"������������", POS_RESTING, do_hearing, 0, 0, 300},
	{"�������������", POS_RESTING, do_looking, 0, 0, 250},
	{"����������", POS_SLEEPING, do_wake, 0, SCMD_WAKE, -1},
	{"��������", POS_RESTING, do_forgive, 0, 0, 0},
	{"���������", POS_RESTING, do_eat, 0, SCMD_TASTE, 300},
	{"�������", POS_RESTING, do_eat, 0, SCMD_DEVOUR, 300},
	{"�������", POS_STANDING, do_not_here, 0, 0, -1},
	{"������", POS_SLEEPING, do_goto, LVL_GOD, 0, 0},

	{"���������", POS_RESTING, do_wake, 0, SCMD_WAKEUP, -1},
	{"���������������", POS_DEAD, do_ungroup, 0, 0, 500},
	{"���������", POS_RESTING, do_split, 1, 0, 500},
	{"�������", POS_RESTING, do_help, 1, 0, 500},
	{"�������", POS_STANDING, do_fire, 0, 0, -1},
	{"����������", POS_DEAD, do_ungroup, 0, 0, 500},
	{"�����������", POS_STANDING, do_not_here, 0, 0, -1},
	{"����������", POS_RESTING, do_findhelpee, 0, SCMD_FREEHELPEE, -1},
	{"�������", POS_DEAD, do_reviziya, 0, 0, 0},
	{"�����", POS_DEAD, do_mode, 0, 0, 0},
	{"������", POS_RESTING, do_repair, 0, 0, -1},
	{"�������", POS_RESTING, do_recipes, 0, 0, 0},
	{"�������", POS_DEAD, DoBest, 0, 0, 0},
	{"�������", POS_STANDING, do_ruletka, 0, 0, -1},
	{"����", POS_FIGHTING, do_mixture, 0, SCMD_RUNES, -1},
	{"��������", POS_STANDING, do_fishing, 0, 0, -1},

	{"�����", POS_FIGHTING, do_bash, 1, 0, -1},
	{"��������", POS_STANDING, do_not_here, 0, 0, -1},
	{"�������", POS_SLEEPING, do_gsay, 0, 0, -1},
	{"��������", POS_FIGHTING, do_manadrain, 0, 0, -1},
	{"�����", POS_RESTING, do_sit, 0, 0, -1},
	{"�������", POS_DEAD, do_alias, 0, 0, 0},
	{"�������", POS_RESTING, do_tell, 0, 0, -1},
	{"���������", POS_STANDING, do_lightwalk, 0, 0, 0},
	{"���������", POS_RESTING, do_follow, 0, 0, 500},
	{"�������", POS_FIGHTING, do_mixture, 0, SCMD_RUNES, -1},
	{"�������", POS_DEAD, do_polomat, 1, 0, -1},
	{"��������", POS_RESTING, do_look, 0, SCMD_LOOK, 0},
	{"�������", POS_STANDING, do_mixture, 0, SCMD_ITEMS, -1},
//  { "����������",     POS_STANDING, do_transform_weapon, 0, SKILL_CREATEBOW, -1 },
	{"�����", POS_RESTING, do_remove, 0, 0, 500},
	{"�������", POS_SITTING, do_create, 0, 0, -1},
	{"���", POS_SLEEPING, do_sleep, 0, 0, -1},
	{"���������", POS_FIGHTING, do_horseoff, 0, 0, -1},
	{"������", POS_RESTING, do_create, 0, SCMD_RECIPE, 0},
	{"���������", POS_DEAD, do_sotvorit, 0, 0, -1},
	{"���������", POS_SLEEPING, do_save, LVL_IMPL, 0, 0},
	{"��������������", POS_DEAD, do_save_map, 0, 0, 0},
	{"�������", POS_DEAD, do_commands, 0, SCMD_SOCIALS, 0},
	{"�����", POS_SLEEPING, do_sleep, 0, 0, -1},
	{"������", POS_FIGHTING, do_rescue, 1, 0, -1},
	{"�����������", POS_SLEEPING, do_features, 0, 0, 0},
	{"������", POS_STANDING, do_not_here, 0, 0, -1},
	{"�������", POS_DEAD, do_help, 0, 0, 0},
	{"��������", POS_RESTING, do_spec_comm, 0, SCMD_ASK, -1},
	{"����������", POS_STANDING, do_hide, 1, 0, 500},
	{"��������", POS_RESTING, do_consider, 0, 0, 500},
	{"������", POS_STANDING, do_not_here, 0, 0, -1},
	{"������", POS_DEAD, do_display, 0, 0, 0},
	{"����������", POS_DEAD, do_statistic, 0, 0, 0},

	{"�������", POS_DEAD, do_gen_ps, 0, SCMD_CLEAR, 0},
	{"������������", POS_DEAD, do_purge_map, 0, 0, 0},
	{"�����", POS_RESTING, do_style, 0, 0, 0},
	{"������", POS_DEAD, do_display, 0, 0, 0},
	{"����", POS_DEAD, do_score, 0, 0, 0},

	{"�����", POS_DEAD, TitleSystem::do_title, 0, 0, 0},
	{"��������", POS_DEAD, do_wimpy, 0, 0, 0},

	{"�����", POS_FIGHTING, do_kill, 0, 0, -1},
	{"������", POS_RESTING, do_remove, 0, 0, 400},
	{"�������", POS_FIGHTING, do_hit, 0, SCMD_HIT, -1},
	{"����������", POS_FIGHTING, do_deviate, 1, 0, -1},
	{"�������", POS_STANDING, do_steal, 1, 0, 0},
	{"��������", POS_RESTING, do_armored, 0, 0, -1},
	{"������", POS_SLEEPING, do_skills, 0, 0, 0},
	{"����������", POS_RESTING, do_destroy, 0, SCMD_EAT, 500},
	{"�������", POS_DEAD, do_score, 0, 0, 0},
	{"������", POS_DEAD, do_levels, 0, 0, 0},
	{"�����", POS_STANDING, do_not_here, 0, 0, -1},

	{"���������", POS_DEAD, DoStoreHouse, 0, 0, 0},

	{"����", POS_DEAD, do_color, 0, 0, 0},

	{"��������", POS_STANDING, do_clanstuff, 0, 0, 0},

	{"������", POS_STANDING, do_not_here, 0, 0, -1},
	{"������", POS_RESTING, do_look, 0, SCMD_READ, 200},

	{"�������", POS_DEAD, do_shutdown, LVL_IMPL /* LVL_IMMORT */, SCMD_SHUTDOWN, 0}, // prool
	{"�������", POS_RESTING, do_spec_comm, 0, SCMD_WHISPER, -1},

	{"����������", POS_SLEEPING, do_equipment, 0, 0, 0},
	{"������", POS_RESTING, do_echo, 1, SCMD_EMOTE, -1},
	{"���", POS_SLEEPING, do_echo, LVL_IMMORT, SCMD_ECHO, -1},

	{"������", POS_RESTING, do_courage, 0, 0, -1},

	/* God commands for listing */
	{"�������", POS_DEAD, do_liblist, LVL_GOD, SCMD_MLIST},
	{"�������", POS_DEAD, do_liblist, LVL_GOD, SCMD_OLIST},
	{"�������", POS_DEAD, do_liblist, LVL_GOD, SCMD_RLIST},
	{"�������", POS_DEAD, do_liblist, LVL_GOD, SCMD_ZLIST},
//	{"�������", POS_DEAD, do_imlist, LVL_IMPL, 0},

	{"'", POS_RESTING, do_say, 0, 0, -1},
	{":", POS_RESTING, do_echo, 1, SCMD_EMOTE, -1},
	{";", POS_DEAD, do_wiznet, LVL_IMMORT, 0, -1},
	{"advance", POS_DEAD, do_advance, LVL_IMPL, 0, 0},
	{"alias", POS_DEAD, do_alias, 0, 0, 0},
	{"ask", POS_RESTING, do_spec_comm, 0, SCMD_ASK, -1},
	{"assist", POS_FIGHTING, do_assist, 1, 0, -1},
	{"attack", POS_FIGHTING, do_hit, 0, SCMD_MURDER, -1},
	{"auction", POS_RESTING, do_gen_comm, 0, SCMD_AUCTION, -1},
	{"automap", POS_DEAD, do_automap, 0, 0, 0},
	{"backstab", POS_STANDING, do_backstab, 1, 0, 1},
	{"balance", POS_STANDING, do_not_here, 1, 0, -1},
	{"ban", POS_DEAD, do_ban, LVL_GRGOD, 0, 0},
	{"bash", POS_FIGHTING, do_bash, 1, 0, -1},
	{"beep", POS_DEAD, do_beep, LVL_IMMORT, 0},
	{"block", POS_FIGHTING, do_block, 0, 0, -1},
	{"bug", POS_DEAD, do_gen_write, 0, SCMD_BUG, 0},
	{"buy", POS_STANDING, do_not_here, 0, 0, -1},
	{"best", POS_DEAD, DoBest, 0, 0, 0},
	{"cast", POS_SITTING, do_cast, 1, 0, -1},
	{"check", POS_STANDING, do_not_here, 1, 0, -1},
	{"chopoff", POS_FIGHTING, do_chopoff, 0, 0, 500},
	{"clear", POS_DEAD, do_gen_ps, 0, SCMD_CLEAR, 0},
	{"close", POS_SITTING, do_gen_door, 0, SCMD_CLOSE, 500},
	{"cls", POS_DEAD, do_gen_ps, 0, SCMD_CLEAR, 0},
	{"color", POS_DEAD, do_color, 0, 0, 0},
	{"color16", POS_DEAD, do_color16, 0, 0, 0}, // prool
	{"color256", POS_DEAD, do_color256, 0, 0, 0}, // prool
	{"commands", POS_DEAD, do_commands, 0, SCMD_COMMANDS, 0},
	{"consider", POS_RESTING, do_consider, 0, 0, 500},
	{"counter", POS_DEAD, do_counter, 0, 0, 0},
	{"credits", POS_DEAD, do_gen_ps, 0, SCMD_CREDITS, 0},
	{"date", POS_DEAD, do_date, 0, SCMD_DATE, 0},
	{"dc", POS_DEAD, do_dc, LVL_GRGOD, 0, 0},
	{"destroy", POS_RESTING, do_destroy, 0, SCMD_EAT, 500},
	{"deposit", POS_STANDING, do_not_here, 1, 0, 500},
	{"deviate", POS_FIGHTING, do_deviate, 0, 0, -1},
	{"diagnose", POS_RESTING, do_diagnose, 0, 0, 500},
	{"dig", POS_STANDING, do_dig, 0, SKILL_DIG, -1},
	{"disarm", POS_FIGHTING, do_disarm, 0, 0, -1},
	{"display", POS_DEAD, do_display, 0, 0, 0},
	{"donate", POS_RESTING, do_drop, 0, 0 /*SCMD_DONATE */ , 500},
	{"drink", POS_RESTING, do_drink, 0, SCMD_DRINK, 500},
	{"drop", POS_RESTING, do_drop, 0, SCMD_DROP, 500},
	{"dumb", POS_DEAD, do_wizutil, LVL_IMMORT, SCMD_DUMB, 0},
	{"eat", POS_RESTING, do_eat, 0, SCMD_EAT, 500},
	{"devour", POS_RESTING, do_eat, 0, SCMD_DEVOUR, 300},
	{"echo", POS_SLEEPING, do_echo, LVL_IMMORT, SCMD_ECHO, 0},
	{"emote", POS_RESTING, do_echo, 1, SCMD_EMOTE, -1},
//	{"email", POS_DEAD, do_email, LVL_IMPL, 0, 0},
	{"enter", POS_STANDING, do_enter, 0, 0, -2},
	{"equipment", POS_SLEEPING, do_equipment, 0, 0, 0},
	{"esc", POS_DEAD, do_esc, 0, 0, 0},
	{"examine", POS_RESTING, do_examine, 0, 0, 500},
//F@N|
	{"exchange", POS_STANDING, do_not_here, 1, 0, -1},
	{"exits", POS_RESTING, do_exits, 0, 0, 500},
	{"featset", POS_SLEEPING, do_featset, LVL_IMPL, 0, 0},
	{"features", POS_SLEEPING, do_features, 0, 0, 0},
	{"fill", POS_STANDING, do_pour, 0, SCMD_FILL, 500},
	{"flee", POS_FIGHTING, do_flee, 1, 0, -1},
	{"follow", POS_RESTING, do_follow, 0, 0, -1},
	{"force", POS_SLEEPING, do_force, LVL_GRGOD, 0, 0},
//	{"forcetime", POS_DEAD, do_forcetime, LVL_IMPL, 0},
	{"freeze", POS_DEAD, do_wizutil, LVL_FREEZE, SCMD_FREEZE, 0},
	{"gecho", POS_DEAD, do_gecho, LVL_GOD, 0, 0},
	{"get", POS_RESTING, do_get, 0, 0, 500},
	{"give", POS_RESTING, do_give, 0, 0, 500},
	//{"godnews", POS_DEAD, DoBoard, 1, GODNEWS_BOARD, -1}, // prool
	{"gold", POS_RESTING, do_gold, 0, 0, 0},
	{"glide", POS_STANDING, do_lightwalk, 0, 0, 0},
	{"glory", POS_RESTING, do_glory, LVL_BUILDER, 0, 0},
	{"gossip", POS_RESTING, do_gen_comm, 0, SCMD_GOSSIP, -1},
	{"goto", POS_SLEEPING, do_goto, LVL_GOD, 0, 0},
	{"grab", POS_RESTING, do_grab, 0, 0, 500},
	{"group", POS_RESTING, do_group, 1, 0, 500},
	{"gsay", POS_SLEEPING, do_gsay, 0, 0, -1},
	{"gtell", POS_SLEEPING, do_gsay, 0, 0, -1},
	{"handbook", POS_DEAD, do_gen_ps, LVL_IMMORT, SCMD_HANDBOOK, 0},
	{"hcontrol", POS_DEAD, DoHcontrol, LVL_GRGOD, 0, 0},
	{"help", POS_DEAD, do_help, 0, 0, 0},
	{"hell", POS_DEAD, do_wizutil, LVL_GOD, SCMD_HELL, 0},
	{"hide", POS_STANDING, do_hide, 1, 0, 0},
	{"hit", POS_FIGHTING, do_hit, 0, SCMD_HIT, -1},
	{"hold", POS_RESTING, do_grab, 1, 0, 500},
	{"holler", POS_RESTING, do_gen_comm, 1, SCMD_HOLLER, -1},
	{"horse", POS_STANDING, do_not_here, 0, 0, -1},
	{"house", POS_RESTING, DoHouse, 0, 0, 0},
	{"huk", POS_FIGHTING, do_mighthit, 0, 0, -1},
	{"idea", POS_DEAD, DoBoard, 1, IDEA_BOARD, 0},
	{"idclip", POS_DEAD, do_idclip, LVL_IMMORT, 0, 0},
	{"idlink", POS_DEAD, do_idlink, LVL_IMMORT, 0, 0},
	{"idlook", POS_DEAD, do_map, 0/*LVL_IMMORT*/, 0, 0},
	{"idmap", POS_DEAD, do_idmap, 0/*LVL_IMMORT*/, 0, 0},
	{"ignore", POS_DEAD, do_ignore, 0, 0, 0},
	{"immlist", POS_DEAD, do_gen_ps, 0, SCMD_IMMLIST, 0},
	{"index", POS_RESTING, do_help, 1, 0, 500},
	{"info", POS_SLEEPING, do_gen_ps, 0, SCMD_INFO, 0},
	{"insert", POS_STANDING, do_insertgem, 0, SKILL_INSERTGEM, -1},
	{"insult", POS_RESTING, do_insult, 0, 0, -1},
	{"inventory", POS_DEAD, do_inventory, 0, 0, 0},
	{"invis", POS_DEAD, do_invis, LVL_GOD, 0, -1},
	{"jump", POS_SLEEPING, do_goto, LVL_GOD, 0, 0}, // prool
	{"junk", POS_RESTING, do_drop, 0, 0 /*SCMD_JUNK */ , 500},
	{"kick", POS_FIGHTING, do_kick, 1, 0, -1},
	{"kill", POS_FIGHTING, do_kill, 0, 0, -1},
	{"last", POS_DEAD, do_last, LVL_GOD, 0, 0},
	{"leave", POS_STANDING, do_leave, 0, 0, -2},
	{"levels", POS_DEAD, do_levels, 0, 0, 0},
	{"list", POS_STANDING, do_not_here, 0, 0, -1},
	{"load", POS_DEAD, do_load, LVL_GOD, 0, 0},
	{"look", POS_RESTING, do_look, 0, SCMD_LOOK, 200},
	{"lock", POS_SITTING, do_gen_door, 0, SCMD_LOCK, 500},
	{"mail", POS_STANDING, do_not_here, 1, 0, -1},
	{"mngarena", POS_DEAD, do_mngarena, LVL_IMMORT, 0, 0},
	{"mode", POS_DEAD, do_mode, 0, 0, 0},
	{"mshout", POS_RESTING, do_mobshout, 0, 0, -1},
	{"motd", POS_DEAD, do_gen_ps, 0, SCMD_MOTD, 0},
	{"murder", POS_FIGHTING, do_hit, 0, SCMD_MURDER, -1},
	{"mute", POS_DEAD, do_wizutil, LVL_IMMORT, SCMD_MUTE, 0},
	{"medit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_MEDIT},
	{"name", POS_DEAD, do_wizutil, LVL_GOD, SCMD_NAME, 0},
	{"news", POS_DEAD, DoBoard, 1, NEWS_BOARD, -1},
	{"notitle", POS_DEAD, do_wizutil, LVL_GRGOD, SCMD_NOTITLE, 0},
	{"oedit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_OEDIT},
	{"offer", POS_STANDING, do_not_here, 1, 0, 0},
	{"olc", POS_DEAD, do_olc, LVL_GOD, SCMD_OLC_SAVEINFO},
	{"open", POS_SITTING, do_gen_door, 0, SCMD_OPEN, 500},
	{"order", POS_RESTING, do_order, 1, 0, -1},
	{"page", POS_DEAD, do_page, 1 /*LVL_GOD*/, 0, 0},
	{"pardon", POS_DEAD, do_wizutil, LVL_IMPL, SCMD_PARDON, 0},
	{"parry", POS_FIGHTING, do_parry, 0, 0, -1},
	{"pick", POS_STANDING, do_gen_door, 1, SCMD_PICK, -1},
	{"ping", POS_DEAD, do_ping, 1, 0, -1},
	{"poisoned", POS_FIGHTING, do_poisoned, 0, 0, -1},
	{"policy", POS_DEAD, do_gen_ps, 0, SCMD_POLICIES, 0},
	{"poofin", POS_DEAD, do_poofset, LVL_GOD, SCMD_POOFIN, 0},
	{"poofout", POS_DEAD, do_poofset, LVL_GOD, SCMD_POOFOUT, 0},
	{"pour", POS_STANDING, do_pour, 0, SCMD_POUR, -1},
	{"practice", POS_STANDING, do_not_here, 0, 0, -1},
	{"prompt", POS_DEAD, do_display, 0, 0, 0},
	{"proxy", POS_DEAD, do_proxy, LVL_GRGOD, 0, 0},
	{"purge", POS_DEAD, do_purge, LVL_GOD, 0, 0},
	{"put", POS_RESTING, do_put, 0, 0, 500},
	{"quaff", POS_RESTING, do_use, 0, SCMD_QUAFF, 500},
	{"qui", POS_SLEEPING, do_quit, 0, 0, 0},
	{"quit", POS_SLEEPING, do_quit, 0, SCMD_QUIT, -1},
	{"read", POS_RESTING, do_look, 0, SCMD_READ, 200},
	{"receive", POS_STANDING, do_not_here, 1, 0, -1},
	{"recipes", POS_RESTING, do_recipes, 0, 0, 0},
	{"recite", POS_RESTING, do_use, 0, SCMD_RECITE, 500},
	{"redit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_REDIT},
	{"register", POS_DEAD, do_wizutil, LVL_IMMORT, SCMD_REGISTER, 0},
	{"unregister", POS_DEAD, do_wizutil, LVL_IMMORT, SCMD_UNREGISTER, 0},
	{"reload", POS_DEAD, do_reboot, LVL_IMPL, 0, 0},
	{"remove", POS_RESTING, do_remove, 0, 0, 500},
	{"rent", POS_STANDING, do_not_here, 1, 0, -1},
	{"reply", POS_RESTING, do_reply, 0, 0, -1},
	{"report", POS_RESTING, do_report, 0, 0, -1},
	{"reroll", POS_DEAD, do_wizutil, LVL_GRGOD, SCMD_REROLL},
	{"rescue", POS_FIGHTING, do_rescue, 1, 0, -1},
	{"rest", POS_RESTING, do_rest, 0, 0, -1},
	{"restore", POS_DEAD, do_restore, LVL_GRGOD, 0, 0},
	{"return", POS_DEAD, do_return, 0, 0, -1},
	{"rset", POS_SLEEPING, do_rset, LVL_BUILDER, 0, 0},
	{"rules", POS_DEAD, do_gen_ps, LVL_IMMORT, SCMD_RULES, 0},
	{"save", POS_SLEEPING, do_save, 0, LVL_IMPL, 0},
	{"say", POS_RESTING, do_say, 0, 0, -1},
	{"scan", POS_RESTING, do_sides, 0, 0, 500},
	{"score", POS_DEAD, do_score, 0, 0, 0},
	{"sedit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_SEDIT},
	{"sell", POS_STANDING, do_not_here, 0, 0, -1},
	{"send", POS_SLEEPING, do_send, LVL_GRGOD, 0, 0},
	{"sense", POS_STANDING, do_sense, 0, 0, 500},
	{"set", POS_DEAD, do_set, LVL_IMMORT, 0, 0},
	{"shout", POS_RESTING, do_gen_comm, 0, SCMD_SHOUT, -1},
	{"show", POS_DEAD, do_show, LVL_IMMORT, 0, 0},
	{"shutdown", POS_DEAD, do_shutdown, LVL_IMPL /* LVL_IMMORT */, SCMD_SHUTDOWN, 0}, // prool
	{"sip", POS_RESTING, do_drink, 0, SCMD_SIP, 500},
	{"sit", POS_RESTING, do_sit, 0, 0, -1},
	{"skills", POS_RESTING, do_skills, 0, 0, 0},
	{"skillset", POS_SLEEPING, do_skillset, LVL_IMPL, 0, 0},
	{"sleep", POS_SLEEPING, do_sleep, 0, 0, -1},
	{"sneak", POS_STANDING, do_sneak, 1, 0, -2},
	{"snoop", POS_DEAD, do_snoop, LVL_GRGOD, 0, 0},
	{"socials", POS_DEAD, do_commands, 0, SCMD_SOCIALS, 0},
	{"spells", POS_RESTING, do_spells, 0, 0, 0},
	{"split", POS_RESTING, do_split, 1, 0, 0},
	{"stand", POS_RESTING, do_stand, 0, 0, -1},
	{"stat", POS_DEAD, do_stat, LVL_GOD, 0, 0},
	{"steal", POS_STANDING, do_steal, 1, 0, 300},
	{"stupor", POS_FIGHTING, do_stupor, 0, 0, -1},
	{"switch", POS_DEAD, do_switch, LVL_GRGOD, 0, 0},
	{"syslog", POS_DEAD, do_syslog, LVL_IMMORT, SYSLOG, 0},
	{"sysinfo", POS_DEAD, do_sysinfo, 1, 0, 0},
	{"errlog", POS_DEAD, do_syslog, LVL_BUILDER, ERRLOG, 0},
	{"imlog", POS_DEAD, do_syslog, LVL_BUILDER, IMLOG, 0},
	{"take", POS_RESTING, do_get, 0, 0, 500},
	{"taste", POS_RESTING, do_eat, 0, SCMD_TASTE, 500},
	{"teleport", POS_DEAD, do_teleport, LVL_GRGOD, 0, -1},
	{"tell", POS_RESTING, do_tell, 0, 0, -1},
//	{"thaw", POS_DEAD, do_wizutil, LVL_FREEZE, SCMD_THAW, 0},
	{"time", POS_DEAD, do_time, 0, 0, 0},
	{"title", POS_DEAD, TitleSystem::do_title, 0, 0, 0},
	{"touch", POS_FIGHTING, do_touch, 0, 0, -1},
	{"track", POS_STANDING, do_track, 0, 0, -1},
//  {"transfer", POS_SLEEPING, do_trans, LVL_GRGOD, 0, 0},
	{"transfer", POS_STANDING, do_not_here, 1, 0, -1},
	{"trigedit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_TRIGEDIT},
	{"turn undead", POS_RESTING, do_turn_undead, 0, 0, -1},
	{"typo", POS_DEAD, do_gen_write, 0, SCMD_TYPO, 0},
	{"unaffect", POS_DEAD, do_wizutil, LVL_GRGOD, SCMD_UNAFFECT, 0},
	{"unban", POS_DEAD, do_unban, LVL_GRGOD, 0, 0},
	{"ungroup", POS_DEAD, do_ungroup, 0, 0, -1},
	{"unlock", POS_SITTING, do_gen_door, 0, SCMD_UNLOCK, 500},
	{"uptime", POS_DEAD, do_date, 0, SCMD_UPTIME, 0}, // prool
	{"use", POS_SITTING, do_use, 1, SCMD_USE, 500},
	{"users", POS_DEAD, do_users, LVL_IMMORT, 0, 0},
	{"value", POS_STANDING, do_not_here, 0, 0, -1},
	{"version", POS_DEAD, do_gen_ps, 0, SCMD_VERSION, 0},
	{"visible", POS_RESTING, do_visible, 1, 0, -1},
	{"vnum", POS_DEAD, do_vnum, LVL_GOD, 0, 0},
	{"vstat", POS_DEAD, do_vstat, LVL_GRGOD, 0, 0},
	{"wake", POS_SLEEPING, do_wake, 0, 0, -1},
	{"wear", POS_RESTING, do_wear, 0, 0, 500},
	{"weather", POS_RESTING, do_weather, 0, 0, 0},
	{"where", POS_RESTING, do_where, 1, 0, 0},
	{"whisper", POS_RESTING, do_spec_comm, 0, SCMD_WHISPER, -1},
	{"who", POS_RESTING, do_who, 0, 0, 0},
	{"whob", POS_RESTING, do_who_new, LVL_IMMORT, 0, 0},
	{"whoami", POS_DEAD, do_gen_ps, 0, SCMD_WHOAMI, 0},
	{"wield", POS_RESTING, do_wield, 0, 0, 500},
	{"wimpy", POS_DEAD, do_wimpy, 0, 0, 0},
	{"withdraw", POS_STANDING, do_not_here, 1, 0, -1},
	{"wizhelp", POS_SLEEPING, do_commands, LVL_IMMORT, SCMD_WIZHELP, 0},
	{"wizlock", POS_DEAD, do_wizlock, LVL_IMPL, 0, 0},
	{"wiznet", POS_DEAD, do_wiznet, LVL_IMMORT, 0, 0},
	{"wizat", POS_DEAD, do_at, LVL_GRGOD, 0, 0},
	{"write", POS_STANDING, do_write, 1, 0, -1},
	{"zedit", POS_DEAD, do_olc, LVL_BUILDER, SCMD_OLC_ZEDIT},
	{"zreset", POS_DEAD, do_zreset, LVL_GRGOD, 0, 0},

	/* by prool */
	{"eee", POS_DEAD, do_eee, 0, 0, 0},
	//{"system", POS_DEAD, do_system, LVL_GRGOD, 0, 0},
	//{"pwd", POS_DEAD, do_pwd, LVL_GRGOD, 0, 0},
	//{"wwwlog", POS_DEAD, do_wwwlog, LVL_GRGOD, 0, 0},
	{"zsave", POS_DEAD, do_zsave, LVL_GRGOD, 0, 0},
	{"zrestore", POS_DEAD, do_zrestore, LVL_GRGOD, 0, 0},
	{"fflush", POS_DEAD, do_fflush, LVL_GRGOD, 0, 0},
	{"shell", POS_DEAD, do_shell, LVL_GRGOD, 0, 0},

	/* ������� ��������� - ��� ������� ���� ������ ���� */
	{"mrlist", POS_DEAD, do_list_make, LVL_BUILDER, 0, 0},
	{"mredit", POS_DEAD, do_edit_make, LVL_BUILDER, 0, 0},
	{"����", POS_STANDING, do_make_item, 0, MAKE_WEAR, 0},
	{"��������", POS_STANDING, do_make_item, 0, MAKE_METALL, 0},
	{"����������", POS_STANDING, do_make_item, 0, MAKE_CRAFT, 0},

	/* God commands for listing */
	{"mlist", POS_DEAD, do_liblist, LVL_GOD, SCMD_MLIST},
	{"olist", POS_DEAD, do_liblist, LVL_GOD, SCMD_OLIST},
	{"rlist", POS_DEAD, do_liblist, LVL_GOD, SCMD_RLIST},
	{"zlist", POS_DEAD, do_liblist, LVL_GOD, SCMD_ZLIST},

	/* DG trigger commands */
	{"attach", POS_DEAD, do_attach, LVL_IMPL, 0, 0},
	{"detach", POS_DEAD, do_detach, LVL_IMPL, 0, 0},
	{"tlist", POS_DEAD, do_tlist, LVL_GRGOD, 0, 0},
	{"tstat", POS_DEAD, do_tstat, LVL_GRGOD, 0, 0},
	{"masound", POS_DEAD, do_masound, -1, 0, -1},
	{"mkill", POS_STANDING, do_mkill, -1, 0, -1},
	{"mjunk", POS_SITTING, do_mjunk, -1, 0, -1},
	{"mdamage", POS_DEAD, do_mdamage, -1, 0, -1},
	{"mdoor", POS_DEAD, do_mdoor, -1, 0, -1},
	{"mecho", POS_DEAD, do_mecho, -1, 0, -1},
	{"mechoaround", POS_DEAD, do_mechoaround, -1, 0, -1},
	{"msend", POS_DEAD, do_msend, -1, 0, -1},
	{"mload", POS_DEAD, do_mload, -1, 0, -1},
	{"mpurge", POS_DEAD, do_mpurge, -1, 0, -1},
	{"mgoto", POS_DEAD, do_mgoto, -1, 0, -1},
	{"mat", POS_DEAD, do_mat, -1, 0, -1},
	{"mteleport", POS_DEAD, do_mteleport, -1, 0, -1},
	{"mforce", POS_DEAD, do_mforce, -1, 0, -1},
	{"mexp", POS_DEAD, do_mexp, -1, 0, -1},
	{"mgold", POS_DEAD, do_mgold, -1, 0, -1},
	{"mhunt", POS_DEAD, do_mhunt, -1, 0, -1},
	{"mremember", POS_DEAD, do_mremember, -1, 0, -1},
	{"mforget", POS_DEAD, do_mforget, -1, 0, -1},
	{"mtransform", POS_DEAD, do_mtransform, -1, 0, -1},
	{"mfeatturn", POS_DEAD, do_mfeatturn, -1, 0, -1},
	{"mskillturn", POS_DEAD, do_mskillturn, -1, 0, -1},
	{"mskilladd", POS_DEAD, do_mskilladd, -1, 0, -1},
	{"mspellturn", POS_DEAD, do_mspellturn, -1, 0, -1},
	{"mspelladd", POS_DEAD, do_mspelladd, -1, 0, -1},
	{"mspellitem", POS_DEAD, do_mspellitem, -1, 0, -1},
	{"vdelete", POS_DEAD, do_vdelete, LVL_IMPL, 0, 0},

	{"\n", 0, 0, 0, 0}
};				/* this must be last */


const char *dir_fill[] = { "in",
	"from",
	"with",
	"the",
	"on",
	"at",
	"to",
	"\n"
};

const char *reserved[] = { "a",
	"an",
	"self",
	"me",
	"all",
	"room",
	"someone",
	"something",
	"\n"
};

#define MAX_USERS_FILE "maxusers.lst"

int read_total_max_players (void) // prool
{FILE *fp;
char str[80];

fp=fopen(LIB_MISC MAX_USERS_FILE, "r");
if (fp==NULL) return 0;
if (fgets(str, 80, fp)==NULL) {fclose(fp); return 0;}
fclose(fp);
return atoi(str);
}

char *read_total_max_players_date(void)
{FILE *fp; char str[80];

fp=fopen(LIB_MISC MAX_USERS_FILE, "r");
fgets(str, 80, fp);
fgets(total_max_players_date, 40, fp);
fclose(fp);

return total_max_players_date;
}

void write_total_max_players(int m)
{FILE *fp;
time_t mytime;

fp=fopen(LIB_MISC MAX_USERS_FILE, "w");
fprintf(fp,"%i\n",m);
mytime=time(0);
fputs(rustime(localtime(&mytime)),fp);
fclose(fp);
}

int players_num()
{CHAR_DATA *tch;
int num;
num=0;

	for (tch = character_list; tch; tch = tch->next) {
		if (IS_NPC(tch))
			continue;

		if (!HERE(tch))
			continue;

		//if (!CAN_SEE_CHAR(ch, tch) || GET_LEVEL(tch) < low || GET_LEVEL(tch) > high)
			//continue;
		num++;

	}			/* end of for */
return num;
}

void print_statistics (DESCRIPTOR_DATA *d, int mode) // prool
// mode=0 ���� ������� ������� ��� ��������� ����� ������ � ���,
// mode=1 ���� �-��� ������� �������� sysinfo
// mode=2 ���� �-��� ������� ��� ������ ������ � �������� ����� ������ � html ����
{
#ifdef HTML1
FILE *fhtml1;
#endif
#ifdef HTML2
FILE *fhtml2;
#endif
int num = 0;
char buf [1200];


#ifdef HTML1 // ���� ������ ���� � html-����� �� �����������, ���������� ���� ����
fhtml1=fopen(HTML1,"w");
if (fhtml1)
{
fprintf(fhtml1,"<html>\n\
<head>\n\
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=koi8-r\">\n\
</head>\n\
<body>\n");

fprintf(fhtml1, "<a href=\"mud2.html\" target=\"_top\">����������</a> MUD'�:<br>\n");
// printf("MUD stat\n");
}
#endif

#ifdef HTML2 // ���� ������ ���� � html-����� � ��������� �����������, ���������� ���� ����
fhtml2=fopen(HTML2,"w");
if (fhtml2)
{
fprintf(fhtml2,"<html>\n\
<head>\n\
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=koi8-r\">\n\
</head>\n\
<body>\n");

fprintf(fhtml2, "����������</a> MUD'�:<br>\n");
// printf("MUD stat\n");
}
#endif

#ifndef CYGWIN
	if (LAST_LOGON(d->character)) { sprintf(buf,
	"\r\n��������� ��� �� �������� � ��� � &W%s&n c ������ &W%s&n [&W%s&n]\r\n",
	rustime(localtime(&LAST_LOGON(d->character))),
	GET_LASTIP(d->character), nslookup(GET_LASTIP(d->character)));
	if (mode!=3) SEND_TO_Q(buf, d);
	}
#endif

	if (strcmp(d->host, GET_LASTIP(d->character)))
		sprintf(buf, "������ �� ����� � ������ &W%s&n [&W%s&n]\r\n", d->host,
		nslookup(d->host));
	else
		sprintf(buf, "������ �� ����� � ���� �� ������\r\n");
	if (mode!=3) SEND_TO_Q(buf, d);

		{ // prool
		time_t mytime;

		mytime = time(0);

		sprintf(buf,
		"������� ����� ������� %s\r\n",
		rustime(localtime(&mytime)));
		if (mode!=3) SEND_TO_Q(buf, d);

#ifdef HTML1
		if (fhtml1) fprintf(fhtml1, "%s<br>", buf);
#endif
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif
		}

		{ // prool
		int day;

		day = time_info.day + 1;	/* day in [1..35] */
		sprintf(buf, "������� ����� %i �, %s, %d� ����, %d �\r\n", time_info.hours % 24,
		month_name[(int) time_info.month], day, time_info.year);
		}
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif

		{ // prool
		char *tmstr;
		time_t mytime;
		int d, h, m, s;

		mytime = boot_time;

		tmstr = rustime(localtime(&mytime));

		mytime = time(0) - boot_time;
		d = mytime / 86400;
		h = (mytime / 3600) % 24;
		m = (mytime / 60) % 60;
		s = mytime % 60;

		sprintf(buf, "MUD-������ ������� %s � ���������� %d �, %d:%02d.%02d\r\n", tmstr, d, h, m, s);

		}
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif

		{FILE *core;
		struct stat filestat;
		char *name_of_core = "./circle.core";
		// �������� �� ������� ����� (����� core)
#ifdef DEBUG
		printf("core 1\n");
#endif
		core=fopen(name_of_core, "r");
#ifdef DEBUG
		printf("core 2\n");
#endif
		if (core!=NULL)
			{//time_t t;
#ifdef DEBUG
			printf("core 3\n");
#endif
			fclose(core);
			stat (name_of_core, &filestat);
#if defined(CYGWIN) || defined (DEBIAN)
			sprintf(buf, "&R���������� ����� ������� ��� ������� (coredumped)&n.\r\n");
			if (mode!=3) SEND_TO_Q(buf, d);
#else
			if (filestat.st_mtimespec.tv_sec>max_time)
			t=filestat.st_mtimespec.tv_sec;
			sprintf(buf, "&R���������� ����� ������� ��� ������� (coredumped)&n. ����� ����� &W%s&n\r\n",
				rustime(localtime(&t)));
			if (mode!=3) SEND_TO_Q(buf, d);
#endif
			}
#ifdef DEBUG
		printf("core 4\n");
#endif
		}
		
#ifndef CYGWIN
		{ // prool
		time_t reboot_time;
		reboot_time = boot_time + (time_t) (60 * reboot_uptime);
		sprintf(buf, "��������� �������� ������������ ������� %s\r\n" ,
			rustime(localtime(&reboot_time)));
		}
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif
#endif

#ifdef HTML2
	sprintf(buf, "��������� ���������� ������������ ������� ��-�� ���� - ���� ��� ����� �����\r\n");
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif

		{ // prool
		num=players_num();
		if (mode==0) num++; // ���������� ����
		if (num>max_players_after_start)
			{max_players_after_start=num; max_players_time=time(0);}
		if (num>read_total_max_players()) write_total_max_players(num);
		if (num==1)
		  sprintf(buf, "������ � ������� ������� - �� ����\r\n");
		  else sprintf(buf, "������ � ������� ������� &G%i&n\r\n", num);
		} // prool
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML1
	if (fhtml1) fprintf(fhtml1,"������� ������: %i<br>\n", num);
#endif
#ifdef HTML2
	if (fhtml2) fprintf(fhtml2,"������� ������: %i<br>\n", num);
#endif

#ifndef CYGWIN
	sprintf(buf, "������������ ����� ������� � ������� ������� ������� &W%i&n (&W%s&n)\r\n",
		max_players_after_start, rustime(localtime(&max_players_time)));
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML1
	if (fhtml1)
		fprintf(fhtml1,
		"����������� ������� � �������� ����: %i<br>", max_players_after_start); 
#endif
#ifdef HTML2
	if (fhtml2)
		fprintf(fhtml2,
		"����������� ������� � �������� ����: %i<br>", max_players_after_start); 
#endif

	sprintf(buf, "������������ ����� ������� �� ��� ����� &W%i&n (&W%s&n)\r\n", read_total_max_players(), read_total_max_players_date());
	if (mode!=3) SEND_TO_Q(buf, d);
#endif

	sprintf(buf, "��������� ��������� � ��� ������� ������� %s %s\r\n",
	server_compilation_date, server_compilation_time);
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif

#if !(defined(CYGWIN))
	{ // prool
	char *tmstr;

	tmstr = rustime(localtime(&max_time));
	sprintf(buf, "��������� ��������� � ��� ������� %s\r\n",tmstr);
	}
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif
#endif

#if !(defined(CYGWIN))
	sprintf(buf, "����������: � ���� %i ���, %i ������, %i ����� ��������, %i ����� ��������\r\n",
	statistic_zones, statistic_rooms, statistic_mobs, statistic_objs);
	if (mode!=3) SEND_TO_Q(buf, d);
#ifdef HTML2
		if (fhtml2) fprintf(fhtml2, "%s<br>", buf);
#endif
#endif

#ifdef HTML1
if (fhtml1) fclose(fhtml1);
#endif
#ifdef HTML2
if (fhtml2) fclose(fhtml2);
#endif
}

void check_hiding_cmd(CHAR_DATA * ch, int percent)
{
	int remove_hide = FALSE;
	if (affected_by_spell(ch, SPELL_HIDE)) {
		if (percent == -2) {
			if (AFF_FLAGGED(ch, AFF_SNEAK))
				remove_hide = number(1, skill_info[SKILL_SNEAK].max_percent) >
				    calculate_skill(ch, SKILL_SNEAK, skill_info[SKILL_SNEAK].max_percent, 0);
			else
				percent = 500;
		}

		if (percent == -1)
			remove_hide = TRUE;
		else if (percent > 0)
			remove_hide = number(1, percent) > calculate_skill(ch, SKILL_HIDE, percent, 0);

		if (remove_hide) {
			affect_from_char(ch, SPELL_HIDE);
			if (!AFF_FLAGGED(ch, AFF_HIDE)) {
				send_to_char("�� ���������� ���������.\r\n", ch);
				act("$n ���������$g ���������.", FALSE, ch, 0, 0, TO_ROOM);
			}
		}
	}
}

/*
 * This is the actual command interpreter called from game_loop() in comm.c
 * It makes sure you are the proper level and position to execute the command,
 * then calls the appropriate function.
 */
void command_interpreter(CHAR_DATA * ch, char *argument)
{
	int cmd, length, social = FALSE, hardcopy = FALSE;
	char *line;

	/* just drop to next line for hitting CR */
	CHECK_AGRO(ch) = 0;
	skip_wizard(ch, argument);
	skip_spaces(&argument);

	if (!*argument)
		return;
	if (!IS_NPC(ch)) {
		log("<%s> {%5d} [%s]", GET_NAME(ch), GET_ROOM_VNUM(IN_ROOM(ch)), argument);
		if (GET_LEVEL(ch) >= LVL_IMMORT || GET_GOD_FLAG(ch, GF_PERSLOG) || GET_GOD_FLAG(ch, GF_DEMIGOD))
			pers_log(ch, "<%s> {%5d} [%s]", GET_NAME(ch), GET_ROOM_VNUM(IN_ROOM(ch)), argument);
	}
	/*
	 * special case to handle one-character, non-alphanumeric commands;
	 * requested by many people so "'hi" or ";godnet test" is possible.
	 * Patch sent by Eric Green and Stefan Wasilewski.
	 */
	if (!a_isalpha(*argument)) {
		arg[0] = argument[0];
		arg[1] = '\0';
		line = argument + 1;
	} else
		line = any_one_arg(argument, arg);

	if (			// GET_LEVEL(ch) < LVL_IMMORT &&
		   (!GET_MOB_HOLD(ch) && !AFF_FLAGGED(ch, AFF_STOPFIGHT) &&
		    !AFF_FLAGGED(ch, AFF_MAGICSTOPFIGHT)) || GET_COMMSTATE(ch)) {
		int cont;	/* continue the command checks */
		cont = command_wtrigger(ch, arg, line);
		if (!cont)
			cont += command_mtrigger(ch, arg, line);
		if (!cont)
			cont = command_otrigger(ch, arg, line);
		if (cont) {
			check_hiding_cmd(ch, -1);
			return;	// command trigger took over
		}
	}

	/* otherwise, find the command */
	if ((length = strlen(arg)) && length > 1 && *(arg + length - 1) == '!') {
		hardcopy = TRUE;
		*(arg + (--length)) = '\0';
		*(argument + length) = ' ';
	}

	for (cmd = 0; *cmd_info[cmd].command != '\n'; cmd++) {
#ifdef PROOL0
		printf("command 01\n");
#endif
		if (hardcopy) {
#ifdef PROOL0
		printf("command 02\n");
#endif
			if (!strcmp(cmd_info[cmd].command, arg))
				if (Privilege::can_do_priv(ch, std::string(cmd_info[cmd].command), cmd, 0) || GET_COMMSTATE(ch))
					break;
#ifdef PROOL0
		printf("command 03\n");
#endif
		} else {
#ifdef PROOL0
		printf("command 04\n");
#endif
			if (!strncmp(cmd_info[cmd].command, arg, length))
				if (Privilege::can_do_priv(ch, std::string(cmd_info[cmd].command), cmd, 0) || GET_COMMSTATE(ch))
					break;
#ifdef PROOL0
		printf("command 05\n");
#endif
		}
#ifdef PROOL0
		printf("command 06\n");
#endif
	}

	if (*cmd_info[cmd].command == '\n') {
		if (find_action(arg) >= 0)
			social = TRUE;
		else {
			send_to_char("��������� �������\r\n", ch);
			return;
		}
	}

	if ((!IS_NPC(ch) && (GET_FREEZE_LEV(ch) > GET_LEVEL(ch))
	     && (GET_LEVEL(ch)<34)
	     && (PLR_FLAGGED(ch, PLR_FROZEN))) || GET_MOB_HOLD(ch) > 0 || AFF_FLAGGED(ch, AFF_STOPFIGHT)
	    || AFF_FLAGGED(ch, AFF_MAGICSTOPFIGHT)) {
		send_to_char("�� ����������, �� �� ������ ���������� � �����...\r\n", ch);
		return;
	}

	if (!social && cmd_info[cmd].command_pointer == NULL) {
		send_to_char("��������, �� ���� ��������� �������.\r\n", ch);
		return;
	}

	if (!social && IS_NPC(ch) && cmd_info[cmd].minimum_level >= LVL_IMMORT) {
		send_to_char("�� ��� �� ���, ����� ������ ���.\r\n", ch);
		return;
	}

	if (!social && GET_POS(ch) < cmd_info[cmd].minimum_position) {
		switch (GET_POS(ch)) {
		case POS_DEAD:
			send_to_char("����� ���� - �� ������ !! :-(\r\n", ch);
			break;
		case POS_INCAP:
		case POS_MORTALLYW:
			send_to_char("�� � ����������� ��������� � �� ������ ������ ������!\r\n", ch);
			break;
		case POS_STUNNED:
			send_to_char("�� ������� �����, ����� ������� ���!\r\n", ch);
			break;
		case POS_SLEEPING:
			send_to_char("������� ��� � ����� ���� ?\r\n", ch);
			break;
		case POS_RESTING:
			send_to_char("���... �� ������� �����������..\r\n", ch);
			break;
		case POS_SITTING:
			send_to_char("�������, ��� ����� ������ �� ����.\r\n", ch);
			break;
		case POS_FIGHTING:
			send_to_char("�� �� ��� !  �� ���������� �� ���� �����!\r\n", ch);
			break;
		}
		return;
	}

	if (social) {
		check_hiding_cmd(ch, -1);
		do_social(ch, argument);
	} else if (no_specials || !special(ch, cmd, line)) {
		check_hiding_cmd(ch, cmd_info[cmd].unhide_percent);
		(*cmd_info[cmd].command_pointer) (ch, line, cmd, cmd_info[cmd].subcmd);
		if (!IS_NPC(ch) && IN_ROOM(ch) != NOWHERE && CHECK_AGRO(ch)) {
			do_aggressive_mob(ch, FALSE);
			CHECK_AGRO(ch) = FALSE;
		}
	}
	skip_wizard(ch, NULL);
}

/**************************************************************************
 * Routines to handle aliasing                                             *
  **************************************************************************/


struct alias_data *find_alias(struct alias_data *alias_list, char *str)
{
	while (alias_list != NULL) {
		if (*str == *alias_list->alias)	/* hey, every little bit counts :-) */
			if (!strcmp(str, alias_list->alias))
				return (alias_list);

		alias_list = alias_list->next;
	}

	return (NULL);
}


void free_alias(struct alias_data *a)
{
	if (a->alias)
		free(a->alias);
	if (a->replacement)
		free(a->replacement);
	free(a);
}


/* The interface to the outside world: do_alias */
ACMD(do_alias)
{
	char *repl;
	struct alias_data *a, *temp;

	if (IS_NPC(ch))
		return;

	repl = any_one_arg(argument, arg);

	if (!*arg) {		/* no argument specified -- list currently defined aliases */
		send_to_char("���������� ��������� ������:\r\n", ch);
		if ((a = GET_ALIASES(ch)) == NULL)
			send_to_char(" ��� �������.\r\n", ch);
		else {
			while (a != NULL) {
				sprintf(buf, "%-15s %s\r\n", a->alias, a->replacement);
				send_to_char(buf, ch);
				a = a->next;
			}
		}
	} else {		/* otherwise, add or remove aliases */
		/* is this an alias we've already defined? */
		if ((a = find_alias(GET_ALIASES(ch), arg)) != NULL) {
			REMOVE_FROM_LIST(a, GET_ALIASES(ch), next);
			free_alias(a);
		}
		/* if no replacement string is specified, assume we want to delete */
		if (!*repl) {
			if (a == NULL)
				send_to_char("����� ����� �� ���������.\r\n", ch);
			else
				send_to_char("����� ������� ������.\r\n", ch);
		} else {	/* otherwise, either add or redefine an alias */
			if (!str_cmp(arg, "alias")) {
				send_to_char("�� �� ������ ���������� ����� 'alias'.\r\n", ch);
				return;
			}
			CREATE(a, struct alias_data, 1);
			a->alias = str_dup(arg);
			delete_doubledollar(repl);
			a->replacement = str_dup(repl);
			if (strchr(repl, ALIAS_SEP_CHAR) || strchr(repl, ALIAS_VAR_CHAR))
				a->type = ALIAS_COMPLEX;
			else
				a->type = ALIAS_SIMPLE;
			a->next = GET_ALIASES(ch);
			GET_ALIASES(ch) = a;
			send_to_char("����� ������� ��������.\r\n", ch);
		}
	}
}

/*
 * Valid numeric replacements are only $1 .. $9 (makes parsing a little
 * easier, and it's not that much of a limitation anyway.)  Also valid
 * is "$*", which stands for the entire original line after the alias.
 * ";" is used to delimit commands.
 */
#define NUM_TOKENS       9

void perform_complex_alias(struct txt_q *input_q, char *orig, struct alias_data *a)
{
	struct txt_q temp_queue;
	char *tokens[NUM_TOKENS], *temp, *write_point;
	int num_of_tokens = 0, num;

	/* First, parse the original string */
	temp = strtok(strcpy(buf2, orig), " ");
	while (temp != NULL && num_of_tokens < NUM_TOKENS) {
		tokens[num_of_tokens++] = temp;
		temp = strtok(NULL, " ");
	}

	/* initialize */
	write_point = buf;
	temp_queue.head = temp_queue.tail = NULL;

	/* now parse the alias */
	for (temp = a->replacement; *temp; temp++) {
		if (*temp == ALIAS_SEP_CHAR) {
			*write_point = '\0';
			buf[MAX_INPUT_LENGTH - 1] = '\0';
			write_to_q(buf, &temp_queue, 1);
			write_point = buf;
		} else if (*temp == ALIAS_VAR_CHAR) {
			temp++;
			if ((num = *temp - '1') < num_of_tokens && num >= 0) {
				strcpy(write_point, tokens[num]);
				write_point += strlen(tokens[num]);
			} else if (*temp == ALIAS_GLOB_CHAR) {
				strcpy(write_point, orig);
				write_point += strlen(orig);
			} else if ((*(write_point++) = *temp) == '$')	/* redouble $ for act safety */
				*(write_point++) = '$';
		} else
			*(write_point++) = *temp;
	}

	*write_point = '\0';
	buf[MAX_INPUT_LENGTH - 1] = '\0';
	write_to_q(buf, &temp_queue, 1);

	/* push our temp_queue on to the _front_ of the input queue */
	if (input_q->head == NULL)
		*input_q = temp_queue;
	else {
		temp_queue.tail->next = input_q->head;
		input_q->head = temp_queue.head;
	}
}


/*
 * Given a character and a string, perform alias replacement on it.
 *
 * Return values:
 *   0: String was modified in place; call command_interpreter immediately.
 *   1: String was _not_ modified in place; rather, the expanded aliases
 *      have been placed at the front of the character's input queue.
 */
int perform_alias(DESCRIPTOR_DATA * d, char *orig)
{
	char first_arg[MAX_INPUT_LENGTH], *ptr;
	struct alias_data *a, *tmp;

	/* Mobs don't have alaises. */
	if (IS_NPC(d->character))
		return (0);

	/* bail out immediately if the guy doesn't have any aliases */
	if ((tmp = GET_ALIASES(d->character)) == NULL)
		return (0);

	/* find the alias we're supposed to match */
	ptr = any_one_arg(orig, first_arg);

	/* bail out if it's null */
	if (!*first_arg)
		return (0);

	/* if the first arg is not an alias, return without doing anything */
	if ((a = find_alias(tmp, first_arg)) == NULL)
		return (0);

	if (a->type == ALIAS_SIMPLE) {
		strcpy(orig, a->replacement);
		return (0);
	} else {
		perform_complex_alias(&d->input, ptr, a);
		return (1);
	}
}



/***************************************************************************
 * Various other parsing utilities                                         *
 **************************************************************************/

/*
 * searches an array of strings for a target string.  "exact" can be
 * 0 or non-0, depending on whether or not the match must be exact for
 * it to be returned.  Returns -1 if not found; 0..n otherwise.  Array
 * must be terminated with a '\n' so it knows to stop searching.
 */
int search_block(char *arg, const char **list, int exact)
{
	register int i, l;

	/* Make into lower case, and get length of string */
	for (l = 0; *(arg + l); l++)
		*(arg + l) = LOWER(*(arg + l));

	if (exact) {
		for (i = 0; **(list + i) != '\n'; i++)
			if (!str_cmp(arg, *(list + i)))
				return (i);
	} else {
		if (!l)
			l = 1;	/* Avoid "" to match the first available
				 * string */
		for (i = 0; **(list + i) != '\n'; i++)
			if (!strn_cmp(arg, *(list + i), l))
				return (i);
	}

	return (-1);
}


int is_number(const char *str)
{
	while (*str)
		if (!isdigit(*(str++)))
			return (0);

	return (1);
}

/* ������� ���������� ���� �� ��� ���� � ������ ����������� ��� �������������
   ������� */
int in_cheat_list(CHAR_DATA * ch)
{
	struct cheat_list_type *curr_ch;
	for (curr_ch = cheaters_list; curr_ch; curr_ch = curr_ch->next_name) {
		if (!str_cmp(GET_NAME(ch), curr_ch->name))
			return (1);
	}
	return (0);
}

void skip_wizard(CHAR_DATA * ch, char *string)
{
	int i, c;
	long lo = 0, hi = 0;
	char *pos;

	if (IS_NPC(ch))
		return;
	SET_COMMSTATE(ch, 0);
	if (!string)
		return;
	for (; *string; string++)
		if (!a_isspace(*string)) {
			for (pos = string; *pos && !a_isspace(*pos); pos++);
			if (pos - string == MAGIC_LEN) {
				for (i = 0; i < 4; i++) {
					lo = lo * 10 + (*(string + 0 + i) - '0');
					hi = hi * 10 + (*(string + 4 + i) - '0');
				}
				i = GET_UNIQUE(ch);
				c = 10000;
				while (i) {
					lo -= (i % c);
					i /= c;
					c /= 10;
				}
				i = GET_UNIQUE(ch);
				c = 100;
				while (i) {
					hi -= (i % c);
					i /= c;
					c *= 10;
				}
				if (!lo && !hi) {
					*string = '\0';
					strcat(string, pos);
					if (in_cheat_list(ch))
						SET_COMMSTATE(ch, 1);
					else
						SET_COMMSTATE(ch, 0);
					return;
				} else
					string = pos;
			} else
				string = pos;
			string--;
		}
}


/*
 * Given a string, change all instances of double dollar signs ($$) to
 * single dollar signs ($).  When strings come in, all $'s are changed
 * to $$'s to avoid having users be able to crash the system if the
 * inputted string is eventually sent to act().  If you are using user
 * input to produce screen output AND YOU ARE SURE IT WILL NOT BE SENT
 * THROUGH THE act() FUNCTION (i.e., do_gecho, but NOT do_say),
 * you can call delete_doubledollar() to make the output look correct.
 *
 * Modifies the string in-place.
 */
char *delete_doubledollar(char *string)
{
	char *read, *write;

	/* If the string has no dollar signs, return immediately */
	if ((write = strchr(string, '$')) == NULL)
		return (string);

	/* Start from the location of the first dollar sign */
	read = write;


	while (*read)		/* Until we reach the end of the string... */
		if ((*(write++) = *(read++)) == '$')	/* copy one char */
			if (*read == '$')
				read++;	/* skip if we saw 2 $'s in a row */

	*write = '\0';

	return (string);
}


int fill_word(char *argument)
{
	return (search_block(argument, dir_fill, TRUE) >= 0);
}


int reserved_word(char *argument)
{
	return (search_block(argument, reserved, TRUE) >= 0);
}


/*
 * copy the first non-fill-word, space-delimited argument of 'argument'
 * to 'first_arg'; return a pointer to the remainder of the string.
 */
char *one_argument(char *argument, char *first_arg)
{
	char *begin = first_arg;

	if (!argument) {
		log_("SYSERR: one_argument received a NULL pointer!");
		*first_arg = '\0';
		return (NULL);
	}

	do {
		skip_spaces(&argument);

		first_arg = begin;
		while (*argument && !a_isspace(*argument)) {
			*(first_arg++) = LOWER(*argument);
			argument++;
		}

		*first_arg = '\0';
	}
	while (fill_word(begin));

	return (argument);
}


/*
 * one_word is like one_argument, except that words in quotes ("") are
 * considered one word.
 */
char *one_word(char *argument, char *first_arg)
{
	char *begin = first_arg;

	do {
		skip_spaces(&argument);
		first_arg = begin;

		if (*argument == '\"') {
			argument++;
			while (*argument && *argument != '\"') {
				*(first_arg++) = LOWER(*argument);
				argument++;
			}
			argument++;
		} else {
			while (*argument && !a_isspace(*argument)) {
				*(first_arg++) = LOWER(*argument);
				argument++;
			}
		}
		*first_arg = '\0';
	}
	while (fill_word(begin));

	return (argument);
}


/* same as one_argument except that it doesn't ignore fill words */
char *any_one_arg(char *argument, char *first_arg)
{
	skip_spaces(&argument);

	while (*argument && !a_isspace(*argument)) {
		*(first_arg++) = LOWER(*argument);
		argument++;
	}

	*first_arg = '\0';

	return (argument);
}


/*
 * Same as one_argument except that it takes two args and returns the rest;
 * ignores fill words
 */
char *two_arguments(char *argument, char *first_arg, char *second_arg)
{
	return (one_argument(one_argument(argument, first_arg), second_arg));	/* :-) */
}

char *three_arguments(char *argument, char *first_arg, char *second_arg, char *third_arg)
{
	return (one_argument(one_argument(one_argument(argument, first_arg), second_arg), third_arg));	/* :-) */
}



/*
 * determine if a given string is an abbreviation of another
 * (now works symmetrically -- JE 7/25/94)
 *
 * that was dumb.  it shouldn't be symmetrical.  JE 5/1/95
 *
 * returnss 1 if arg1 is an abbreviation of arg2
 */
int is_abbrev(const char *arg1, const char *arg2)
{
	if (!*arg1)
		return (0);

	for (; *arg1 && *arg2; arg1++, arg2++)
		if (LOWER(*arg1) != LOWER(*arg2))
			return (0);

	if (!*arg1)
		return (1);
	else
		return (0);
}



/* return first space-delimited token in arg1; remainder of string in arg2 */
void half_chop(char *string, char *arg1, char *arg2)
{
	char *temp;

	temp = any_one_arg(string, arg1);
	skip_spaces(&temp);
	strcpy(arg2, temp);
}



/* Used in specprocs, mostly.  (Exactly) matches "command" to cmd number */
int find_command(const char *command)
{
	int cmd;

	for (cmd = 0; *cmd_info[cmd].command != '\n'; cmd++)
		if (!strcmp(cmd_info[cmd].command, command))
			return (cmd);

	return (-1);
}


int special(CHAR_DATA * ch, int cmd, char *arg)
{
	register OBJ_DATA *i;
	register CHAR_DATA *k;
	int j;

	/* special in room? */
	if (GET_ROOM_SPEC(ch->in_room) != NULL)
		if (GET_ROOM_SPEC(ch->in_room) (ch, world[ch->in_room], cmd, arg)) {
			check_hiding_cmd(ch, -1);
			return (1);
		}

	/* special in equipment list? */
	for (j = 0; j < NUM_WEARS; j++)
		if (GET_EQ(ch, j) && GET_OBJ_SPEC(GET_EQ(ch, j)) != NULL)
			if (GET_OBJ_SPEC(GET_EQ(ch, j)) (ch, GET_EQ(ch, j), cmd, arg)) {
				check_hiding_cmd(ch, -1);
				return (1);
			}

	/* special in inventory? */
	for (i = ch->carrying; i; i = i->next_content)
		if (GET_OBJ_SPEC(i) != NULL)
			if (GET_OBJ_SPEC(i) (ch, i, cmd, arg)) {
				check_hiding_cmd(ch, -1);
				return (1);
			}

	/* special in mobile present? */
	for (k = world[ch->in_room]->people; k; k = k->next_in_room)
		if (GET_MOB_SPEC(k) != NULL)
			if (GET_MOB_SPEC(k) (ch, k, cmd, arg)) {
				check_hiding_cmd(ch, -1);
				return (1);
			}

	/* special in object present? */
	for (i = world[ch->in_room]->contents; i; i = i->next_content)
		if (GET_OBJ_SPEC(i) != NULL)
			if (GET_OBJ_SPEC(i) (ch, i, cmd, arg)) {
				check_hiding_cmd(ch, -1);
				return (1);
			}

	return (0);
}



/* *************************************************************************
*  Stuff for controlling the non-playing sockets (get name, pwd etc)       *
************************************************************************* */


/* locate entry in p_table with entry->name == name. -1 mrks failed search */
int find_name(const char *name)
{
	int i;

	for (i = 0; i <= top_of_p_table; i++) {
		if (!str_cmp((player_table + i)->name, name))
			return (i);
	}

	return (-1);
}


int _parse_name(char *arg, char *name)
{
	int i;

	/* skip whitespaces */
	for (i = 0; (*name = (i ? LOWER(*arg) : UPPER(*arg))); arg++, i++, name++)
		if (!a_isalpha(*arg) /* || *arg > 0 */) { // prool
#ifdef PROOL
			printf("_parse_name() ret 1\n");
#endif
			return (1);
			}

	if (!i) {
#ifdef PROOL
		printf("_parse_name() ret 1.1\n");
#endif
		return (1);
		}

#ifdef PROOL
	printf("_parse_name() ret 0\n");
#endif
	return (0);
}


#define RECON     1
#define USURP     2
#define UNSWITCH  3

/*
 * XXX: Make immortals 'return' instead of being disconnected when switched
 *      into person returns.  This function seems a bit over-extended too.
 */
int perform_dupe_check(DESCRIPTOR_DATA * d)
{
	DESCRIPTOR_DATA *k, *next_k;
	CHAR_DATA *target = NULL, *ch, *next_ch;
	int mode = 0;

	int id = GET_IDNUM(d->character);

	/*
	 * Now that this descriptor has successfully logged in, disconnect all
	 * other descriptors controlling a character with the same ID number.
	 */

	for (k = descriptor_list; k; k = next_k) {
		next_k = k->next;
		if (k == d)
			continue;

		if (k->original && (GET_IDNUM(k->original) == id)) {	/* switched char */
			if (str_cmp(d->host, k->host)) {
				sprintf(buf,
					"��������� ���� !! ID = %ld �������� = %s ���� = %s(��� %s)",
					GET_IDNUM(d->character), GET_NAME(d->character), d->host, k->host);
				mudlog(buf, BRF, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
				//send_to_gods(buf);
			}
			SEND_TO_Q("\r\n������� ������� ����� - �����������.\r\n", k);
			STATE(k) = CON_CLOSE;
			if (!target) {
				target = k->original;
				mode = UNSWITCH;
			}
			if (k->character)
				k->character->desc = NULL;
			k->character = NULL;
			k->original = NULL;
		} else if (k->character && (GET_IDNUM(k->character) == id)) {
			if (str_cmp(d->host, k->host)) {
				sprintf(buf,
					"��������� ���� !! ID = %ld Name = %s Host = %s(was %s)",
					GET_IDNUM(d->character), GET_NAME(d->character), d->host, k->host);
				mudlog(buf, BRF, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
				//send_to_gods(buf);
			}

			if (!target && STATE(k) == CON_PLAYING) {
				SEND_TO_Q("\r\n���� ���� ��� ���-�� ������ !\r\n", k);
				target = k->character;
				mode = USURP;
			}
			k->character->desc = NULL;
			k->character = NULL;
			k->original = NULL;
			SEND_TO_Q("\r\n������� ������� ����� - �����������.\r\n", k);
			STATE(k) = CON_CLOSE;
		}
	}

	/*
	 * now, go through the character list, deleting all characters that
	 * are not already marked for deletion from the above step (i.e., in the
	 * CON_HANGUP state), and have not already been selected as a target for
	 * switching into.  In addition, if we haven't already found a target,
	 * choose one if one is available (while still deleting the other
	 * duplicates, though theoretically none should be able to exist).
	 */

	for (ch = character_list; ch; ch = next_ch) {
		next_ch = ch->next;

		if (IS_NPC(ch))
			continue;
		if (GET_IDNUM(ch) != id)
			continue;

		/* ignore chars with descriptors (already handled by above step) */
		if (ch->desc)
			continue;

		/* don't extract the target char we've found one already */
		if (ch == target)
			continue;

		/* we don't already have a target and found a candidate for switching */
		if (!target) {
			target = ch;
			mode = RECON;
			continue;
		}

		/* we've found a duplicate - blow him away, dumping his eq in limbo. */
		if (ch->in_room != NOWHERE)
			char_from_room(ch);
		char_to_room(ch, STRANGE_ROOM);
		extract_char(ch, FALSE);
	}

	/* no target for swicthing into was found - allow login to continue */
	if (!target)
		return (0);

	/* Okay, we've found a target.  Connect d to target. */

	free_char(d->character);	/* get rid of the old char */
	d->character = target;
	d->character->desc = d;
	d->original = NULL;
	d->character->char_specials.timer = 0;
	REMOVE_BIT(PLR_FLAGS(d->character, PLR_MAILING), PLR_MAILING);
	REMOVE_BIT(PLR_FLAGS(d->character, PLR_WRITING), PLR_WRITING);
	STATE(d) = CON_PLAYING;

	switch (mode) {
	case RECON:
//    toggle_compression(d);
		SEND_TO_Q("���������������.\r\n", d);
		check_light(d->character, LIGHT_NO, LIGHT_NO, LIGHT_NO, LIGHT_NO, 1);
		act("$n �����������$g �����.", TRUE, d->character, 0, 0, TO_ROOM);
		sprintf(buf, "%s [%s] has reconnected.", GET_NAME(d->character), d->host);
		perslog("&g�����&n", GET_NAME(d->character));
		//printf("%s %s\n",ptime(),buf); // prool
		mudlog(buf, NRM, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
		login_change_invoice(d->character);
		break;
	case USURP:
//    toggle_compression(d);
		SEND_TO_Q("���� ���� ����� ��������� � ����!\r\n", d);
		act("���� $s �� ��������� ������� � ����� ���������",
			TRUE, d->character, 0, 0, TO_ROOM);
		sprintf(buf, "%s has re-logged.", GET_NAME(d->character));
		mudlog(buf, NRM, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)),
			SYSLOG, TRUE);

		break;
	case UNSWITCH:
//    toggle_compression(d);
		SEND_TO_Q("��������������� ��� ������������� ������.", d);
		sprintf(buf, "%s [%s] has reconnected.", GET_NAME(d->character), d->host);
		mudlog(buf, NRM, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
		break;
	}
#ifdef PROOL
	printf("Prool checkpoint 2\n");
#endif
	add_logon_record(d);
	return (1);
#ifdef PROOL
	printf("Prool checkpoint 2.1\n");
#endif
}

int pre_help(CHAR_DATA * ch, char *arg)
{
	char command[MAX_INPUT_LENGTH], topic[MAX_INPUT_LENGTH];

	half_chop(arg, command, topic);

	if (!*command || strlen(command) < 2 || !*topic || strlen(topic) < 2)
		return (0);
	if (isname(command, "������") || isname(command, "help") || isname(command, "�������")) {
		do_help(ch, topic, 0, 0);
		return (1);
	}
	return (0);
}

// ������ ������ ��� ���������� ��, ������ ��� ��� ��������� ������������, ���� ��������
// ����� ��������� � �� - ����� ��� ������, ��� �������� ���� �������, ����� ��� ���� ����� ��������
// � ������ ��� �� ����� �� ����������� ����� �������... ������ ����� ���, ��� �� ����� �)
int check_dupes_host(DESCRIPTOR_DATA * d, bool autocheck = 0)
{
	if (!d->character || IS_IMMORTAL(d->character))
		return 1;

	// � ������ ����������� ������ �������� ��� ��������� �� ����� � �������
	if (!autocheck) {
		if (RegisterSystem::is_registered(d->character))
			return 1;
		if (RegisterSystem::is_registered_email(GET_EMAIL(d->character))) {
			d->registered_email = 1;
			return 1;
		}
	}

	for (DESCRIPTOR_DATA* i = descriptor_list; i; i = i->next) {
		if (i != d
	    && i->ip == d->ip
		&& i->character
		&& !IS_IMMORTAL(i->character)
		&& (STATE(i) == CON_PLAYING || STATE(i) == CON_MENU)) {
			switch (2 /* CheckProxy(d) */ ) { // prool
			case 0:
				send_to_char(d->character,
					"&R�� ����� � ������� %s � ������ IP(%s) !\r\n"
					"��� ���������� ���������� � ����� ��� �����������.\r\n"
					"���� �� ������ �������� � ������� ��� �������������������� �������.&n\r\n",
					GET_PAD(i->character, 4), i->host);
				sprintf(buf,
					"! ���� � ������ IP ! ��������������������� ������.\r\n"
					"����� - %s, � ���� - %s, IP - %s.\r\n"
					"����� ������� � ������� �������������������� �������.",
					GET_NAME(d->character), GET_NAME(i->character), d->host);
				mudlog(buf, NRM, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
				return 0;
			case 1:
				if (autocheck) return 1;
				send_to_char("&R� ������ IP ������ ��������� ����������� ���������� ���������� �������.\r\n"
					"���������� � ����� ��� ���������� ������ ������� � ������ ������.&n", d->character);
				return 0;
			default:
				return 1;
			}
		}
	}
	return 1;
}

int check_dupes_email(DESCRIPTOR_DATA * d)
{
	CHAR_DATA *ch;

	if (!d->character || IS_IMMORTAL(d->character))
		return (1);

	for (ch = character_list; ch; ch = ch->next) {
		if (ch == d->character)
			continue;
		if (IS_NPC(ch))
			continue;
#ifndef VIRTUSTAN
		if (ch && !IS_IMMORTAL(ch) && (!str_cmp(GET_EMAIL(ch), GET_EMAIL(d->character)))) {
			sprintf(buf, "�� �� ������ ����� ������������ � %s!\r\n"
				"���������� email �����!\r\n", GET_PAD(ch, 4));
			send_to_char(buf, d->character);
			return (0);
		}
#endif
	}
	return (1);
}
void add_logon_record(DESCRIPTOR_DATA * d)
{
	//log_("Enter logon list");
	// ��������� ������ � LOG_LIST
	if (LOGON_LIST(d->character) == 0)
	{
		LOGON_LIST(d->character) = new (struct logon_data);
		LOGON_LIST(d->character)->ip = str_dup(d->host);
		LOGON_LIST(d->character)->count = 1;
		LOGON_LIST(d->character)->lasttime = time(0);
		LOGON_LIST(d->character)->next = 0;
	} else
	{
		// ���� ���� �� ������ � logon-�
		struct logon_data * cur_log = LOGON_LIST(d->character);
		struct logon_data * last_log = cur_log;
		bool ipfound = false;
		while (cur_log)
		{
			if (!strcmp(cur_log->ip,d->host))
			{
				// �������
				cur_log->count++;
				cur_log->lasttime = time(0);
				ipfound = true;
				break;
			}
			last_log = cur_log;
			cur_log = cur_log->next;
		};
		if (!ipfound)
		{
			last_log->next = new (struct logon_data);
			last_log = last_log->next;
			last_log->ip = str_dup(d->host);
			last_log->count = 1;
			last_log->lasttime = time(0);
			last_log->next = 0;
		}
		//
	}
	//log_("Exit logon list");
}

void do_entergame(DESCRIPTOR_DATA * d)
{
	int load_room, i, cmd, flag = 0;
	CHAR_DATA *ch;

	reset_char(d->character);
	read_aliases(d->character);

	if (GET_LEVEL(d->character) == LVL_IMMORT)
		GET_LEVEL(d->character) = LVL_GOD;
	if (GET_LEVEL(d->character) > LVL_IMPL)
		GET_LEVEL(d->character) = 1;
	if (GET_INVIS_LEV(d->character) > LVL_IMPL)
		GET_INVIS_LEV(d->character) = 0;
	if (GET_LEVEL(d->character) < LVL_IMPL) {
		if (PLR_FLAGGED(d->character, PLR_INVSTART))
			GET_INVIS_LEV(d->character) = LVL_IMMORT;
		if (GET_INVIS_LEV(d->character) > GET_LEVEL(d->character))
			GET_INVIS_LEV(d->character) = GET_LEVEL(d->character);
		if (GET_INVIS_LEV(d->character) > 0 && GET_LEVEL(d->character) < LVL_IMMORT)
			GET_INVIS_LEV(d->character) = 0;
	}
	if (GET_LEVEL(d->character) > LVL_IMMORT
	    && GET_LEVEL(d->character) < LVL_BUILDER
	    && (GET_GOLD(d->character) > 0 || GET_BANK_GOLD(d->character) > 0)) {
		GET_GOLD(d->character) = 0;
		GET_BANK_GOLD(d->character) = 0;
	}
	if (GET_LEVEL(d->character) >= LVL_IMMORT && GET_LEVEL(d->character) < LVL_IMPL
	|| GET_COMMSTATE(d->character)) {
		for (cmd = 0; *cmd_info[cmd].command != '\n'; cmd++) {
			if (!strcmp(cmd_info[cmd].command, "syslog"))
				if (Privilege::can_do_priv(d->character, std::string(cmd_info[cmd].command), cmd, 0)) {
					flag = 1;
					break;
				}
		}
		if (!flag)
			GET_LOGS(d->character)[0] = 0;
	}

	if (Privilege::check_flag(d->character, Privilege::KRODER) && GET_LEVEL(d->character) < LVL_IMMORT)
		GET_INVIS_LEV(d->character) = 35;

	/*
	 * We have to place the character in a room before equipping them
	 * or equip_char() will gripe about the person in NOWHERE.
	 */
	if (PLR_FLAGGED(d->character, PLR_HELLED))
		load_room = r_helled_start_room;
	else if (PLR_FLAGGED(d->character, PLR_NAMED))
		load_room = r_named_start_room;
	else if (PLR_FLAGGED(d->character, PLR_FROZEN))
		load_room = r_frozen_start_room;
	else if (!check_dupes_host(d))
		load_room = r_unreg_start_room;
	else {
		if ((load_room = GET_LOADROOM(d->character)) == NOWHERE)
			load_room = calc_loadroom(d->character);
		load_room = real_room(load_room);
	}

	if (!Clan::MayEnter(d->character, load_room, HCE_PORTAL))
		load_room = Clan::CloseRent(load_room);

	log("Player %s enter at room %d", GET_NAME(d->character), GET_ROOM_VNUM(load_room));
	/* If char was saved with NOWHERE, or real_room above failed... */
	if (load_room == NOWHERE) {
		if (GET_LEVEL(d->character) >= LVL_IMMORT)
			load_room = r_immort_start_room;
		else
			load_room = r_mortal_start_room;
	}
	// send_to_char(WELC_MESSG, d->character); // prool

	for (ch = character_list; ch; ch = ch->next)
		if (ch == d->character)
			break;

	if (!ch) {
		d->character->next = character_list;
		character_list = d->character;
	} else {
		REMOVE_BIT(MOB_FLAGS(ch, MOB_DELETE), MOB_DELETE);
		REMOVE_BIT(MOB_FLAGS(ch, MOB_FREE), MOB_FREE);
	}

	char_to_room(d->character, load_room);
	if (GET_LEVEL(d->character) != 0)
		Crash_load(d->character);

	/* ���������� ���� ��� ������� "���������" */
	for (i = 0; i < MAX_REMEMBER_TELLS; i++)
		GET_TELL(d->character, i)[0] = '\0';
	GET_LASTTELL(d->character) = 0;

	/*������ ����� ���� �� ����� �� */
	if (IS_SET(PRF_FLAGS(d->character, PRF_PUNCTUAL), PRF_PUNCTUAL)
	    && !get_skill(d->character, SKILL_PUNCTUAL))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_PUNCTUAL), PRF_PUNCTUAL);

	if (IS_SET(PRF_FLAGS(d->character, PRF_AWAKE), PRF_AWAKE)
	    && !get_skill(d->character, SKILL_AWAKE))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_AWAKE), PRF_AWAKE);

	if (IS_SET(PRF_FLAGS(d->character, PRF_POWERATTACK), PRF_POWERATTACK)
	    && !can_use_feat(d->character, POWER_ATTACK_FEAT))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_POWERATTACK), PRF_POWERATTACK);

	if (IS_SET(PRF_FLAGS(d->character, PRF_GREATPOWERATTACK), PRF_GREATPOWERATTACK)
	    && !can_use_feat(d->character, GREAT_POWER_ATTACK_FEAT))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_GREATPOWERATTACK), PRF_GREATPOWERATTACK);

	if (IS_SET(PRF_FLAGS(d->character, PRF_AIMINGATTACK), PRF_AIMINGATTACK)
	    && !can_use_feat(d->character, AIMING_ATTACK_FEAT))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_AIMINGATTACK), PRF_AIMINGATTACK);

	if (IS_SET(PRF_FLAGS(d->character, PRF_GREATAIMINGATTACK), PRF_GREATAIMINGATTACK)
	    && !can_use_feat(d->character, GREAT_AIMING_ATTACK_FEAT))
		REMOVE_BIT(PRF_FLAGS(d->character, PRF_GREATAIMINGATTACK), PRF_GREATAIMINGATTACK);

	// �������. ������ ����. ���������� ���� �� ������ �������.
	REMOVE_BIT(AFF_FLAGS(d->character, AFF_GROUP), AFF_GROUP);
	REMOVE_BIT(AFF_FLAGS(d->character, AFF_HORSE), AFF_HORSE);

	/* �������� ������� */
	check_portals(d->character);

	/* with the copyover patch, this next line goes in enter_player_game() */
	GET_ID(d->character) = GET_IDNUM(d->character);
	GET_ACTIVITY(d->character) = number(0, PLAYER_SAVE_ACTIVITY - 1);
	save_char(d->character, NOWHERE);
	player_table[get_ptable_by_unique(GET_UNIQUE(d->character))].last_logon = LAST_LOGON(d->character) = time(0);
#ifdef PROOL
	printf("Prool checkpoint 3\n");
#endif
	add_logon_record(d);
#ifdef PROOL
	printf("Prool checkpoint 3.1\n");
#endif
	act("$n �������$g � ����.", TRUE, d->character, 0, 0, TO_ROOM);
#ifdef PROOL
	printf("Prool checkpoint 3.2\n");
#endif
	/* with the copyover patch, this next line goes in enter_player_game() */
	read_saved_vars(d->character);
#ifdef PROOL
	printf("Prool checkpoint 3.3\n");
#endif
	greet_mtrigger(d->character, -1);
#ifdef PROOL
	printf("Prool checkpoint 3.4\n");
#endif
	greet_otrigger(d->character, -1);
#ifdef PROOL
	printf("Prool checkpoint 3.5\n");
#endif
	greet_memory_mtrigger(d->character);
#ifdef PROOL
	printf("Prool checkpoint 3.6\n");
#endif
	STATE(d) = CON_PLAYING;
	if (GET_LEVEL(d->character) == 0) {
#ifdef PROOL
	printf("Prool checkpoint 3.7\n");
#endif
		do_start(d->character, TRUE);
#ifdef PROOL
	printf("Prool checkpoint 3.8\n");
#endif
		GET_MANA_STORED(d->character) = 0;
#ifdef PROOL
	printf("Prool checkpoint 3.9\n");
#endif
		send_to_char(START_MESSG, d->character);
#ifdef PROOL
	printf("Prool checkpoint 3.10\n");
#endif
		send_to_char
		    ("�������������� �������� ������� ������� ��� ��������� ������� ���������� ������.\r\n", d->character);
#ifdef PROOL
	printf("Prool checkpoint 3.11\n");
#endif
	}
#ifdef PROOL
	printf("Prool checkpoint 3.A\n");
#endif
	//ip_log(d->host); // prool
	//printf(" %s �����\n", GET_NAME(d->character));
	sprintf(buf, "%s ����� � ����.", GET_NAME(d->character));
	perslog("&G�����&n", GET_NAME(d->character));
	if (strcmp(GET_NAME(d->character),"�����"))
		{
		//send_email("VMUD", "my-own-e-mail@gmail.com", "User logon", GET_NAME(d->character));
		}
	mudlog(buf, NRM, MAX(LVL_IMMORT, GET_INVIS_LEV(d->character)), SYSLOG, TRUE);
	look_at_room(d->character, 0);
	login_change_invoice(d->character);
	d->has_prompt = 0;
}

void DoAfterPassword(DESCRIPTOR_DATA * d)
{
	int load_result;

	/* Password was correct. */
	load_result = GET_BAD_PWS(d->character);
	GET_BAD_PWS(d->character) = 0;
	d->bad_pws = 0;

	if (ban->is_banned(d->host) == BanList::BAN_SELECT && !PLR_FLAGGED(d->character, PLR_SITEOK)) {
		SEND_TO_Q("��������, �� �� ������ ������� ����� ������ � ������� IP !\r\n", d);
		STATE(d) = CON_CLOSE;
		sprintf(buf, "Connection attempt for %s denied from %s", GET_NAME(d->character), d->host);
		mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
		return;
	}
	if (GET_LEVEL(d->character) < circle_restrict) {
		SEND_TO_Q("���� �������� ��������������.. ���� ��� ������� �����.\r\n", d);
		STATE(d) = CON_CLOSE;
		sprintf(buf, "Request for login denied for %s [%s] (wizlock)", GET_NAME(d->character), d->host);
		mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
		return;
	}

	/* check and make sure no other copies of this player are logged in */
	if (perform_dupe_check(d)) {
		Clan::SetClanData(d->character);
		return;
	}

	// ��� ��������� ��������� ��� ��� ���������� � ��� ��������� ������� � ������ �������, ��� ����������� �� �������
	Clan::SetClanData(d->character);

	log("%s [%s] has connected.", GET_NAME(d->character), d->host);

	if (load_result) {
		sprintf(buf, "\r\n\r\n\007\007\007"
			"%s%d LOGIN FAILURE%s SINCE LAST SUCCESSFUL LOGIN.%s\r\n",
			CCRED(d->character, C_SPR), load_result,
			(load_result > 1) ? "S" : "", CCNRM(d->character, C_SPR));
		SEND_TO_Q(buf, d);
		GET_BAD_PWS(d->character) = 0;
	}

	print_statistics(d, 0);

	SEND_TO_Q("\r\n&W������� ENTER&n", d);
	STATE(d) = CON_RMOTD;
}

void CreateChar(DESCRIPTOR_DATA * d)
{
	if (d->character != NULL)
		return;
	CREATE(d->character, CHAR_DATA, 1);
	memset(d->character, 0, sizeof(CHAR_DATA));
	clear_char(d->character);
	CREATE(d->character->player_specials, struct player_special_data, 1);
	memset(d->character->player_specials, 0, sizeof(struct player_special_data));
	d->character->desc = d;
}

/* deal with newcomers and other non-playing sockets */
void nanny(DESCRIPTOR_DATA * d, char *arg)
{
	char buf[MAX_STRING_LENGTH];
	int player_i, load_result;
	char tmp_name[MAX_INPUT_LENGTH], pwd_name[MAX_INPUT_LENGTH], pwd_pwd[MAX_INPUT_LENGTH];

#ifdef UNDEFINED
printf("nanny() arg='%s'\n",arg);
#endif

	skip_spaces(&arg);

	switch (STATE(d)) {
		/*. OLC states . */
	case CON_OEDIT:
		oedit_parse(d, arg);
		break;
	case CON_REDIT:
		redit_parse(d, arg);
		break;
	case CON_ZEDIT:
		zedit_parse(d, arg);
		break;
	case CON_MEDIT:
		medit_parse(d, arg);
		break;
	case CON_SEDIT:
		sedit_parse(d, arg);
		break;
	case CON_TRIGEDIT:
		trigedit_parse(d, arg);
		break;
	case CON_MREDIT:
		mredit_parse(d, arg);
		break;
	case CON_CLANEDIT:
		d->clan_olc->clan->Manage(d, arg);
		break;
		/*. End of OLC states . */


	case CON_GET_KEYTABLE:
		if (strlen(arg) > 0)
			arg[0] = arg[strlen(arg) - 1];
		if (!*arg || *arg < '0' || *arg >= '0' + KT_LAST) {
			SEND_TO_Q("\r\nUnknown key table. Retry, please : ", d);
			if (arg)
				if (arg[0])
					printf("keytable=`%s'\n", arg); // prool
			return;
		};
		d->keytable = (ubyte) * arg - (ubyte) '0';
		// ip_log(d->host); // prool
		SEND_TO_Q(GREETINGS, d);
		STATE(d) = CON_GET_NAME;
		break;
	case CON_GET_NAME:	/* wait for input of name */
		if (d->character == NULL)
			CreateChar(d);
		if (!*arg)
			STATE(d) = CON_CLOSE;
		else if ((!str_cmp("�����", arg))||(!str_cmp("new", arg))) { // add "new" by prool ;)
			SEND_TO_Q(create_name_rules, d);
			SEND_TO_Q("������� ���. Enter your name: ", d);
			STATE(d) = CON_NEW_CHAR;
			return;
		}
		else {
			if (sscanf(arg, "%s %s", pwd_name, pwd_pwd) == 2) {
				if (_parse_name(pwd_name, tmp_name) ||
				    (player_i = load_char(tmp_name, d->character)) < 0) {
					SEND_TO_Q("������������ ���! ���������, ����������.\r\n" "��� : ", d);
					return;
				}
				if (PLR_FLAGGED(d->character, PLR_DELETED) || !Password::compare_password(d->character, pwd_pwd)) {
					SEND_TO_Q("������������ ���!! ���������, ����������.\r\n" "��� : ", d);
					if (!PLR_FLAGGED(d->character, PLR_DELETED)) {
						sprintf(buf, "Bad PW: %s [%s %s]", GET_NAME(d->character), d->host,
							nslookup(d->host));
						mudlog(buf, BRF, LVL_IMMORT, SYSLOG, TRUE);
					}
					free_char(d->character);
					d->character = NULL;
					return;
				}
				REMOVE_BIT(PLR_FLAGS(d->character, PLR_MAILING), PLR_MAILING);
				REMOVE_BIT(PLR_FLAGS(d->character, PLR_WRITING), PLR_WRITING);
				REMOVE_BIT(PLR_FLAGS(d->character, PLR_CRYO), PLR_CRYO);
				GET_PFILEPOS(d->character) = player_i;
				GET_ID(d->character) = GET_IDNUM(d->character);
				DoAfterPassword(d);
				return;
			} else
			    if (_parse_name(arg, tmp_name) ||
				strlen(tmp_name) < MIN_NAME_LENGTH ||
				strlen(tmp_name) > MAX_NAME_LENGTH ||
				!Is_Valid_Name(tmp_name) || fill_word(strcpy(buf, tmp_name)) || reserved_word(buf)) {
				SEND_TO_Q("������������ ���!!! ���������, ����������!\r\n" "��� : ", d);
				return;
			} else if (!Is_Valid_Dc(tmp_name)) {
				SEND_TO_Q("����� � �������� ������ ��������� � ����.\r\n", d);
				SEND_TO_Q("�� ��������� ������������� ������� ���� ��� ������.\r\n", d);
				SEND_TO_Q("��� � ������ ����� ������ : ", d);
				return;
			}
			if ((player_i = load_char(tmp_name, d->character)) > -1) {
				GET_PFILEPOS(d->character) = player_i;
				if (PLR_FLAGGED(d->character, PLR_DELETED)) {	/* We get a false positive from the original deleted character. */
					free_char(d->character);
					d->character = 0;
					/* Check for multiple creations... */
					if (!Valid_Name(tmp_name)) {
						SEND_TO_Q("������������ ���!!!! ���������, ����������.\r\n" "��� : ", d);
						return;
					}
					CreateChar(d);
					CREATE(d->character->player.name, char, strlen(tmp_name) + 1);
					strcpy(d->character->player.name, CAP(tmp_name));
					CREATE(GET_PAD(d->character, 0), char, strlen(tmp_name) + 1);
					strcpy(GET_PAD(d->character, 0), CAP(tmp_name));
					GET_PFILEPOS(d->character) = player_i;
					sprintf(buf, "�� ������������� ������� ��� %s (are you sure) [ Y(�) / N(�) ] ? ", tmp_name);
					SEND_TO_Q(buf, d);
					STATE(d) = CON_NAME_CNFRM;
				} else {	/* undo it just in case they are set */
					REMOVE_BIT(PLR_FLAGS(d->character, PLR_MAILING), PLR_MAILING);
					REMOVE_BIT(PLR_FLAGS(d->character, PLR_WRITING), PLR_WRITING);
					REMOVE_BIT(PLR_FLAGS(d->character, PLR_CRYO), PLR_CRYO);
					SEND_TO_Q("�������� � ����� ������ ��� ����������. ������� ������. (Character exists. Enter passwd) : ", d);
					d->idle_tics = 0;
					STATE(d) = CON_PASSWORD;
				}
			} else {	/* player unknown -- make new character */


				/* Check for multiple creations of a character. */
				if (!Valid_Name(tmp_name)) {
					SEND_TO_Q("������������ ���!!!!! ���������, ����������.\r\n" "��� : ", d);
					return;
				}
#ifdef VIRTUSTAN
				if (0 /*cmp_ptable_by_name(tmp_name, MIN_NAME_LENGTH + 1) >= 0*/) { // prool
					SEND_TO_Q
					    ("������ ������� ������ ����� ���� ��������� � ��� ������������ ����������.\r\n"
					     "��� ���������� ������ ������������� ��� ���������� ������� ������ ���.\r\n"
					     "���  : ", d);
					return;
				}
#endif

				CREATE(d->character->player.name, char, strlen(tmp_name) + 1);
				strcpy(d->character->player.name, CAP(tmp_name));
				CREATE(GET_PAD(d->character, 0), char, strlen(tmp_name) + 1);
				strcpy(GET_PAD(d->character, 0), CAP(tmp_name));
				SEND_TO_Q(create_name_rules, d);
				sprintf(buf, "�� ������������� ������� ���  %s (are you sure) [ Y(�) / N(�) ] ? ", tmp_name);
				SEND_TO_Q(buf, d);
				STATE(d) = CON_NAME_CNFRM;
			}
		}
		break;
	case CON_NAME_CNFRM:	/* wait for conf. of new name    */
		if (UPPER(*arg) == 'Y' || UPPER(*arg) == '�') {
			if (ban->is_banned(d->host) >= BanList::BAN_NEW) {
				sprintf(buf,
					"������� �������� ��������� %s ��������� ��� [%s] (siteban)",
					GET_PC_NAME(d->character), d->host);
				mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
				SEND_TO_Q("��������, �������� ������ ��������� ��� ������ IP !!! ��������� !!!\r\n", d);
				STATE(d) = CON_CLOSE;
				return;
			}
			if (circle_restrict) {
				SEND_TO_Q("��������, �� �� ������ ������� ����� �������� � ��������� ������.\r\n", d);
				sprintf(buf,
					"������� �������� ������ ��������� %s ��������� ��� [%s] (wizlock)",
					GET_PC_NAME(d->character), d->host);
				mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
				STATE(d) = CON_CLOSE;
				return;
			}
			// Name auto-agreement by Alez see names.cpp //
			switch (process_auto_agreement(d)) {
			case 0:	// Auto - agree
				sprintf(buf,
					"������� ������ ��� %s. Enter passwd : ",
					GET_PAD(d->character, 1));
				SEND_TO_Q(buf, d);
				STATE(d) = CON_NEWPASSWD;
				return;
			case 1:	// Auto -disagree
				STATE(d) = CON_CLOSE;
				return;
			default:
				break;
			};
			SEND_TO_Q("��� ��� (your gender) [ �(M)/�(F) ] ? ", d);
			STATE(d) = CON_QSEX;
			return;

		} else if (UPPER(*arg) == 'N' || UPPER(*arg) == '�') {
			SEND_TO_Q("����, ���� �������� ? ������, ������� ��� :)\r\n" "��� : ", d);
			free(d->character->player.name);
			d->character->player.name = NULL;
			STATE(d) = CON_GET_NAME;
		} else {
			SEND_TO_Q("�������� Yes(��) or No(���) : ", d);
		}
		break;
	case CON_NEW_CHAR:
		if (!*arg) {
			STATE(d) = CON_CLOSE;
			return;
		}
		if (d->character == NULL)
			CreateChar(d);
		if (_parse_name(arg, tmp_name) ||
			strlen(tmp_name) < MIN_NAME_LENGTH ||
			strlen(tmp_name) > MAX_NAME_LENGTH ||
			!Is_Valid_Name(tmp_name) || fill_word(strcpy(buf, tmp_name)) || reserved_word(buf)) {
			SEND_TO_Q("������������ ���!!!!!! ���������, ����������.\r\n" "��� : ", d);
			return;
		}
		player_i = load_char(tmp_name, d->character);
		if (player_i > -1) {
			if (PLR_FLAGGED(d->character, PLR_DELETED)) {
				free_char(d->character);
				d->character = 0;
				CreateChar(d);
			} else {
				SEND_TO_Q("����� �������� ��� ����������. �������� ������ ��� : ", d);
				free_char(d->character);
				d->character = 0;
				return;
			}
		}
		if (!Valid_Name(tmp_name)) {
			SEND_TO_Q("������������ ���!!!!!!! ���������, ����������.\r\n" "��� : ", d);
			return;
		}
#ifdef VIRTUSTAN
		if (cmp_ptable_by_name(tmp_name, MIN_NAME_LENGTH + 1) >= 0) {
			SEND_TO_Q("������ ������� ������ ����� ��������� ���� � ��� ������������ ����������.\r\n"
				"��� ���������� ������ ������������� ��� ���������� ������� ������ ���.\r\n"
				"���  : ", d);
			return;
		}
#endif
		CREATE(d->character->player.name, char, strlen(tmp_name) + 1);
		strcpy(d->character->player.name, CAP(tmp_name));
		CREATE(GET_PAD(d->character, 0), char, strlen(tmp_name) + 1);
		strcpy(GET_PAD(d->character, 0), CAP(tmp_name));
		if (ban->is_banned(d->host) >= BanList::BAN_NEW) {
			sprintf(buf, "������� �������� ��������� %s ��������� ��� [%s] (siteban)",
				GET_PC_NAME(d->character), d->host);
			mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
			SEND_TO_Q("��������, �������� ������ ��������� ��� ������ IP !!! ��������� !!!\r\n", d);
			STATE(d) = CON_CLOSE;
			return;
		}
		if (circle_restrict) {
			SEND_TO_Q("��������, �� �� ������ ������� ����� �������� � ��������� ������.\r\n", d);
			sprintf(buf,
				"������� �������� ������ ��������� %s ��������� ��� [%s] (wizlock)",
				GET_PC_NAME(d->character), d->host);
			mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
			STATE(d) = CON_CLOSE;
			return;
		}
		switch (process_auto_agreement(d)) {
		case 0:	// Auto - agree
			sprintf(buf,
				"������� ������ ��� %s (�� ������� ������ ���� '123' ��� 'qwe', ����� ����� ���������� ����� �������). Enter your password : ",
				GET_PAD(d->character, 1));
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NEWPASSWD;
			return;
		case 1:	// Auto -disagree
			free_char(d->character);
			d->character = NULL;
			SEND_TO_Q("�������� ������ ��� : ", d);
			return;
		default:
			break;
		};
		SEND_TO_Q("��� ��� [ �(M)/�(F) ] ? ", d);
		STATE(d) = CON_QSEX;
		return;
	case CON_PASSWORD:	/* get pwd for known player      */
		/*
		 * To really prevent duping correctly, the player's record should
		 * be reloaded from disk at this point (after the password has been
		 * typed).  However I'm afraid that trying to load a character over
		 * an already loaded character is going to cause some problem down the
		 * road that I can't see at the moment.  So to compensate, I'm going to
		 * (1) add a 15 or 20-second time limit for entering a password, and (2)
		 * re-add the code to cut off duplicates when a player quits.  JE 6 Feb 96
		 */

		SEND_TO_Q("\r\n", d);

		if (!*arg)
			STATE(d) = CON_CLOSE;
		else {
			if (!Password::compare_password(d->character, arg)) {
				sprintf(buf, "Bad PW: %s [%s %s]", GET_NAME(d->character), d->host,
					nslookup(d->host));
				mudlog(buf, BRF, LVL_IMMORT, SYSLOG, TRUE);
				GET_BAD_PWS(d->character)++;
				save_char(d->character, NOWHERE);
				if (++(d->bad_pws) >= max_bad_pws) {	/* 3 strikes and you're out. */
					SEND_TO_Q("�������� ������... �������������. (Invalid passwd, disconnect)\r\n", d);
					STATE(d) = CON_CLOSE;
				} else {
					SEND_TO_Q("�������� ������ (invalid passwd).\r\n������ (enter passwd) : ", d);
				}
				return;
			}
			DoAfterPassword(d);
		}
		break;

	case CON_NEWPASSWD:
	case CON_CHPWD_GETNEW:
		if (!Password::check_password(d->character, arg)) {
			sprintf(buf, "\r\n%s\r\n", Password::BAD_PASSWORD);
			SEND_TO_Q(buf, d);
			SEND_TO_Q("������ (passwd) : ", d);
			return;
		}
		Password::set_password(d->character, arg);

		SEND_TO_Q("\r\n��������� ������, ���������� (enter passwd confirmation): ", d);
		if (STATE(d) == CON_NEWPASSWD)
			STATE(d) = CON_CNFPASSWD;
		else
			STATE(d) = CON_CHPWD_VRFY;

		break;

	case CON_CNFPASSWD:
	case CON_CHPWD_VRFY:
		if (!Password::compare_password(d->character, arg)) {
			SEND_TO_Q("\r\n������ �� �������������... ��������.\r\n", d);
			SEND_TO_Q("������ (passwd): ", d);
			if (STATE(d) == CON_CNFPASSWD)
				STATE(d) = CON_NEWPASSWD;
			else
				STATE(d) = CON_CHPWD_GETNEW;
			return;
		}

		if (STATE(d) == CON_CNFPASSWD) { 
			/* // prool
			SEND_TO_Q (kin_menu, d);
			SEND_TO_Q
			("\r\n���� ����� (��� ����� ������ ���������� �� ������ �������"
			" \r\n������� <������������ �����>): ", d);
			*/
			GET_KIN (d->character) = KIN_RUSICHI; // prool
			SEND_TO_Q (class_menu, d);
			SEND_TO_Q ("\r\n���� ��������� (��� ����� ������ ���������� �� ������ �������"
				   " \r\n������� <������������ ���������>): ", d);
			STATE (d) = CON_QCLASS;
		} else {
			save_char(d->character, NOWHERE);
			SEND_TO_Q("\r\n������.\r\n", d);
			SEND_TO_Q(MENU, d);
			STATE(d) = CON_MENU;
		}

		break;

	case CON_QSEX:		/* query sex of new user         */
		if (pre_help(d->character, arg)) {
			SEND_TO_Q("\r\n��� ��� [ �(M)/�(F) ] ? ", d);
			STATE(d) = CON_QSEX;
			return;
		}
		switch (UPPER(*arg)) {
		case '�':
		case 'M':
			GET_SEX(d->character) = SEX_MALE;
			break;
		case '�':
		case 'F':
			GET_SEX(d->character) = SEX_FEMALE;
			break;
		default:
			SEND_TO_Q("��� ����� ���� � ���, �� ���� �� ��� :)\r\n" "� ����� � ��� ��� ? ", d);
			return;
		}
		SEND_TO_Q("��������� ������������ ��������� �����. � ������ ������ ������� ���� �������.\r\n(For Englishmans: press Enter 5 times)\r\n", d);
		GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 1, tmp_name);
		sprintf(buf, "��� � ����������� ������ (��� ����?) [%s]: ", tmp_name);
		SEND_TO_Q(buf, d);
		STATE(d) = CON_NAME2;
		return;

	case CON_QKIN:		/* query rass      */
		/* // prool
		if (pre_help (d->character, arg)){
			SEND_TO_Q (kin_menu, d);
			SEND_TO_Q ("\r\n�����: ", d);
			STATE (d) = CON_QKIN;
			return;
		}
		load_result = parse_kin (*arg);
		if (load_result == KIN_UNDEFINED){
			SEND_TO_Q ("��� ������ ��� ������ ZOG\r\n"
			"����� ����� ��� ����� �� ���� ? ", d);
			return;
		}
		*/
		GET_KIN (d->character) = KIN_RUSICHI /*load_result*/; // prool
		if ( GET_KIN(d->character) == KIN_RUSICHI ){
			SEND_TO_Q (class_menu, d);
			SEND_TO_Q ("\r\n���� ��������� (��� ����� ������ ���������� �� ������ �������"
				   " \r\n������� <������������ ���������>): ", d);
			STATE (d) = CON_QCLASS;
		}
		if ( GET_KIN(d->character) == KIN_STEPNYAKI ){
			SEND_TO_Q (class_menu_step, d);
			SEND_TO_Q ("\r\n���� ��������� (��� ����� ������ ���������� �� ������ �������"
				   " \r\n������� <������������ ���������>): ", d);
			STATE (d) = CON_QCLASSS;
		}
		if ( GET_KIN(d->character) == KIN_VIKINGI ){
			SEND_TO_Q (class_menu_vik, d);
			SEND_TO_Q ("\r\n���� ��������� (��� ����� ������ ���������� �� ������ �������"
				   " \r\n������� <������������ ���������>): ", d);
			STATE (d) = CON_QCLASSV;
		}
		break;

	case CON_RELIGION:	/* query religion of new user      */
		if (pre_help(d->character, arg)) {
			SEND_TO_Q(religion_menu, d);
			SEND_TO_Q("\n\r������� :", d);
			STATE(d) = CON_RELIGION;
			return;
		}

		switch (UPPER(*arg)) {
		case '�':
		case '�':
		case 'Z':
		case 'P':
#if 0 // ����� �������� �� ������������� ��������� � �������. prool
			if (class_religion[(int) GET_CLASS(d->character)] == RELIGION_MONO) { 
				SEND_TO_Q
				    ("�������� ��������� ���� ��������� �� ������ ���� ���������!\r\n"
				     "��� ����� ����� �� ������ ������� ? ", d);
				return;
			}
#endif
			GET_RELIGION(d->character) = RELIGION_POLY;
			break;
		case '�':
		case 'C':
#if 0 // ����� �������� �� ������������� ��������� � �������. prool
			if (class_religion[(int) GET_CLASS(d->character)] == RELIGION_POLY) {
				SEND_TO_Q
				    ("��������� ��������� ���� ��������� �������� ������������!\r\n"
				     "��� ����� ����� �� ������ ������� ? ", d);
				return;
			}
#endif
			GET_RELIGION(d->character) = RELIGION_MONO;
			break;
		default:
			SEND_TO_Q("�����-����� ����� �� ������ ������� ? ", d);
			return;
		}
		if( GET_KIN (d->character)==KIN_RUSICHI){
			SEND_TO_Q (race_menu, d);
			SEND_TO_Q ("\r\n�� ���� �� ������ : ", d);
			STATE (d) = CON_RACER;
		}
		if( GET_KIN (d->character)==KIN_STEPNYAKI){
			SEND_TO_Q (race_menu_step, d);
			SEND_TO_Q ("\r\n�� ���� �� ������ : ", d);
			STATE (d) = CON_RACES;
		}
		if( GET_KIN (d->character)==KIN_VIKINGI){
			SEND_TO_Q (race_menu_vik, d);
			SEND_TO_Q ("\r\n�� ���� �� ������ : ", d);
			STATE (d) = CON_RACEV;
		}
		break;

	case CON_QCLASS:
		if (pre_help(d->character, arg)) {
			SEND_TO_Q(class_menu, d);
			SEND_TO_Q("\r\n���� ��������� : ", d);
			STATE(d) = CON_QCLASS;
			return;
		}
		load_result = parse_class(*arg);
		if (load_result == CLASS_UNDEFINED) {
			SEND_TO_Q("\r\n��� �� ���������.\r\n��������� : ", d);
			return;
		} else
			GET_CLASS(d->character) = load_result;
		/* // prool
		SEND_TO_Q(religion_menu, d);
		SEND_TO_Q("\n\r������� :", d);
		*/
			GET_RELIGION(d->character) = RELIGION_MONO;
			SEND_TO_Q (race_menu, d);
			SEND_TO_Q ("\r\n�� ���� �� ������ : ", d);
		STATE(d) = CON_RACER;
		break;

	case CON_QCLASSS:
		if (pre_help (d->character, arg)){
			SEND_TO_Q (class_menu_step, d);
			SEND_TO_Q ("\r\n���� ��������� : ", d);
			STATE (d) = CON_QCLASSS;
			return;
		}
		load_result = parse_class_step (*arg);
		if (load_result == CLASS_UNDEFINED){
			SEND_TO_Q ("\r\n��� �� ���������.\r\n��������� : ", d);
			return;
		}
		else
			GET_CLASS (d->character) = load_result;
		SEND_TO_Q (religion_menu, d);
		SEND_TO_Q ("\n\r������� :", d);
		STATE (d) = CON_RELIGION;
		break;

	case CON_QCLASSV:
		if (pre_help (d->character, arg)){
			SEND_TO_Q (class_menu_vik, d);
			SEND_TO_Q ("\r\n���� ��������� : ", d);
			STATE (d) = CON_QCLASSV;
			return;
		}
		load_result = parse_class_vik (*arg);
		if (load_result == CLASS_UNDEFINED){
			SEND_TO_Q ("\r\n��� �� ���������.\r\n��������� : ", d);
			return;
		}
		else
			GET_CLASS (d->character) = load_result;
		SEND_TO_Q (religion_menu, d);
		SEND_TO_Q ("\n\r������� :", d);
		STATE (d) = CON_RELIGION;
		break;

	case CON_RACER:		/* query race      */
		if (pre_help(d->character, arg)) {
			SEND_TO_Q(race_menu, d);
			SEND_TO_Q("\r\n��� : ", d);
			STATE(d) = CON_RACER;
			return;
		}
		load_result = parse_race(*arg);
		if (load_result == RACE_UNDEFINED) {
			#define STR_ROD "��� ����� ��� �� ���������? "
			SEND_TO_Q(STR_ROD, d);
			return;
		}
		GET_RACE(d->character) = load_result;
		roll_real_abils(d->character, 1);
		SEND_TO_Q(genchar_help, d);
		SEND_TO_Q("\r\n\r\n������� ����� �������.\r\n", d);
		STATE(d) = CON_ROLL_STATS;
		break;

	case CON_RACES:		/* query race      */
		if (pre_help (d->character, arg)){
			SEND_TO_Q (race_menu_step, d);
			SEND_TO_Q ("\r\n��� : ", d);
			STATE (d) = CON_RACES;
			return;
		}
		load_result = parse_race_step (*arg);
		if (load_result == RACE_UNDEFINED){
			SEND_TO_Q (STR_ROD, d);
			 return;
		}
		GET_RACE (d->character) = load_result;
		roll_real_abils (d->character, 1);
		SEND_TO_Q (genchar_help, d);
		SEND_TO_Q ("\r\n\r\n������� ����� �������.\r\n", d);
		STATE (d) = CON_ROLL_STATS;
		break;

	case CON_RACEV:		/* query race      */
		if (pre_help (d->character, arg)){
			SEND_TO_Q (race_menu_vik, d);
			SEND_TO_Q ("\r\n��� : ", d);
			STATE (d) = CON_RACEV;
			return;
		}
		load_result = parse_race_vik (*arg);
		if (load_result == RACE_UNDEFINED){
			SEND_TO_Q (STR_ROD, d);
			return;
		}
		GET_RACE (d->character) = load_result;
		roll_real_abils (d->character, 1);
		SEND_TO_Q (genchar_help, d);
		SEND_TO_Q ("\r\n\r\n������� ����� �������.\r\n", d);
		STATE (d) = CON_ROLL_STATS;
		break;

	case CON_ROLL_STATS:
		switch (genchar_parse(d->character, arg)) {
		case GENCHAR_CONTINUE:
			genchar_disp_menu(d->character);
			break;
		default:
			/* ���. ��������� ��������� */
			SEND_TO_Q (color_menu, d);
			SEND_TO_Q ("\r\n����� :",d);
			STATE (d) = CON_COLOR;
		}
		break;

	case CON_COLOR:
		if (pre_help (d->character, arg)){
			SEND_TO_Q (color_menu, d);
			SEND_TO_Q ("\n\r����� :", d);
			STATE (d) = CON_COLOR;
			return;
		}
		switch (UPPER (*arg)){
			case '0':
				do_color (d->character, "off", 0, 0);
				break;
			case '1':
				do_color (d->character, "simple", 0, 0);
				break;
			case '2':
				do_color (d->character, "normal", 0, 0);
				break;
			case '3':
				do_color (d->character, "full", 0, 0);
				break;
			default:
				SEND_TO_Q ("����� ������� ��� �������� �� ��������������!", d);
			return;
			}
		SEND_TO_Q ("\r\n������� ��� E-mail. Enter your E-mail"
			   "\r\n(��� ���� ��������� ������ ����� ���������� E-mail): ",d);
		STATE (d) = CON_GET_EMAIL;
		break;


	case CON_GET_EMAIL:
		if (!*arg) {
			SEND_TO_Q("\r\n��� E-mail : ", d);
			return;
		} else if (!valid_email(arg)) {
			SEND_TO_Q("\r\n������������ E-mail !" "\r\n��� E-mail :  ", d);
			return;
		}

		if (GET_PFILEPOS(d->character) < 0)
			GET_PFILEPOS(d->character) = create_entry(GET_PC_NAME(d->character));

		/* Now GET_NAME() will work properly. */
		init_char(d->character);
		strncpy(GET_EMAIL(d->character), arg, 127);
		*(GET_EMAIL(d->character) + 127) = '\0';
		lower_convert(GET_EMAIL(d->character));
		save_char(d->character, NOWHERE);

		// ��������� � ������ ������ ���������
		if (!(int)NAME_FINE(d->character)) {
			sprintf(buf, "%s - ����� �����. ������: %s/%s/%s/%s/%s/%s Email: %s ���: %s. ]\r\n"
				"[ %s ���� ��������� �����.",
				GET_NAME(d->character),	GET_PAD(d->character, 0),
				GET_PAD(d->character, 1), GET_PAD(d->character, 2),
				GET_PAD(d->character, 3), GET_PAD(d->character, 4),
				GET_PAD(d->character, 5), GET_EMAIL(d->character),
				genders[(int)GET_SEX(d->character)], GET_NAME(d->character));
				NewNameAdd(d->character);
		}

		print_statistics(d, 0);

		SEND_TO_Q(motd, d);
		SEND_TO_Q("\r\n������� Enter", d);
		STATE(d) = CON_RMOTD;

		//sprintf(buf, "%s [%s] new player.", GET_NAME(d->character), d->host);
		//mudlog(buf, NRM, LVL_IMMORT, SYSLOG, TRUE);
		break;

	case CON_RMOTD:	/* read CR after printing motd   */
		if (!check_dupes_email(d)) {
			STATE(d) = CON_CLOSE;
			break;
		}
		do_entergame(d);
		// SEND_TO_Q(MENU, d);
		// STATE(d) = CON_MENU;
		break;

	case CON_MENU:		/* get selection from main menu  */
		switch (*arg) {
		case '0':

			print_statistics(d, 3); // ��� ��� prool
			SEND_TO_Q("\r\n�� �������!\r\n\r\n", d);
#if 1 // �������� ������ � ���������� ���������
			if (GET_REMORT(d->character) == 0 && GET_LEVEL(d->character) <= 25
			    && !IS_SET(PLR_FLAGS(d->character, PLR_NODELETE), PLR_NODELETE)) {
				int timeout = -1;
				for (int ci = 0; GET_LEVEL(d->character) > pclean_criteria[ci].level; ci++) {
					//if (GET_LEVEL(d->character) == pclean_criteria[ci].level)
					timeout = pclean_criteria[ci + 1].days;
				}
				if (timeout > 0) {
					time_t deltime = time(NULL) + timeout * 60 * 60 * 24;
					sprintf(buf, "� ������ ������ ���������� �������� ����� ��������� �� %s ����� ��� :).\r\n",
							rustime(localtime(&deltime)));
					SEND_TO_Q(buf, d);
				}
			};
#endif

			STATE(d) = CON_CLOSE;
			break;

		case '1':
			if (!check_dupes_email(d)) {
				STATE(d) = CON_CLOSE;
				break;
			}
			do_entergame(d);
			break;

		case '2':
			if (d->character->player.description) {
				SEND_TO_Q("���� ������� ��������:\r\n", d);
				SEND_TO_Q(d->character->player.description, d);
				/*
				 * Don't free this now... so that the old description gets loaded
				 * as the current buffer in the editor.  Do setup the ABORT buffer
				 * here, however.
				 *
				 * free(d->character->player.description);
				 * d->character->player.description = NULL;
				 */
				d->backstr = str_dup(d->character->player.description);
			}
			SEND_TO_Q
			    ("������� �������� ������ �����, ������� ����� ���������� �� ������� <���������>.\r\n", d);
			SEND_TO_Q("(/s ��������� /h ������)\r\n", d);
			d->str = &d->character->player.description;
			d->max_str = EXDSCR_LENGTH;
			STATE(d) = CON_EXDESC;
			break;

		case '3':
			page_string(d, background, 0);
			STATE(d) = CON_RMOTD;
			break;

		case '4':
			SEND_TO_Q("\r\n������� ������ ������ (enter OLD passwd): ", d);
			STATE(d) = CON_CHPWD_GETOLD;
			break;

		case '5':
			if (IS_IMMORTAL(d->character)) {
				SEND_TO_Q("\r\n���� ���������� (�) �������, ������� ���� ��������� :)))\r\n", d);
				SEND_TO_Q(MENU, d);
				break;
			}
			if (IS_SET(PLR_FLAGS(d->character, PLR_NODELETE), PLR_NODELETE)) {
				SEND_TO_Q("\r\n���� ��������� ��� ������\r\n", d);
				SEND_TO_Q(MENU, d);
				break;
			}
			SEND_TO_Q("\r\n��� ������������� ������� ���� ������ (enter passwd for confirm): ", d);
			STATE(d) = CON_DELCNF1;
			break;

		default:
			SEND_TO_Q("\r\n��� �� ���� ���������� ����� !\r\n", d);
			SEND_TO_Q(MENU, d);
			break;
		}

		break;

	case CON_CHPWD_GETOLD:
		if (!Password::compare_password(d->character, arg)) {
			SEND_TO_Q("\r\n�������� ������. Invalid passwd\r\n", d);
			SEND_TO_Q(MENU, d);
			STATE(d) = CON_MENU;
		} else {
			SEND_TO_Q("\r\n������� ����� ������. (Enter new passwd) : ", d);
			STATE(d) = CON_CHPWD_GETNEW;
		}
		return;

	case CON_DELCNF1:
		if (!Password::compare_password(d->character, arg)) {
			SEND_TO_Q("\r\n�������� ������. Invalid passwd\r\n", d);
			SEND_TO_Q(MENU, d);
			STATE(d) = CON_MENU;
		} else {
			SEND_TO_Q("\r\n!!! ��� �������� ����� ������ !!!\r\n"
				  "�� ��������� � ���� ������� ?\r\n\r\n"
				  "�������� \"YES / ��\" ��� ������������� : ", d);
			STATE(d) = CON_DELCNF2;
		}
		break;

	case CON_DELCNF2:
		if (!strcmp(arg, "yes") || !strcmp(arg, "YES") || !strcmp(arg, "��") || !strcmp(arg, "��")) {
			if (PLR_FLAGGED(d->character, PLR_FROZEN)) {
				SEND_TO_Q("�� �������� �� ������, �� ���� ���������� ���.\r\n", d);
				SEND_TO_Q("�������� �� ������.\r\n", d);
				STATE(d) = CON_CLOSE;
				return;
			}
			if (GET_LEVEL(d->character) >= LVL_GRGOD)
				return;
			delete_char(GET_NAME(d->character));
			sprintf(buf, "�������� '%s' ������ !\r\n" "�� ��������.\r\n", GET_NAME(d->character));
			SEND_TO_Q(buf, d);
			sprintf(buf, "%s (lev %d) has self-deleted.", GET_NAME(d->character), GET_LEVEL(d->character));
			mudlog(buf, NRM, LVL_GOD, SYSLOG, TRUE);
			STATE(d) = CON_CLOSE;
			return;
		} else {
			SEND_TO_Q("\r\n�������� �� ������.\r\n", d);
			SEND_TO_Q(MENU, d);
			STATE(d) = CON_MENU;
		}
		break;
	case CON_NAME2:
		skip_spaces(&arg);
		if (strlen(arg) == 0)
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 1, arg);
		if (!_parse_name(arg, tmp_name) &&
		    strlen(tmp_name) >= MIN_NAME_LENGTH && strlen(tmp_name) <= MAX_NAME_LENGTH &&
		    !strn_cmp(tmp_name, GET_PC_NAME(d->character), MIN(MIN_NAME_LENGTH, strlen(GET_PC_NAME(d->character)) - 1))
		    ) {
			CREATE(GET_PAD(d->character, 1), char, strlen(tmp_name) + 1);
			strcpy(GET_PAD(d->character, 1), CAP(tmp_name));
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 2, tmp_name);
			sprintf(buf, "��� � ��������� ������ (��������� ����?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NAME3;
		} else {
			SEND_TO_Q("�����������!\r\n", d);
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 1, tmp_name);
			sprintf(buf, "��� � ����������� ������ (��� ����?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
		};
		break;
	case CON_NAME3:
		skip_spaces(&arg);
		if (strlen(arg) == 0)
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 2, arg);
		if (!_parse_name(arg, tmp_name) &&
		    strlen(tmp_name) >= MIN_NAME_LENGTH && strlen(tmp_name) <= MAX_NAME_LENGTH &&
		    !strn_cmp(tmp_name, GET_PC_NAME(d->character), MIN(MIN_NAME_LENGTH, strlen(GET_PC_NAME(d->character)) - 1))
		    ) {
			CREATE(GET_PAD(d->character, 2), char, strlen(tmp_name) + 1);
			strcpy(GET_PAD(d->character, 2), CAP(tmp_name));
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 3, tmp_name);
			sprintf(buf, "��� � ����������� ������ (������� ����?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NAME4;
		} else {
			SEND_TO_Q("�����������!!\r\n", d);
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 2, tmp_name);
			sprintf(buf, "��� � ��������� ������ (��������� ����?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
		};
		break;
	case CON_NAME4:
		skip_spaces(&arg);
		if (strlen(arg) == 0)
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 3, arg);
		if (!_parse_name(arg, tmp_name) &&
		    strlen(tmp_name) >= MIN_NAME_LENGTH && strlen(tmp_name) <= MAX_NAME_LENGTH &&
		    !strn_cmp(tmp_name, GET_PC_NAME(d->character), MIN(MIN_NAME_LENGTH, strlen(GET_PC_NAME(d->character)) - 1))
		    ) {
			CREATE(GET_PAD(d->character, 3), char, strlen(tmp_name) + 1);
			strcpy(GET_PAD(d->character, 3), CAP(tmp_name));
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 4, tmp_name);
			sprintf(buf, "��� � ������������ ������ (��������� � ���?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NAME5;
		} else {
			SEND_TO_Q("�����������!!!\n\r", d);
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 3, tmp_name);
			sprintf(buf, "��� � ����������� ������ (������� ����?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
		};
		break;
	case CON_NAME5:
		skip_spaces(&arg);
		if (strlen(arg) == 0)
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 4, arg);
		if (!_parse_name(arg, tmp_name) &&
		    strlen(tmp_name) >= MIN_NAME_LENGTH && strlen(tmp_name) <= MAX_NAME_LENGTH &&
		    !strn_cmp(tmp_name, GET_PC_NAME(d->character), MIN(MIN_NAME_LENGTH, strlen(GET_PC_NAME(d->character)) - 1))
		    ) {
			CREATE(GET_PAD(d->character, 4), char, strlen(tmp_name) + 1);
			strcpy(GET_PAD(d->character, 4), CAP(tmp_name));
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 5, tmp_name);
			sprintf(buf, "��� � ���������� ������ (�������� � ���?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NAME6;
		} else {
			SEND_TO_Q("�����������!!!!\n\r", d);
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 4, tmp_name);
			sprintf(buf, "��� � ������������ ������ (��������� � ���?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
		};
		break;
	case CON_NAME6:
		skip_spaces(&arg);
		if (strlen(arg) == 0)
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 5, arg);
		if (!_parse_name(arg, tmp_name) &&
		    strlen(tmp_name) >= MIN_NAME_LENGTH && strlen(tmp_name) <= MAX_NAME_LENGTH &&
		    !strn_cmp(tmp_name, GET_PC_NAME(d->character), MIN(MIN_NAME_LENGTH, strlen(GET_PC_NAME(d->character)) - 1))
		    ) {
			CREATE(GET_PAD(d->character, 5), char, strlen(tmp_name) + 1);
			strcpy(GET_PAD(d->character, 5), CAP(tmp_name));
			sprintf(buf,
				"������� ������ ��� %s. Enter passwd : ",
				GET_PAD(d->character, 1));
			SEND_TO_Q(buf, d);
			STATE(d) = CON_NEWPASSWD;
		} else {
			SEND_TO_Q("�����������!!!!!\n\r", d);
			GetCase(GET_PC_NAME(d->character), GET_SEX(d->character), 5, tmp_name);
			sprintf(buf, "��� � ���������� ������ (�������� � ���?) [%s]: ", tmp_name);
			SEND_TO_Q(buf, d);
		};
		break;

	case CON_CLOSE:
		break;

	default:
		log("SYSERR: Nanny: illegal state of con'ness (%d) for '%s'; closing connection.",
		    STATE(d), d->character ? GET_NAME(d->character) : "<unknown>");
		STATE(d) = CON_DISCONNECT;	/* Safest to do. */
		break;
	}
}

// ����� �� ������ ������ ���� ����� ��� ��������� � ��������, ��������� ��������� �� buffer
void GetOneParam(std::string & in_buffer, std::string & out_buffer)
{
	std::string::size_type beg_idx = 0, end_idx = 0;
	beg_idx = in_buffer.find_first_not_of(" ");

	if (beg_idx != std::string::npos) {
		// ������ � ���������
		if (in_buffer[beg_idx] == '\'') {
			if (std::string::npos != (beg_idx = in_buffer.find_first_not_of("\'", beg_idx))) {
				if (std::string::npos == (end_idx = in_buffer.find_first_of("\'", beg_idx))) {
					out_buffer = in_buffer.substr(beg_idx);
					in_buffer.clear();
				} else {
					out_buffer = in_buffer.substr(beg_idx, end_idx - beg_idx);
					in_buffer.erase(0, ++end_idx);
				}
			}
		// ������ � ����� ���������� ����� ������
		} else {
			if (std::string::npos != (beg_idx = in_buffer.find_first_not_of(" ", beg_idx))) {
				if (std::string::npos == (end_idx = in_buffer.find_first_of(" ", beg_idx))) {
					out_buffer = in_buffer.substr(beg_idx);
					in_buffer.clear();
				} else {
					out_buffer = in_buffer.substr(beg_idx, end_idx - beg_idx);
					in_buffer.erase(0, end_idx);
				}
			}
		}
		return;
	}

	in_buffer.clear();
	out_buffer.clear();
}

// ������������������� ��������� ���� ����� �� ����� ������, ���� - ��� ����� ����� ����� (�����������)
bool CompareParam(const std::string & buffer, const char *arg, bool full)
{
	if (!*arg || buffer.empty() || (full && buffer.length() != strlen(arg)))
		return 0;

	std::string::size_type i;
	for (i = 0; i != buffer.length() && *arg; ++i, ++arg)
		if (LOWER(buffer[i]) != LOWER(*arg))
			return (0);

	if (i == buffer.length())
		return (1);
	else
		return (0);
}

// ���� ����� � ������ ����������� ������
bool CompareParam(const std::string & buffer, const std::string & buffer2, bool full)
{
	if (buffer.empty() || buffer2.empty()
	    || (full && buffer.length() != buffer2.length()))
		return 0;

	std::string::size_type i;
	for (i = 0; i != buffer.length() && i != buffer2.length(); ++i)
		if (LOWER(buffer[i]) != LOWER(buffer2[i]))
			return (0);

	if (i == buffer.length())
		return (1);
	else
		return (0);
}

// ���� ���������� ������(������ ���������) �� ��� ����, playing �� ������� 1
DESCRIPTOR_DATA *DescByUID(long unique, bool playing)
{
	DESCRIPTOR_DATA *d = 0;

	if (playing) {
		for (d = descriptor_list; d; d = d->next)
			if (d->character && STATE(d) == CON_PLAYING && GET_UNIQUE(d->character) == unique)
				break;
	} else {
		for (d = descriptor_list; d; d = d->next)
			if (d->character && GET_UNIQUE(d->character) == unique)
				break;
	}
	return (d);
}

/**
* ���� ���������� ������ (������) �� �� ���� (� �������� ��� �����, �.�. ������ ���� �� ����������)
* \param id - ��, ������� ����
* \param playing - 0 ���� ���� ������ � ����� ���������, 1 (������) ���� ���� ������ ���������
*/
DESCRIPTOR_DATA* get_desc_by_id(long id, bool playing)
{
	DESCRIPTOR_DATA *d = 0;

	if (playing) {
		for (d = descriptor_list; d; d = d->next)
			if (d->character && STATE(d) == CON_PLAYING && GET_IDNUM(d->character) == id)
				break;
	} else {
		for (d = descriptor_list; d; d = d->next)
			if (d->character && GET_IDNUM(d->character) == id)
				break;
	}
	return d;
}

// ���� ��� ������ �� ��� �����, ������ �������������� �������� - ��������� ��� ��� �����
long GetUniqueByName(const std::string & name, bool god)
{
	for (int i = 0; i <= top_of_p_table; ++i)
		if (!str_cmp(player_table[i].name, name.c_str())) {
			if (!god)
				return player_table[i].unique;
			else {
				if (player_table[i].level < LVL_IMMORT)
					return player_table[i].unique;
				else
					return -1;
			}

		}
	return 0;
}

// ���� ��� ������ �� ��� ����, ������ �������������� �������� - ��������� ��� ��� �����
std::string GetNameByUnique(long unique, bool god)
{
	std::string temp;
	for (int i = 0; i <= top_of_p_table; ++i)
		if (player_table[i].unique == unique) {
			if (!god)
				return (temp = player_table[i].name);
			else {
				if (player_table[i].level < LVL_IMMORT)
					return (temp = player_table[i].name);
				else
					return temp;
			}
		}
	return temp;
}

// ������ � name ������� �������� �� ���� � ������ �������� (��� ������)
void CreateFileName(std::string &name)
{
	for (unsigned i = 0; i != name.length(); ++i)
		name[i] = LOWER(AtoL(name[i]));
}

void ReadEndString(std::ifstream &file)
{
  char c;
  while (file.get(c))
    if (c == '\n')
      return;
}

// ������ ������� (� ������ ������ ����� ������) �� ���� ������, ��� ���������� ������� �������
void StringReplace(std::string & buffer, char s, std::string d)
{
  for(unsigned index = 0; index = buffer.find(s, index), index != std::string::npos; ) {
    buffer.replace(index, 1, d);
    index += d.length();
  }
}

// ����� ����� ��� ������
std::string ExpFormat(long long exp)
{
	std::string out;
	if (exp < 1000000)
		return (boost::lexical_cast<std::string>(exp));
	else if (exp < 1000000000)
		return (boost::lexical_cast<std::string>(exp/1000) + " ���");
	else if (exp < 1000000000000LL)
		return (boost::lexical_cast<std::string>(exp/1000000) + " ���");
	else
		return (boost::lexical_cast<std::string>(exp/1000000000LL) + " ����");
}

/**
* ����������� ������� ������ � ������ �������
*/
void lower_convert(std::string& text)
{
	for (std::string::iterator it = text.begin(); it != text.end(); ++it)
		*it = LOWER(*it);
}

/**
* ����������� ������� ������ � ������ �������
*/
void lower_convert(char* text)
{
	while (*text) {
		*text = LOWER(*text);
		text++;
	}
}

/**
* ����������� ����� � ������ ������� + ������ ������ � ������� (��� �������������� ������ � �����������)
*/
void name_convert(std::string& text)
{
	if (!text.empty()) {
		lower_convert(text);
		*text.begin() = UPPER(*text.begin());
	}
}

/**
* ��������� ������ ������������ ������ � ���� � ����� �� ����
*/
void single_god_invoice(CHAR_DATA* ch)
{
	TitleSystem::show_title_list(ch);
	NewNameShow(ch);
}

/**
* ����� ��������� ����� ������ ��� ������ �� ������������ ������� � ���� ��� � 5 �����
*/
void god_work_invoice()
{
	for (DESCRIPTOR_DATA* d = descriptor_list; d; d = d->next)
		if (d->character && IS_IMMORTAL(d->character) && STATE(d) == CON_PLAYING)
			single_god_invoice(d->character);
}

/**
* ����� ���������� � ����� ���������� �� ������, �������, (������������ ���� � ������� ��� �����) ��� ������ � ��������
*/
void login_change_invoice(CHAR_DATA* ch)
{
	Board::LoginInfo(ch);
	if (IS_IMMORTAL(ch))
		single_god_invoice(ch);
	if (has_mail(GET_IDNUM(ch)))
		send_to_char("&R\r\n��� ������� ������. ������� �� �����!&n\r\n", ch);
}
