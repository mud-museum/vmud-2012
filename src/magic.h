/* ************************************************************************
*   File: magic.h                                     Part of Bylins      *
*  Usage: header: low-level functions for magic; spell template code      *
*                                                                         *
*  All rights reserved.  See license.doc for complete information.        *
*                                                                         *
*  Copyright (C) 1993, 94 by the Trustees of the Johns Hopkins University *
*  CircleMUD is based on DikuMUD, Copyright (C) 1990, 1991.               *
* 									  *
*  $Author: prool $                                                        *
*  $Date: 2008/06/04 18:47:23 $                                           *
*  $Revision: 1.1.1.1 $                                                      *
************************************************************************ */
#ifndef _MAGIC_H_
#define _MAGIC_H_

/* These mobiles do not exist. */
#define MOB_DOUBLE        3000 /*���� ��������� ��� �����*/
#define MOB_SKELETON      3001
#define MOB_ZOMBIE        3002
#define MOB_BONEDOG       3003
#define MOB_BONEDRAGON    3004
#define MOB_BONESPIRIT    3005
#define LAST_NECR_MOB	  3005
#define MOB_KEEPER        104
#define MOB_FIREKEEPER    105

#define MAX_SPELL_AFFECTS 5	/* change if more needed */

#define SpINFO spell_info[spellnum]

#endif
