// #define DEBUG

/* ************************************************************************
*   File: prool.cpp                              Part of Virtustan MUD    *
*  Usage: prool subprograms                                               *
*                                                                         *
*  Copyleft 2007-2010, Prool                                              *
*                                                                         *
*  Author: Prool, proolix@gmail.com, http://prool.kharkov.org             *
************************************************************************ */

#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "db.h"
#include "comm.h"
#include "handler.h"
#include "dg_scripts.h"
#include "interpreter.h"
#include "utils.h"
#include "prool.h"
#include <sstream>
#include <iconv.h>

extern time_t boot_time;

// prototypes
void speech_prooltrigger(CHAR_DATA *ch, char *argument);
int receive_prooltrigger(CHAR_DATA *vict, CHAR_DATA *ch, OBJ_DATA *obj);

char *ptime(void) // ������������ ��������: ������ �� ��������� ������ � ������� ��������
	{
	char *tmstr;
	time_t mytime;

	mytime = time(0);

	tmstr = (char *) asctime(localtime(&mytime));
	*(tmstr + strlen(tmstr) - 1) = '\0';

	return tmstr;

	}

char *boottime(void) // ������������ ��������: ������ �� ��������� ������ � �������� ������� MUD�
	{
	char *tmstr;

	tmstr = (char *) asctime(localtime(&boot_time));
	*(tmstr + strlen(tmstr) - 1) = '\0';

	return tmstr;

	}

void print_time(void) // ��������: ����� �������� ������� �� stdout
	{
	char *tmstr;
	time_t mytime;

	mytime = time(0);

	tmstr = (char *) asctime(localtime(&mytime));
	*(tmstr + strlen(tmstr) - 1) = '\0';

	printf("%s",tmstr);

	return;
	}

void send_email (char *from, char *to, char *subj, char *text)
	{char buf [80*25];

	// char str[]="echo \"Subject: ����\r\nContent-Type: text/plain; charset=koi8-r\r\n\r\n�����\"|/usr/sbin/sendmail -F\"VMUD\" proolix@gmail.com\r\n";

	sprintf(buf,"echo \"Subject: %s\r\nContent-Type: text/plain; charset=koi8-r\r\n\r\n%s\"|/usr/sbin/sendmail -F\"%s\" %s\r\n",subj,text,from,to);

	system(buf);
	// printf("send_email(): '%s'\n",buf);
/*
"echo \"Subject: ��� ���\r\nContent-Type: text/plain; charset=koi8-r\r\n\r\n����������� ������ ������\r\n���: %s\r\n������: %s\"|/usr/sbin/sendmail -F\"VMUD\" %s\r\n",
*/
	}

void koi_to_utf8 (/*const*/ char *str_i, char *str_o)
	{
	iconv_t cd;
	size_t len_i, len_o=MAX_SOCK_BUF*6;
	size_t i;

	if ((cd=iconv_open("UTF-8","KOI8-RU"))==(iconv_t)-1)
		{
		printf("koi_to_utf8: iconv_open error\n");
		return;
		}
	len_i=strlen(str_i);
	if ((i=iconv(cd,&str_i,&len_i,&str_o,&len_o))==(size_t)-1)
		{
		printf("koi_to_utf8: iconv error\n");
		return;
		}
	*str_o=0;
#ifdef DEBUG
	printf("koi_to_utf8() `%s'\n",str_o);
#endif
	if (iconv_close(cd)==-1)
		{
		printf("koi_to_utf8: iconv_close error\n");
		return;
		}
	}

void utf8_to_koi (/*const*/ char *str_i, char *str_o)
	{
	iconv_t cd;
	size_t len_i, len_o=MAX_SOCK_BUF*6;
	size_t i;

	if ((cd=iconv_open("KOI8-RU","UTF-8"))==(iconv_t)-1)
		{
		printf("utf8_to_koi: iconv_open error\n");
		return;
		}
	len_i=strlen(str_i);
#ifdef DEBUG
	printf("utf8_to_koi len_i=%i len_o=%i ",(int)len_i,(int)len_o);
	printf("`%s' ",str_i);
	putshex((char*)str_i);
#endif
	if ((i=iconv(cd,&str_i,&len_i,&str_o,&len_o))==(size_t)-1)
		{
		printf("utf8_to_koi: iconv error: ");
		putshex(str_i);
		printf("\n");
		// return;
		}
//	*str_o=0;
#ifdef DEBUG
	printf("-- `%s'",str_o);
	putshex(str_o);
	printf(" len_i=%i len_o=%i i=%i\n",(int)len_i,(int)len_o,(int)i);
#endif
	if (iconv_close(cd)==-1)
		{
		printf("utf8_to_koi: iconv_close error\n");
		return;
		}
	}

void putshex (char * str) // ����� ������ � 16-������ ���� �� stdout
	{unsigned char *cc;
	cc=(unsigned char*)str;
	while (*cc)
		{
		printf("%2X ",*cc++);
		}
	}

#define PERSLOG_FILE "../log/perslog.txt"

void perslog (char *verb, char *pers)
{FILE *fp;
fp=fopen(PERSLOG_FILE, "a");
fprintf(fp,"%s &W%s&n %s\n",ptime(),pers,verb);
fclose(fp);
}

void sputshex (char * kuda, char * stroka)
// ����� ������ � 16-������ ���� � ������
	{unsigned char *cc;
	cc=(unsigned char*)stroka;
	while (*cc)
		{
		sprintf(kuda, "%2X ",*cc++);
		kuda+=3;
		}
	*kuda=0;
	}

#if 0
void foolish_utf8_to_koi (char *str_i, char *str_o)
	{char *p_i, *p_o;
	char c;
	p_i=str_i;
	p_o=str_o;
	while (*p_i)
		{
		switch (*p_i)
			{
			case 0xD0:
					p_i++;
					switch (*p_i)
						{
						case 0: goto l_exit;
						case 0x81: c='�'; break;
						case 0x90: c='�'; break;
						case 0x91: c='�'; break;
						case 0x92: c='�'; break;
						case 0x93: c='�'; break;
						case 0x94: c='�'; break;
						case 0x95: c='�'; break;
						case 0x96: c='�'; break;
						case 0x97: c='�'; break;
						case 0x98: c='�'; break;
						case 0x99: c='�'; break;
						case 0x9a: c='K'; break;
						case 0x9b: c='�'; break;
						case 0x9c: c='�'; break;
						case 0x9d: c='�'; break;
						case 0x9e: c='�'; break;
						case 0x9f: c='�'; break;
						case 0xa0: c='�'; break;
						case 0xa1: c='�'; break;
						case 0xa2: c='�'; break;
						case 0xa3: c='�'; break;
						case 0xa4: c='�'; break;
						case 0xa5: c='�'; break;
						case 0xa6: c='�'; break;
						case 0xa7: c='�'; break;
						case 0xa8: c='�'; break;
						case 0xa9: c='�'; break;
						case 0xaa: c='�'; break;
						case 0xab: c='�'; break;
						case 0xac: c='�'; break;
						case 0xad: c='�'; break;
						case 0xae: c='�'; break;
						case 0xaf: c='�'; break;
						case 0xb0: c='�'; break;
						case 0xb1: c='�'; break;
						case 0xb2: c='�'; break;
						case 0xb3: c='�'; break;
						case 0xb4: c='�'; break;
						case 0xb5: c='�'; break;
						case 0xb6: c='�'; break;
						case 0xb7: c='�'; break;
						case 0xb8: c='�'; break;
						case 0xb9: c='�'; break;
						case 0xba: c='�'; break;
						case 0xbb: c='�'; break;
						case 0xbc: c='�'; break;
						case 0xbd: c='�'; break;
						case 0xbe: c='�'; break;
						case 0xbf: c='�'; break;
						default: c=0;
						}
					if (c) *p_o++=c;
					break;
			case 0xD1:
					p_i++;
					switch (*p_i)
						{
						case 0: goto l_exit;
						case 0x80: c='�'; break;
						case 0x81: c='�'; break;
						case 0x82: c='�'; break;
						case 0x83: c='�'; break;
						case 0x84: c='�'; break;
						case 0x85: c='�'; break;
						case 0x86: c='�'; break;
						case 0x87: c='�'; break;
						case 0x88: c='�'; break;
						case 0x89: c='�'; break;
						case 0x8a: c='�'; break;
						case 0x8b: c='�'; break;
						case 0x8c: c='�'; break;
						case 0x8d: c='�'; break;
						case 0x8e: c='�'; break;
						case 0x8f: c='�'; break;
						case 0x91: c='�'; break;
						default: c=0;
						}
					if (c) *p_o++=c;
					break;
					break;
			default:	if (*p_i<127) *p_o++=*p_i;
			}
		p_i++;
		}
l_exit:
	*p_o=0; 
	}

void alef_bet (void)
	{
char *alf = "�������������������������������������ţ��������������������������";
	char str_i[2];
	char str_o[10];
	while (*alf)
		{
		printf("%c ",*alf);
		str_i[0]=*alf;
		str_i[1]=0;
		koi_to_utf8(str_i,str_o);
		putshex(str_o);
		alf++;
		}
	}
#endif

// prool triggers

#if 0 // ��� ���� �� �����
int semafor_1;

void speech_prooltrigger(CHAR_DATA *ch, char *argument)
{int room;

room=GET_ROOM_VNUM(ch->in_room);

// printf("� ������� %i ������� '%s'\n", room, argument);

if ((room==9910) && (!strcmp(argument,"������"))) semafor_1=1;
}
#endif // end of ��� ���� �� �����


int receive_prooltrigger(CHAR_DATA *vict, CHAR_DATA *ch, OBJ_DATA *obj)
{int poluchatel, predmet;
OBJ_DATA *obj2;
obj_rnum r_num;

// printf("%s ������� �� %s ������� %s\n",GET_NAME(vict),GET_NAME(ch),GET_OBJ_PNAME(obj,0));
// printf("%i ������� �� %i ������� %i\n",GET_MOB_VNUM(vict),GET_MOB_VNUM(ch),GET_OBJ_VNUM(obj));

poluchatel=GET_MOB_VNUM(vict); // ��� vnum ���� ��� -1, ���� ���������� �����
predmet=GET_OBJ_VNUM(obj);

switch (poluchatel)
	{
	case 9936: // �������������
		if (predmet==9989)
			{// �������� ��, ��� ����
			extract_obj(obj);
			send_to_char("�������, ������� �������������, ��������� �������\r\n", ch);
				if ((r_num = real_object(9981)) < 0) return 0;
				obj2 = read_object(r_num, REAL);
				GET_OBJ_MAKER(obj2) = GET_UNIQUE(ch);
				obj_to_char(obj2, ch);
				act("�� �������� $o3.", FALSE, ch, obj2, 0, TO_CHAR);
				load_otrigger(obj2);
				obj_decay(obj2);
			return (0);
			}
	}
return (1);
}

#define PUT_OBJ(obj_number) {r_num = real_object(obj_number); obj = read_object(r_num, REAL); GET_OBJ_MAKER(obj) = GET_UNIQUE(ch); obj_to_char(obj, ch); act("$n �������$g �� ���� ���� $o3!", FALSE, ch, obj, 0, TO_ROOM); act("�� �������� �� ���� ���� $o3.", FALSE, ch, obj, 0, TO_CHAR); /* load_otrigger(obj); obj_decay(obj); */ olc_log("�������: %s load obj %s #%d", GET_NAME(ch), GET_OBJ_ALIAS(obj), number);}

#define DUH_INSTR "������ �����, ������� ��� ����� �������� ��� ����: ����, �����, ���, ������, ���, �����, ������, ����\r\n\r\n������ ������:\r\n������� �����\r\n"

ACMD(do_duhmada)
{
	mob_rnum r_num;
	OBJ_DATA *obj;

// char str[100];
// sprintf(str,"`%s'",argument);
// send_to_char(str, ch);

if (*argument==0)
	{
	send_to_char(DUH_INSTR, ch);
	}
else
	{
	argument++;
	if (!strcmp(argument,"����")) PUT_OBJ(9902)
	else if (!strcmp(argument,"�����")) PUT_OBJ(9970)
	else if (!strcmp(argument,"���")) PUT_OBJ(9909)
	else if (!strcmp(argument,"�����")) PUT_OBJ(2024)
	else if (!strcmp(argument,"������")) PUT_OBJ(9908)
	else if (!strcmp(argument,"������")) PUT_OBJ(9907)
	else if (!strcmp(argument,"���")) PUT_OBJ(9974)
	else if (!strcmp(argument,"����")) {PUT_OBJ(222); PUT_OBJ(223);}
	else
		{
		send_to_char("� ����� � ������� ����� �� ����!\r\n\r\n",ch);
		send_to_char(DUH_INSTR, ch);
		}
	}

}
