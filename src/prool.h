/* ************************************************************************
*   File: prool.h                                Part of Virtustan MUD    *
*  Usage: prool subprograms and prool preprocessor defines                *
*                                                                         *
*  Copyleft 2007-2010, Prool                                              *
*                                                                         *
*  Author: Prool, prool@itl.ua, http://www.virtustan.net                  *
************************************************************************ */

#define DEFAULT_PORT 8888

#define DEFAULT_MAX_OBJ_IN_WORLD 100
#define DEFAULT_MAX_MOB_IN_WORLD 10
#define DEFAULT_MAX_MOB_IN_ROOM 1

#define PROOL_MAX_STRLEN 100

void print_time(void);
char *ptime(void);
char *boottime(void);
void send_email (char *from, char *to, char *subj, char *text);

void koi_to_utf8 (/*const*/ char*, char*);
void utf8_to_koi (/*const*/ char*, char*);
void foolish_utf8_to_koi (char*, char*);

void putshex (char*);
void sputshex (char*, char*);
void alef_bet(void);

void perslog (char *verb, char *pers);
