/* ************************************************************************
*  File: robots.cpp
*  Usage: robots
*  Author: Prool
*
*  $Date$
*  $Revision$
************************************************************************ */

#if 1
#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "utils.h"
#include "comm.h"
#include "interpreter.h"
#include "handler.h"
#include "db.h"
#include "spells.h"
#include "skills.h"
#include "screen.h"
#include "dg_scripts.h"
#include "constants.h"
#include "features.hpp"
#include "house.h"
#include "prool.h"

/*   external vars  */

extern CHAR_DATA *character_list;
extern DESCRIPTOR_DATA *descriptor_list;
extern INDEX_DATA *mob_index;
extern INDEX_DATA *obj_index;
extern TIME_INFO_DATA time_info;
extern struct spell_create_type spell_create[];
extern int guild_info[][3];

/* extern functions */
void add_follower(CHAR_DATA * ch, CHAR_DATA * leader);
ACMD(do_drop);
ACMD(do_gen_door);
ACMD(do_say);
int go_track(CHAR_DATA * ch, CHAR_DATA * victim, int skill_no);
int has_key(CHAR_DATA * ch, obj_vnum key);
int find_first_step(room_rnum src, room_rnum target, CHAR_DATA * ch);
void do_doorcmd(CHAR_DATA * ch, OBJ_DATA * obj, int door, int scmd);
int check_recipe_items(CHAR_DATA * ch, int spellnum, int spelltype, int extract);
SPECIAL(shop_keeper);
void ASSIGNMASTER(mob_vnum mob, SPECIAL(fname), int learn_info);
int mag_manacost(CHAR_DATA * ch, int spellnum);
int has_key(CHAR_DATA * ch, obj_vnum key);
int ok_pick(CHAR_DATA * ch, obj_vnum keynum, int pickproof, int scmd);
int find_door(CHAR_DATA * ch, const char *type, char *dir, const char *cmdname);
#endif

/* functions */
SPECIAL(robot1);
SPECIAL(majordomo);
SPECIAL(artefakt);
SPECIAL(quest1);

int robot1_tick=0;
#define STATION1_ROOM 207
#define STATION2_ROOM 7162
#define TRAIN_ROOM 208
#define TEZT_BIT(x,y) ((x)&(y))
#define DOOR_IZ_CLOSE(vnum,dir) TEZT_BIT(world[real_room(STATION1_ROOM)]->dir_option[0]->exit_info, EX_CLOSED)
#define DOOR_IZ_OPEN(vnum,dir) (!DOOR_IZ_CLOSE(vnum,dir))
#define CLOZE_DOOR (rnum, dir) TOGGLE_BIT(world[real_room(rnum)]->dir_option[dir]->exit_info, EX_CLOSED);
#define OUPEN_DOOR (rnum, dir) CLOZE_DOOR(rnum, dir)

SPECIAL(majordomo) //prool
{
return 0;
}

SPECIAL(artefakt) //prool
{
// printf("artefakt!\n");
return 0;
}

SPECIAL(quest1) // prool
{
CHAR_DATA *victim = (CHAR_DATA *) me;
if (cmd)
	{ // ���� ������� �����-�� �������
	// printf("quest1: ������� # %i\n",(int)cmd);
	if ((CMD_IS("��������"))||(CMD_IS("say")))
		{
		// printf("quest1: ������� ������� ��������\n");
		act("$N ������$G � �����: '������, $n! ���� � ���� � �������,\n\
����� ��� ����� ����� ������, � ���� � �������, ����� ����� ���-�� �� ���,\n\
�� �� �� ����, �� �� � �������. ����� ��� ����, �����, � � ���� �����������'",
		FALSE, ch, 0, victim,TO_CHAR);
		return (1);
		}
	if ((CMD_IS("����"))||(CMD_IS("give")))
		{
		// printf("quest1: ������� ������� ����\n");
		act("���-�� ����-�� ���-�� ��� ��� �� ���, �� ����������������� ���������� ��� ������� ������", FALSE, ch, 0, victim,TO_CHAR);
		printf("arg=`%s'\n",arg); // ��� �������, �������� `����'
		printf("argument=`%s'\n",argument); // ��� ��������� �������, ����. ` ����� ���'
		return (0);
		}
	return (0);
	}
return (0);
}

SPECIAL(robot1) // prool
{char buf[PROOL_MAX_STRLEN];

if (cmd)
	{ // ���� ������� �����-�� �������
	// printf("robot1: command # %i\n",(int)cmd);
	return (0);
	}
else
	{ // ������� ������� ��� ������� ��� �������� (cmd==0)
#if 1
	// if (number(0,20)==9) act("$n ������ ����������", FALSE, ch, 0, 0, TO_ROOM);
	// printf("robot1 tick\n");
#endif
	sprintf(buf, "����� %i �����\r\n",robot1_tick);
	send_to_room (buf, real_room(STATION1_ROOM), 0);
	send_to_room (buf, real_room(STATION2_ROOM), 0);
	send_to_room (buf, real_room(TRAIN_ROOM), 0);

#if 0
	switch (robot1_tick%4)
		{
		case 0:
			sprintf(buf, "����� �����������\r\n"); 
			send_to_room (buf, real_room(STATION1_ROOM), 0);
			send_to_room (buf, real_room(TRAIN_ROOM), 0);
			world[real_room(STATION1_ROOM)]->dir_option[0]->to_room=0;
			break;
		case 2:
			sprintf(buf, "����� �����������\r\n");
			send_to_room (buf, real_room(STATION1_ROOM), 0);
			send_to_room (buf, real_room(TRAIN_ROOM), 0);
			world[real_room(STATION1_ROOM)]->dir_option[0]->to_room=real_room(TRAIN_ROOM);
			break;
		default:;
		}
#endif

#define DOOR_CLOSE(rum, direct) world[real_room(rum)]->dir_option[direct]->to_room=0
#define DOOR_OPEN(rum, direct, kuda) world[real_room(rum)]->dir_option[direct]->to_room=real_room(kuda)
#define SAY_TO_ROOM(rum,fraza) send_to_room (fraza, real_room(rum), 0)

#if 1 // train
switch (robot1_tick%8)
{
case 0:
// 0 ����� �����������, ����� ������������ �� ������� ����� 2 // doors closed, train send to station 2 (earth)
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� �����������, ��� ����� ������������ � ������� �����\r\n");
SAY_TO_ROOM(STATION1_ROOM,"��������! ����� �����������, ����� ������������ � ������� �����\r\n");
DOOR_CLOSE(TRAIN_ROOM, YUG);
DOOR_CLOSE(STATION1_ROOM, SEVER);
break;

case 1:
// 1 ����� 2 ���� ��������� �� ��. 2 // After 2 ticks, we arrive at the station 2
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� 2 ���� ��� ����� ��������� �� ������� �����\r\n");
SAY_TO_ROOM(STATION2_ROOM,"��������! ����� 2 ���� ��������� ����� �� ������� ������\r\n");
break;

case 2:
// 2 ����� 1 ��� ��������� �� ��. 2 // After 1 ticks, we arrive at the station 2
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� 1 ��� ��� ����� ��������� �� ������� �����\r\n");
SAY_TO_ROOM(STATION2_ROOM,"��������! ����� 1 ��� ��������� ����� �� ������� ������\r\n");
break;

case 3:
// 3 ����� ����������� (������� ����� 2). ������� 1 ��� // doors open, stand 1 tick
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� ������ �� ������� �����. ���������, ����� �����������. ������� 1 ���\r\n");
SAY_TO_ROOM(STATION2_ROOM,"��������! ������ ����� �� ������� ������. ���������, ����� �����������. ������� 1 ���\r\n");
DOOR_OPEN(TRAIN_ROOM, YUG, STATION2_ROOM);
DOOR_OPEN(STATION2_ROOM, SEVER, TRAIN_ROOM);
break;

case 4:
// 4 ����� �����������, ������������ �� ��. 1 // doors close, train send to station 1 (heaven)
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� �����������, ��� ����� ������������ � ������� ������\r\n");
SAY_TO_ROOM(STATION2_ROOM,"��������! ����� �����������, ����� ������������ � ������� ������\r\n");
DOOR_CLOSE(TRAIN_ROOM, YUG);
DOOR_CLOSE(STATION2_ROOM, SEVER);
break;

case 5:
// 5 ����� 2 ���� ��������� �� ��. 1 // After 2 ticks, we arrive at the station 1
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� 2 ���� ��� ����� ��������� �� ������� ������\r\n");
SAY_TO_ROOM(STATION1_ROOM,"��������! ����� 2 ���� ��������� ����� �� ������� �����\r\n");
break;

case 6:
// 6 ����� 1 ��� ��������� �� ��.1 // After 1 ticks, we arrive at the station 1
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� 1 ��� ��� ����� ��������� �� ������� ������\r\n");
SAY_TO_ROOM(STATION1_ROOM,"��������! ����� 1 ��� ��������� ����� �� ������� �����\r\n");
break;

case 7:
// 7 ����� ����������� (��.1). ������� 1 ��� // doors open, stand 1 tick
SAY_TO_ROOM(TRAIN_ROOM,"��������� ���������! ����� ������ �� ������� ������. ���������, ����� �����������. ������� 1 ���\r\n");
SAY_TO_ROOM(STATION1_ROOM,"��������! ������ ����� �� ������� �����. ���������, ����� �����������. ������� 1 ���\r\n");
DOOR_OPEN(TRAIN_ROOM, YUG, STATION1_ROOM);
DOOR_OPEN(STATION1_ROOM, SEVER, TRAIN_ROOM);
break;

default:;
}
#endif // train
	robot1_tick++;
	return 0;
	}
}
