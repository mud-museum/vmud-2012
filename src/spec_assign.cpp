// #define PROOL
#define VIRTUSTAN

/* ************************************************************************
*   File: spec_assign.cpp
*  Usage: Functions to assign function pointers to objs/mobs/rooms        
*                                                                         
*  $Date: 2010/10/20 14:16:53 $                                           
*  $Revision: 1.17 $                                                      
************************************************************************ */

#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "db.h"
#include "constants.h"
#include "interpreter.h"
#include "utils.h"
#include "house.h"
#include "boards.h"
#include "quest2.h"

extern int dts_are_dumps;
extern int mini_mud;

extern INDEX_DATA *mob_index;
extern INDEX_DATA *obj_index;

extern struct quest_room_stru quests_room [QUEST_ROOM_NUM];

SPECIAL(exchange);
SPECIAL(dump);
SPECIAL(repair);
SPECIAL(destroy);
SPECIAL(kazino);
SPECIAL(shop2);
SPECIAL(pet_shops);
SPECIAL(postmaster);
SPECIAL(cityguard);
SPECIAL(receptionist);
SPECIAL(cryogenicist);
SPECIAL(guild_guard);
SPECIAL(guild_mono);
SPECIAL(guild_poly);
SPECIAL(horse_keeper);
SPECIAL(puff);
SPECIAL(guide);
SPECIAL(fido);
SPECIAL(janitor);
SPECIAL(mayor);
SPECIAL(snake);
SPECIAL(thief);
SPECIAL(magic_user);
SPECIAL(bank);
SPECIAL(robot1);
SPECIAL(majordomo);
// prool: ���� �������, ������ ���������
SPECIAL(artefakt);
// prool: ���� �������, ����� ���������
SPECIAL(igra_v_palochki);
SPECIAL(quest);
SPECIAL(questroom);

void assign_kings_castle(void);
char *str_str(char *cs, char *ct);

/* local functions */
void assign_mobiles(void);
void assign_objects(void);
void assign_rooms(void);
void ASSIGNROOM(room_vnum room, SPECIAL(fname));
void ASSIGNMOB(mob_vnum mob, SPECIAL(fname));
void ASSIGNOBJ(obj_vnum obj, SPECIAL(fname));

/* functions to perform assignments */

void ASSIGNMOB(mob_vnum mob, SPECIAL(fname))
{
	mob_rnum rnum;

	if ((rnum = real_mobile(mob)) >= 0)
		mob_index[rnum].func = fname;
	else if (!mini_mud)
		log("SYSERR: Attempt to assign spec to non-existant mob #%d", mob);
}

void ASSIGNOBJ(obj_vnum obj, SPECIAL(fname))
{
	obj_rnum rnum;

	if ((rnum = real_object(obj)) >= 0)
		obj_index[rnum].func = fname;
	else if (!mini_mud)
		log("SYSERR: Attempt to assign spec to non-existant obj #%d", obj);
}

void ASSIGNROOM(room_vnum room, SPECIAL(fname))
{
	room_rnum rnum;

	if ((rnum = real_room(room)) != NOWHERE)
		world[rnum]->func = fname;
	else if (!mini_mud)
		log("SYSERR: Attempt to assign spec to non-existant room #%d", room);
}

void ASSIGNMASTER(mob_vnum mob, SPECIAL(fname), int learn_info)
{
	mob_rnum rnum;

	if ((rnum = real_mobile(mob)) >= 0) {
		mob_index[rnum].func = fname;
		mob_index[rnum].stored = learn_info;
	} else if (!mini_mud)
		log("SYSERR: Attempt to assign spec to non-existant mob #%d", mob);
}


/* ********************************************************************
*  Assignments                                                        *
******************************************************************** */

/* assign special procedures to mobiles */
void assign_mobiles(void)
{
// assign_kings_castle();

#ifdef PROOL
printf("assign_mobiles() \n");
#endif
//	ASSIGNMOB(9918, puff); // ��� ���������� ��������� ���� ������� ������ � ����� specials.lst

	/* HOTEL */
//	ASSIGNMOB(3005, receptionist);

	/* POSTMASTER */
//	ASSIGNMOB(3027, postmaster);

	/* BANK */
//	ASSIGNMOB(3019, bank);

	/* HORSEKEEPER */
//	ASSIGNMOB(3123, horse_keeper);
}



/* assign special procedures to objects */
void assign_objects(void)
{
#ifdef PROOL
printf("assign_objects() \n");
#endif

#ifndef VIRTUSTAN
	ASSIGNOBJ(GODGENERAL_BOARD_OBJ, Board::Special);
	ASSIGNOBJ(GENERAL_BOARD_OBJ, Board::Special);
	ASSIGNOBJ(GODCODE_BOARD_OBJ, Board::Special);
	ASSIGNOBJ(GODPUNISH_BOARD_OBJ, Board::Special);
	ASSIGNOBJ(GODBUILD_BOARD_OBJ, Board::Special);
#endif
}



/* assign special procedures to rooms */
void assign_rooms(void)
{
	room_rnum i;

#ifdef PROOL
printf("assign_rooms() \n");
#endif
//	ASSIGNROOM(9984,guild_poly);
#ifdef PROOL
printf("assign_rooms() 2 \n");
#endif
	if (dts_are_dumps)
		for (i = FIRST_ROOM; i <= top_of_world; i++)
			if (ROOM_FLAGGED(i, ROOM_DEATH))
				world[i]->func = dump;
}

extern int shop2_rooms [];

void init_spec_procs(void)
{
	FILE *magic;
	char line1[256], line2[256], name[256];
	int i;

	if (!(magic = fopen(LIB_MISC "specials.lst", "r"))) {
		log("Can't open specials list file");
		puts("Can't open specials list file");
		return;
	}
	while (get_line(magic, name)) {
		if (!name[0] || name[0] == ';')
			continue;
		if (sscanf(name, "%s %d %s", line1, &i, line2) != 3) {
			log("Bad format for special string !\r\n"
			    "Format : <who/what (%%s)> <vnum (%%d)> <type (%%s)>");
			_exit(1);
		}
		log("<%s>-%d-[%s]", line1, i, line2);
		if (!str_cmp(line1, "mob")) {
			if (real_mobile(i) < 0) {
				log("Unknown mobile %d in specials assignment", i);
				printf("Unknown mobile %d in specials assignment\n", i);
				continue;
			}
			if (!str_cmp(line2, "puff"))
				ASSIGNMOB(i, puff);
			else if (!str_cmp(line2, "rent"))
				ASSIGNMOB(i, receptionist);
			else if (!str_cmp(line2, "cryo")) // prool
				ASSIGNMOB(i, cryogenicist);
			else if (!str_cmp(line2, "mail"))
				ASSIGNMOB(i, postmaster);
			else if (!str_cmp(line2, "bank"))
				ASSIGNMOB(i, bank);
			else if (!str_cmp(line2, "horse"))
				ASSIGNMOB(i, horse_keeper);
			else if (!str_cmp(line2, "fido"))
				ASSIGNMOB(i, fido);
			else if (!str_cmp(line2, "janitor"))
				ASSIGNMOB(i, janitor);
//++F@N
			else if (!str_cmp(line2, "exchange"))
				ASSIGNMOB(i, exchange);
//--F@N
			else if (!str_cmp(line2, "guild_guard")) // prool
				ASSIGNMOB(i, guild_guard);
			else if (!str_cmp(line2, "cityguard")) // prool
				ASSIGNMOB(i, cityguard);
			else if (!str_cmp(line2, "pet_shops")) // prool
				ASSIGNMOB(i, pet_shops);
			else if (!str_cmp(line2, "mayor")) // prool
				ASSIGNMOB(i, mayor);
			else if (!str_cmp(line2, "snake")) // prool
				ASSIGNMOB(i, snake);
			else if (!str_cmp(line2, "thief")) // prool
				ASSIGNMOB(i, thief);
			else if (!str_cmp(line2, "mage")) // prool
				ASSIGNMOB(i, magic_user);
			else if (!str_cmp(line2, "guide")) // prool
				ASSIGNMOB(i, guide);
			else if (!str_cmp(line2, "robot1")) // prool
				ASSIGNMOB(i, robot1);
			else if (!str_cmp(line2, "majordomo")) // prool
				ASSIGNMOB(i, majordomo);
			else if (!str_cmp(line2, "quest")) // prool
				ASSIGNMOB(i, quest);
			else
				log("Unknown mobile %d assignment type - %s...", i, line2);
		} else if (!str_cmp(line1, "obj")) {
			if (real_object(i) < 0) {
				log("Unknown object %d in spec assign", i);
				continue;
			}
			else if (!str_cmp(line2,"artefakt")) {
			     ASSIGNOBJ(i, artefakt);
			     //printf("ASSIGNOBJ %i artefakt\n",i);
			     }
			// prool: prool: ���� �������, ������ ���������
			else if (!str_cmp(line2,"igra-v-palochki")) {
			     //ASSIGNOBJ(i, igra_v_palochki); // prool: ���� � ������� ��� �� ��������.
			     //printf("ASSIGNOBJ %i igra-v-palochki\n",i);
			     }
			// prool: prool: ���� �������, ����� ���������
		} else if (!str_cmp(line1, "room")) {
			if (!str_cmp(line2, "dump")) // prool
				ASSIGNROOM(i, dump);
			else if (!str_cmp(line2, "repair")) // prool
				ASSIGNROOM(i, repair);
			else if (!str_cmp(line2, "destroy")) // prool
				ASSIGNROOM(i, destroy);
			else if (!str_cmp(line2, "shop2")) // prool
				ASSIGNROOM(i, shop2);
			else if (!str_cmp(line2, "kazino")) // prool
				ASSIGNROOM(i, kazino);
			else if (!str_cmp(line2, "questroom")) // prool
				ASSIGNROOM(i, questroom);
		} else {
			log("Error in specials file !\r\n" "May be : mob, obj or room...");
			_exit(1);
		}
	}
	fclose(magic);

// ���������� ��������� ������ "��������-2" (������������� shop2) �� ������� ������. prool
i=0;
while (shop2_rooms[i])
	ASSIGNROOM(shop2_rooms[i++],shop2);

// ���������� ������, � ������� ��������� ������
for (i=0;i<QUEST_ROOM_NUM;i++)
	{
	if (quests_room[i].room==-1) break;
	else 
		ASSIGNROOM (quests_room[i].room,questroom);
	}
	return;
}
