// $Date: 2010/06/20 11:48:44 $ $Revision: 1.5 $
// Copyright (c) 2007 Krodo

#include "conf.h"
#include "sysdep.h"
#include "structs.h"
#include "comm.h"

#include "prool.h"

extern int ports[];

/**
* ���� ���� - ������������� ��� ������� �������������� ������� � ��� ���������� ���� ������.
*/

char *server_compilation_date = __DATE__;
char *server_compilation_time = __TIME__;

void show_code_date(CHAR_DATA *ch)
{
#ifdef CYGWIN
send_to_char(ch, "Virtustan MUD, cygwin ������, ���������� �� %s %s\r\n",
	server_compilation_date, server_compilation_time);
#else
send_to_char(ch, "Virtustan MUD, ���������� �� %s %s\r\n",
	server_compilation_date, server_compilation_time);
#endif

#if 0
send_to_char(ch, "Port %i\r\n",ports[0]);
if (ports[0]==8888)
	send_to_char(ch, "&G�������� ������&n\r\n");
else if (ports[0]==9999)
	send_to_char(ch, "&R���������� ������ MUD2&n\r\n");
#endif
// send_to_char(ch, "\r\nVMUD start at %s\r\n",boottime());
}
